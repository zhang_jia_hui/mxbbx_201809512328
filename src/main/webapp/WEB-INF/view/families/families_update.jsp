<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/families/editFamilies">
            <div class="form-group">
                <div class="label">
                    <label>家庭成员编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.familiesId}" name="familiesId" data-validate="required:建议不修改" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.familyId}" name="familyId" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>农合证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.nongheId}" name="nongheId" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>医疗证卡号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.medicalId}" name="medicalId" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>户内编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.indoorId}" name="indoorId" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

<%--            <div class="form-group">--%>
<%--                <div class="label">--%>
<%--                    <label>姓名：</label>--%>
<%--                </div>--%>
<%--                <div class="field">--%>
<%--                    <input type="text" class="input w50" value="${families.name}" name="name" data-validate="required:" />--%>
<%--                    <div class="tips"></div>--%>
<%--                </div>--%>
<%--            </div>--%>

            <div class="form-group">
                <div class="label">
                    <label>与户主关系：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.holderRela}" name="holderRela" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

<%--            <div class="form-group">--%>
<%--                <div class="label">--%>
<%--                    <label>身份证号：</label>--%>
<%--                </div>--%>
<%--                <div class="field">--%>
<%--                    <input type="text" class="input w50" value="${families.number}" name="number" data-validate="required:" />--%>
<%--                    <div class="tips"></div>--%>
<%--                </div>--%>
<%--            </div>--%>

            <!-- 人员信息 -->
            <div class="form-group">
                <div class="label">
                    <label>姓名：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${families.name}" name="name" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>性别：</label>
                </div>
                <div class="label">
                    <label>
                        <input type="radio" name="gender" value="1"  data-validate="required:必选"
                                <c:if test="${personnel.gender eq 1}">checked</c:if> />男</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="gender" value="0"
                                  <c:if test="${personnel.gender eq 0}">checked</c:if> />女</label>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>年龄：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.age}" name="age" data-validate="required:必填" maxlength="3"
                           onkeyup="value=value.replace(/[^0-9]/g,'')" onpaste="value=value.replace(/[^0-9]/g,'')" oncontextmenu = "value=value.replace(/[^0-9]/g,'')"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>身份证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" maxlength="18" value="${families.number}" name="number" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>出生日期：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" id="birthday"
                           name="birthday" data-validate="required:必填" value="${personnel.birthday}"/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#birthday' //指定元素
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label>民族：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.nation}" name="nation" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>人员属性：</label>
                </div>
                <div class="field">
                    <select id="personAttributes" style="width:200px" name="personAttributes" class="input w50">
                        <option value="">请选择政治面貌</option>
                        <option <c:if test="${personnel.personAttributes eq '中共党员'}">selected</c:if> value="中共党员">中共党员</option>
                        <option <c:if test="${personnel.personAttributes eq '中共预备党员'}">selected</c:if> value="中共预备党员">中共预备党员</option>
                        <option <c:if test="${personnel.personAttributes eq '共青团员'}">selected</c:if> value="共青团员">共青团员</option>
                        <option <c:if test="${personnel.personAttributes eq '民革党员'}">selected</c:if> value="民革党员">民革党员</option>
                        <option <c:if test="${personnel.personAttributes eq '民盟盟员'}">selected</c:if> value="民盟盟员">民盟盟员</option>
                        <option <c:if test="${personnel.personAttributes eq '民建会员'}">selected</c:if> value="民建会员">民建会员</option>
                        <option <c:if test="${personnel.personAttributes eq '民进会员'}">selected</c:if> value="民进会员">民进会员</option>
                        <option <c:if test="${personnel.personAttributes eq '农工党党员'}">selected</c:if> value="农工党党员">农工党党员</option>
                        <option <c:if test="${personnel.personAttributes eq '致公党党员'}">selected</c:if> value="致公党党员">致公党党员</option>
                        <option <c:if test="${personnel.personAttributes eq '九三学社社员'}">selected</c:if> value="九三学社社员">九三学社社员</option>
                        <option <c:if test="${personnel.personAttributes eq '台盟盟员'}">selected</c:if> value="台盟盟员">台盟盟员</option>
                        <option <c:if test="${personnel.personAttributes eq '无党派人士'}">selected</c:if> value="无党派人士">无党派人士</option>
                        <option <c:if test="${personnel.personAttributes eq '群众'}">selected</c:if> value="群众">群众</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>文化程度：</label>
                </div>
                <div class="field">
                    <select id="degree" style="width:200px" name="degree" class="input w50">
                        <option value="">请选择学历</option>
                        <option <c:if test="${personnel.degree eq '博士'}">selected</c:if> value="博士">博士</option>
                        <option <c:if test="${personnel.degree eq '硕士'}">selected</c:if> value="硕士">硕士</option>
                        <option <c:if test="${personnel.degree eq '本科'}">selected</c:if> value="本科">本科</option>
                        <option <c:if test="${personnel.degree eq '大专'}">selected</c:if> value="大专">大专</option>
                        <option <c:if test="${personnel.degree eq '中专和中技'}">selected</c:if> value="中专和中技">中专和中技</option>
                        <option <c:if test="${personnel.degree eq '技工学校'}">selected</c:if> value="技工学校">技工学校</option>
                        <option <c:if test="${personnel.degree eq '高中'}">selected</c:if> value="高中">高中</option>
                        <option <c:if test="${personnel.degree eq '初中'}">selected</c:if> value="初中">初中</option>
                        <option <c:if test="${personnel.degree eq '小学'}">selected</c:if> value="小学">小学</option>
                        <option <c:if test="${personnel.degree eq '文盲与半文盲'}">selected</c:if> value="文盲与半文盲">文盲与半文盲</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>健康状况：</label>
                </div>
                <div class="field">
                    <select id="health" style="width:200px" name="health" class="input w50">
                        <option value="">请选择健康状况</option>
                        <option <c:if test="${personnel.health eq '健康'}">selected</c:if> value="健康">健康</option>
                        <option <c:if test="${personnel.health eq '轻微疾病'}">selected</c:if> value="轻微疾病">轻微疾病</option>
                        <option <c:if test="${personnel.health eq '重大疾病'}">selected</c:if> value="重大疾病">重大疾病</option>
                        <option <c:if test="${personnel.health eq '遗传疾病'}">selected</c:if> value="遗传疾病">遗传疾病</option>
                        <option <c:if test="${personnel.health eq '流行性疾病'}">selected</c:if> value="流行性疾病">流行性疾病</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>是否是农村户口：</label>
                </div>
                <div class="label">
                    <label><input type="radio" name="countryside" value="1" data-validate="required:必选"
                                  <c:if test="${personnel.countryside eq 1}">checked</c:if> />是</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="countryside" value="0"
                                  <c:if test="${personnel.countryside eq 0}">checked</c:if> />否</label>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>职业：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.occupation}" name="occupation"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>工作单位：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.workUnit}" name="workUnit"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>联系方式：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.contact}" name="contact" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>联系电话：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.telephone}" name="telephone" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>常住家庭住址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${personnel.homeAddress}" name="homeAddress" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>婚姻状况：</label>
                </div>
                <div class="field">
                    <select id="marriage" style="width:200px" name="marriage" class="input w50" >
                        <option value="">请选择婚姻状况</option>
                        <option <c:if test="${personnel.marriage eq '未婚'}">selected</c:if> value="未婚">未婚</option>
                        <option <c:if test="${personnel.marriage eq '初婚'}">selected</c:if> value="初婚">初婚</option>
                        <option <c:if test="${personnel.marriage eq '再婚'}">selected</c:if> value="再婚">再婚</option>
                        <option <c:if test="${personnel.marriage eq '复婚'}">selected</c:if> value="复婚">复婚</option>
                        <option <c:if test="${personnel.marriage eq '丧偶'}">selected</c:if> value="丧偶">丧偶</option>
                        <option <c:if test="${personnel.marriage eq '离婚'}">selected</c:if> value="离婚">离婚</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>结婚时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入结婚时间" id="marriageTime"
                           name="marriageTime" value="${personnel.marriageTime}"/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#marriageTime', //指定元素
                  type: 'datetime'
                });
              });
            </script>


            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/families/findFamiliesAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
