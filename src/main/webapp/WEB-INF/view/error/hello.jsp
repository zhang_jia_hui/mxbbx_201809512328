<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>登录失败</title>
    <script type="text/javascript">
    </script>
</head>
<body>
    <div style="width:100%;height:60%;margin-top:15%">
        <div style="text-align: center; font-weight: bold; color: #666666;font-size: 24px">软件工程（个人版）</div>
        <div style="text-align: center; font-weight: bold; color: #666666;font-size: 24px">18软件工程3班</div>
        <div style="text-align: center; font-weight: bold; color: #666666;font-size: 24px">张佳辉</div>
        <div style="text-align: center; font-weight: bold;line-height: 200px; color: #bbbbbb;font-size: 22px">
            时间：<span id="time"></span>
        </div>
    </div>
</body>
<script>
  function show(){
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    month=month<10?"0"+month:month;
    var day = date.getDate();
    day=day<10?"0"+day:day;
    var week = date.getDay();
    week="日一二三四五六".charAt(week);
    week="星期"+week;
    var hour = date.getHours();
    hour= hour<10?"0"+ hour: hour;
    var minute = date.getMinutes();
    minute=minute<10?"0"+minute:minute;
    var second = date.getSeconds();
    second=second<10?"0"+second:second;
    var result = year+"."+month+"."+day+" "+week+" "+hour+":"+minute+":"+second ;
    document.getElementById("time").innerHTML = result;
  }
  show();
  setInterval("show()",1000);
</script>
</html>