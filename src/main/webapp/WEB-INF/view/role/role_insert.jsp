<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <%--  导入dtree  --%>
    <link rel="StyleSheet" href="<%=basePath %>/js/dtree/dtree.css" type="text/css" />
    <script type="text/javascript" src="<%=basePath %>/js/dtree/dtree.js"></script>
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增角色信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/role/addRole" onsubmit="return findChecks()">
            <div class="form-group">
                <div class="label">
                    <label>角色ID：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入角色ID(仅限数字)" maxlength="10" onclick="JavaScript:this.value=''" value="100" name="roleId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>角色名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入角色名称"  onclick="JavaScript:this.value=''" value="管理员" name="roleName" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>


<%--            <label>--%>
<%--                <c:forEach items="${limitLists}" var="map">--%>
<%--                    <input style="margin: 5px" class="check-box" type="checkbox" name="checkbox" value="${map.limitId}"/>${map.limitText}--%>
<%--                    <c:if test="${map.id % 5 == 0}"><br></c:if>--%>
<%--                </c:forEach>--%>
<%--            </label>--%>

            <div class="form-group">
                <div class="label">
                    <label>角色权限：</label>
                </div>
                <div style="width:80%; float:left;">
                    <div class="dtree">
                        <script type="text/javascript">
                          d = new dTree('d');
                          <c:forEach items="${limitLists}" var="map">
                          <c:if test="${map.limitGrade != null}">
                          d.add(${map.id}, ${map.limitGrade}, '${map.limitText}');
                          </c:if>
                          </c:forEach>
                          document.write(d);
                        </script>
                        <p style="margin-top: 20px">
                            <a href="javascript: d.openAll();">打开全部</a> | <a href="javascript: d.closeAll();">收回全部</a>
                        </p>
                    </div>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/role/findRoleAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
