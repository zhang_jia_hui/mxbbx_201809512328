package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Policy;
import com.gxuwz.zjh.mxbbx.mapper.PolicyMapper;
import com.gxuwz.zjh.mxbbx.service.IPolicyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 慢病政策数据模型 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class PolicyServiceImpl extends ServiceImpl<PolicyMapper, Policy> implements IPolicyService {

    @Autowired
    private PolicyMapper policyMapper;

    @Override
    public Policy findPolicyById(Policy policy) {
        return policyMapper.findPolicyById(policy);
    }

    @Override
    public List<Policy> findPolicyAll() {
        return policyMapper.findPolicyAll();
    }

    @Override
    public Policy findPolicyByYear(Policy policy) {
        return policyMapper.findPolicyByYear(policy);
    }

    @Override
    public IPage<Policy> selectPolicyPage(Page<Policy> page, Wrapper<Policy> wrapper) {
        return policyMapper.selectPolicyPage(page, wrapper);
    }

    @Override
    public void addPolicy(Policy policy) {
        policyMapper.addPolicy(policy);
    }

    @Override
    public void updatePolicyById(Policy policy) {
        policyMapper.updatePolicyById(policy);
    }

    @Override
    public void deletePolicyById(Policy policy) {
        policyMapper.deletePolicyById(policy);
    }
}
