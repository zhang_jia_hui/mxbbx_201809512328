package com.gxuwz.zjh.mxbbx.controller;


import com.gxuwz.zjh.mxbbx.entity.ChronicCard;
import com.gxuwz.zjh.mxbbx.entity.ChronicReim;
import com.gxuwz.zjh.mxbbx.entity.Families;
import com.gxuwz.zjh.mxbbx.entity.Personnel;
import com.gxuwz.zjh.mxbbx.service.IChronicCardService;
import com.gxuwz.zjh.mxbbx.service.IChronicReimService;
import com.gxuwz.zjh.mxbbx.service.IFamiliesService;
import com.gxuwz.zjh.mxbbx.service.IPersonnelService;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 导出execl表格工具
 */

@Controller
@RequestMapping("/mxbbx/excel")
public class ExcelController {

    @Autowired
    private IChronicCardService iChronicCardService;
    @Autowired
    private IChronicReimService iChronicReimService;
    @Autowired
    private IFamiliesService iFamiliesService;


    @RequestMapping(value = "/cardExcelDownloads", method = RequestMethod.GET)
    public void cardExcelDownloads(HttpServletResponse response, HttpServletRequest request,
                                   @Param("address") String address,
                                   @Param("excel") String excel) throws IOException {
        if(excel == null || excel == ""){
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("请假信息表");

            ChronicCard chronicCard = new ChronicCard();
            chronicCard.setAddress(address);
            List<ChronicCard> chronicCardList = iChronicCardService.findChronicCardAll();

            String fileName = "chronicCard"  + ".xls";//设置要导出的文件的名字
            //新增数据行，并且设置单元格数据

            int rowNum = 1;

            String[] headers = { "慢性病证ID", "姓名", "家庭住址", "慢性病名称", "慢性病起始时间", "慢性病结束时间", "医院证明" };
            //headers表示excel表中第一行的表头

            HSSFRow row = sheet.createRow(0);
            //在excel表中添加表头

            for(int i=0;i<headers.length;i++){
                HSSFCell cell = row.createCell(i);
                HSSFRichTextString text = new HSSFRichTextString(headers[i]);
                cell.setCellValue(text);
            }

            //在表中存放查询到的数据放入对应的列
            for (ChronicCard chronicCard1 : chronicCardList) {
                HSSFRow row1 = sheet.createRow(rowNum);
                row1.createCell(0).setCellValue(chronicCard1.getCardId());
                row1.createCell(1).setCellValue(chronicCard1.getName());
                row1.createCell(2).setCellValue(chronicCard1.getAddress());
                row1.createCell(3).setCellValue(chronicCard1.getChronicId());
                row1.createCell(4).setCellValue(chronicCard1.getChronicStartTime());
                row1.createCell(5).setCellValue(chronicCard1.getChronicEndTime());
                rowNum++;
            }

            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            response.flushBuffer();
            workbook.write(response.getOutputStream());
        }else {

            // 获取导出条件
            String chronicId = null;
            String region = null;
            List<ChronicReim> chronicReimList = new ArrayList<>();

            if(request.getSession().getAttribute("chronic") == null || request.getSession().getAttribute("chronic") == ""){
                chronicReimList = iChronicReimService.findChronicReimAll();
            }else {
                chronicId = (String) request.getSession().getAttribute("chronic");
                ChronicReim chronicReim = new ChronicReim();
                chronicReim.setChronicId(chronicId);
                chronicReimList = iChronicReimService.findChronicReimByChronic(chronicReim);
            }

            List<Families> familiesList = iFamiliesService.findFamiliesAll();

            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("报销信息表");

            String fileName = "chronicReim"  + ".xls";//设置要导出的文件的名字
            //新增数据行，并且设置单元格数据

            int rowNum = 1;

            String[] headers = { "医院发票号", "医院花费", "实际报销金额", "报销总金额", "参合农民身份证号", "慢性病编号", "领款时间", "操作员", "状态",
                    "家庭成员编号", "家庭编号", "农合证号", "医疗证卡号", "户内编号", "姓名", "与户主关系"};
            //headers表示excel表中第一行的表头

            HSSFRow row = sheet.createRow(0);
            //在excel表中添加表头

            for(int i=0;i<headers.length;i++){
                HSSFCell cell = row.createCell(i);
                HSSFRichTextString text = new HSSFRichTextString(headers[i]);
                cell.setCellValue(text);
            }

            //在表中存放查询到的数据放入对应的列
            for (ChronicReim chronicReim1 : chronicReimList) {
                HSSFRow row1 = sheet.createRow(rowNum);
                row1.createCell(0).setCellValue(chronicReim1.getInvoice());
                row1.createCell(1).setCellValue(chronicReim1.getWantPay());
                row1.createCell(2).setCellValue(chronicReim1.getNowPay());
                row1.createCell(3).setCellValue(chronicReim1.getPayAll());
                row1.createCell(4).setCellValue(chronicReim1.getNumber());
                row1.createCell(5).setCellValue(chronicReim1.getChronicId());
                row1.createCell(6).setCellValue(chronicReim1.getTime());
                row1.createCell(7).setCellValue(chronicReim1.getPerson());
                row1.createCell(8).setCellValue(chronicReim1.getStart());
                for(Families families:familiesList){
                    if(families.getNumber().equals(chronicReim1.getNumber())){
                        row1.createCell(9).setCellValue(families.getFamiliesId());
                        row1.createCell(10).setCellValue(families.getFamilyId());
                        row1.createCell(11).setCellValue(families.getNongheId());
                        row1.createCell(12).setCellValue(families.getMedicalId());
                        row1.createCell(13).setCellValue(families.getIndoorId());
                        row1.createCell(14).setCellValue(families.getName());
                        row1.createCell(15).setCellValue(families.getHolderRela());
                    }
                }
                rowNum++;
            }

            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            response.flushBuffer();
            workbook.write(response.getOutputStream());
        }
    }

}
