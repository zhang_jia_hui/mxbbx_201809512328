package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Mechanism;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * (S201_04)  机构级别 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IMechanismService extends IService<Mechanism> {

    // 根据 id 查询对应用户信息
    Mechanism findMechanismById(Mechanism mechanism);

    // 查询全部用户信息
    List<Mechanism> findMechanismAll();

    //自定义sql 分页
    IPage<Mechanism> selectMechanismPage(Page<Mechanism> page, @Param(Constants.WRAPPER) Wrapper<Mechanism> wrapper);

    // 添加用户信息
    void addMechanism(Mechanism mechanism);

    // 修改用户信息
    void updateMechanismById(Mechanism mechanism);

    // 删除用户信息
    void deleteMechanismById(Mechanism mechanism);

}
