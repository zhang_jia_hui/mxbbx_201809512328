package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Limit;
import com.gxuwz.zjh.mxbbx.mapper.LimitMapper;
import com.gxuwz.zjh.mxbbx.mapper.RoleMapper;
import com.gxuwz.zjh.mxbbx.service.ILimitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-19
 */
@Service
public class LimitServiceImpl extends ServiceImpl<LimitMapper, Limit> implements ILimitService {

    @Autowired
    private LimitMapper limitMapper;

    @Override
    public Limit findLimitById(Limit limit) {
        return limitMapper.findLimitById(limit);
    }

    @Override
    public Integer findLimitByGrades(Limit limit) {
        return limitMapper.findLimitByGrades(limit);
    }

    @Override
    public List<Limit> findLimitAll() {
        return limitMapper.findLimitAll();
    }

    @Override
    public IPage<Limit> selectLimitPage(Page<Limit> page, Wrapper<Limit> wrapper) {
        return limitMapper.selectLimitPage(page, wrapper);
    }

    @Override
    public void addLimit(Limit limit) {
        limitMapper.addLimit(limit);
    }

    @Override
    public void updateLimitById(Limit limit) {
        limitMapper.updateLimitById(limit);
    }

    @Override
    public void deleteLimitById(Limit limit) {
        limitMapper.deleteLimitById(limit);
    }
}
