package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Region;
import com.gxuwz.zjh.mxbbx.mapper.RegionMapper;
import com.gxuwz.zjh.mxbbx.service.IRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 行政区域信息模型 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements IRegionService {

    @Autowired
    private RegionMapper regionMapper;
    @Override
    public Region findRegionById(Region region) {
        return regionMapper.findRegionById(region);
    }

    @Override
    public List<Region> findRegionAll() {
        return regionMapper.findRegionAll();
    }

    @Override
    public IPage<Region> selectRegionPage(Page<Region> page, Wrapper<Region> wrapper) {
        return regionMapper.selectRegionPage(page, wrapper);
    }

    @Override
    public void addRegion(Region region) {
        regionMapper.addRegion(region);
    }

    @Override
    public void updateRegionById(Region region) {
        regionMapper.updateRegionById(region);
    }

    @Override
    public void deleteRegionById(Region region) {
        regionMapper.deleteRegionById(region);
    }
}
