package com.gxuwz.zjh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

//使用注解方式实现监听器
@ServletComponentScan
@SpringBootApplication
public class ZjhApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZjhApplication.class, args);
    }

}
