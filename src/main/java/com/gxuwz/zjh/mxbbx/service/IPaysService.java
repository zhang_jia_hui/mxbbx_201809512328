package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import com.gxuwz.zjh.mxbbx.entity.Pays;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
public interface IPaysService extends IService<Pays> {

    // 根据 id 查询对应信息
    Pays findPaysById(Pays pays);

    // 根据 family 查询对应信息
    Pays findPaysByFamily(Pays pays);

    // 查询全部信息
    List<Pays> findPaysAll();

    //自定义sql 分页
    IPage<Pays> selectPaysPage(Page<Pays> page, @Param(Constants.WRAPPER) Wrapper<Pays> wrapper);

    // 添加信息
    void addPays(Pays pays);

    // 修改信息
    void updatePaysById(Pays pays);

    // 删除信息
    void deletePaysById(Pays pays);

}
