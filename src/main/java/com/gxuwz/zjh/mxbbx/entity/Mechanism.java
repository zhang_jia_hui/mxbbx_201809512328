package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * (S201_04)  机构级别
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Mechanism") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Mechanism对象", description="(S201_04)  机构级别")
public class Mechanism implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "机构级别编号")
    private String mechanismId;

    @ApiModelProperty(value = "机构级别名称")
    private String mechanismName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
