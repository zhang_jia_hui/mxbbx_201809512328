package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * (S201_01)  机构所属经济类型 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface EconomicMapper extends BaseMapper<Economic> {

    // 根据 id 查询对应用户信息
    Economic findEconomicById(Economic economic);

    // 查询全部用户信息
    List<Economic> findEconomicAll();

    //自定义sql 分页
    IPage<Economic> selectEconomicPage(Page<Economic> page, @Param(Constants.WRAPPER) Wrapper<Economic> wrapper);

    // 添加用户信息
    void addEconomic(Economic economic);

    // 修改用户信息
    void updateEconomicById(Economic economic);

    // 删除用户信息
    void deleteEconomicById(Economic economic);

}
