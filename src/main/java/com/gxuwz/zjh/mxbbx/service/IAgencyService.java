package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxuwz.zjh.mxbbx.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 农合经办机构信息 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IAgencyService extends IService<Agency> {
    // 根据id查询信息
    Agency findAgencyById(Agency agency);

    // 查询全部信息
    List<Agency> findAgencyAll();

    //自定义sql分页
    IPage<Agency> selectAgencyPage(Page<Agency> page, @Param(Constants.WRAPPER) Wrapper<Agency> wrapper);

    // 添加信息
    void addAgency(Agency agency);

    // 修改信息
    void updateAgencyById(Agency agency);

    // 删除信息
    void deleteAgencyById(Agency agency);

}
