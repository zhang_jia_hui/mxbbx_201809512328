package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Mechanism;
import com.gxuwz.zjh.mxbbx.entity.Sub;
import com.gxuwz.zjh.mxbbx.service.IMechanismService;
import com.gxuwz.zjh.mxbbx.service.ISubService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * (S201_04)  机构级别 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/mechanism")
public class HoMechanismController {

    @Autowired
    private IMechanismService iMechanismService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findMechanismAll")
    public ModelAndView findMechanismAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                         @Param("mechanismId") String mechanismId,
                                   HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Mechanism> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("mechanism_id");
        // 进行模糊查询!!!
        if(mechanismId != null && mechanismId != ""){
            wrapper.like("mechanism_id", mechanismId);
            request.getSession().setAttribute("mechanismId", mechanismId);
        }else {
            request.getSession().setAttribute("mechanismId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Mechanism> mechanismIPage = iMechanismService.selectMechanismPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)mechanismIPage.getPages()];
        for(int i=0; i< (int)mechanismIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+mechanismIPage.getTotal());
        System.out.println("总页数"+mechanismIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)mechanismIPage.getPages());
        List<Mechanism> mechanismList = mechanismIPage.getRecords();
        System.out.println("mechanismList = "+mechanismList);
        modelAndView.addObject("mechanismList", mechanismList);
        modelAndView.setViewName("mechanism/mechanism_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertMechanism")
    public ModelAndView insertMechanism(ModelAndView modelAndView) {
        modelAndView.setViewName("mechanism/mechanism_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addMechanism")
    public ModelAndView addMechanism(ModelAndView modelAndView, Mechanism mechanism, HttpServletRequest request) {
        Mechanism mechanism1 = iMechanismService.findMechanismById(mechanism);
        if(mechanism1 == null){
            iMechanismService.addMechanism(mechanism);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("mechanism/mechanism_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateMechanism")
    public ModelAndView updateMechanism(ModelAndView modelAndView, String mechanismId) {
        Mechanism mechanism = new Mechanism();
        mechanism.setMechanismId(mechanismId);
        Mechanism mechanism1 = iMechanismService.findMechanismById(mechanism);
        modelAndView.addObject("mechanism", mechanism1);
        modelAndView.setViewName("mechanism/mechanism_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editMechanism")
    public ModelAndView editMechanism(ModelAndView modelAndView, Mechanism mechanism, HttpServletRequest request) {
        iMechanismService.updateMechanismById(mechanism);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("mechanism/mechanism_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteMechanismById")
    public ModelAndView deleteSubById(ModelAndView modelAndView, HttpServletRequest request, String mechanismId) {
        Mechanism mechanism = new Mechanism();
        mechanism.setMechanismId(mechanismId);
        iMechanismService.deleteMechanismById(mechanism);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("mechanism/mechanism_list");
        return modelAndView;
    }

}
