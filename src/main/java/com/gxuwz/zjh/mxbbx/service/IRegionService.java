package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Region;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 行政区域信息模型 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IRegionService extends IService<Region> {

    // 根据 id 查询对应用户信息
    Region findRegionById(Region region);

    // 查询全部用户信息
    List<Region> findRegionAll();

    //自定义sql 分页
    IPage<Region> selectRegionPage(Page<Region> page, @Param(Constants.WRAPPER) Wrapper<Region> wrapper);

    // 添加用户信息
    void addRegion(Region region);

    // 修改用户信息
    void updateRegionById(Region region);

    // 删除用户信息
    void deleteRegionById(Region region);

}
