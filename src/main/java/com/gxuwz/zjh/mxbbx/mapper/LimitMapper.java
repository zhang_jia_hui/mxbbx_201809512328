package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Limit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-19
 */
@Mapper
@Repository
public interface LimitMapper extends BaseMapper<Limit> {

    // 根据 id 查询对应用户信息
    Limit findLimitById(Limit limit);

    // 根据 grade 查询对应用户信息
    Integer findLimitByGrades(Limit limit);

    // 查询全部用户信息
    List<Limit> findLimitAll();

    //自定义sql 分页
    IPage<Limit> selectLimitPage(Page<Limit> page, @Param(Constants.WRAPPER) Wrapper<Limit> wrapper);

    // 添加用户信息
    void addLimit(Limit limit);

    // 修改用户信息
    void updateLimitById(Limit limit);

    // 删除用户信息
    void deleteLimitById(Limit limit);

}
