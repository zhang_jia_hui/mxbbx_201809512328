package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxuwz.zjh.mxbbx.entity.ChronicReim;
import com.gxuwz.zjh.mxbbx.entity.Echarts;
import com.gxuwz.zjh.mxbbx.entity.Family;
import com.gxuwz.zjh.mxbbx.mapper.ChronicReimMapper;
import com.gxuwz.zjh.mxbbx.service.IChronicReimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Service
public class ChronicReimServiceImpl extends ServiceImpl<ChronicReimMapper, ChronicReim> implements IChronicReimService {

    @Autowired
    private ChronicReimMapper chronicReimMapper;

    @Override
    public ChronicReim findChronicReimById(ChronicReim chronicReim) {
        return chronicReimMapper.findChronicReimById(chronicReim);
    }

    @Override
    public List<ChronicReim> findChronicReimAll() {
        return chronicReimMapper.findChronicReimAll();
    }

    @Override
    public List<ChronicReim> findChronicReimByNumber(ChronicReim chronicReim) {
        return chronicReimMapper.findChronicReimByNumber(chronicReim);
    }

    @Override
    public List<ChronicReim> findChronicReimByChronic(ChronicReim chronicReim) {
        return chronicReimMapper.findChronicReimByChronic(chronicReim);
    }

    @Override
    public IPage<ChronicReim> selectChronicReimPage(Page<ChronicReim> page, Wrapper<ChronicReim> wrapper) {
        return chronicReimMapper.selectChronicReimPage(page, wrapper);
    }

    @Override
    public void addChronicReim(ChronicReim chronicReim) {
        chronicReimMapper.addChronicReim(chronicReim);
    }

    @Override
    public void updateChronicReimById(ChronicReim chronicReim) {
        chronicReimMapper.updateChronicReimById(chronicReim);
    }

    @Override
    public void deleteChronicReimById(ChronicReim chronicReim) {
        chronicReimMapper.deleteChronicReimById(chronicReim);
    }

    @Override
    public List<ChronicReim> findChronicReimByChronicId() {
        return chronicReimMapper.findChronicReimByChronicId();
    }

    @Override
    public List<Echarts> findEcharts(Family family) {
        return chronicReimMapper.findEcharts(family);
    }
}
