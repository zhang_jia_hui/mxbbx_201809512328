package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxuwz.zjh.mxbbx.entity.ChronicReim;
import com.gxuwz.zjh.mxbbx.entity.Echarts;
import com.gxuwz.zjh.mxbbx.entity.Family;
import com.gxuwz.zjh.mxbbx.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
public interface IChronicReimService extends IService<ChronicReim> {

    // 根据 id 查询对应用户信息
    ChronicReim findChronicReimById(ChronicReim chronicReim);

    // 查询全部用户信息
    List<ChronicReim> findChronicReimAll();

    // 根据 number 查询对应用户信息
    List<ChronicReim> findChronicReimByNumber(ChronicReim chronicReim);

    // 根据 chronic 查询对应用户信息
    List<ChronicReim> findChronicReimByChronic(ChronicReim chronicReim);

    //自定义sql 分页
    IPage<ChronicReim> selectChronicReimPage(Page<ChronicReim> page, @Param(Constants.WRAPPER) Wrapper<ChronicReim> wrapper);

    // 添加用户信息
    void addChronicReim(ChronicReim chronicReim);

    // 修改用户信息
    void updateChronicReimById(ChronicReim chronicReim);

    // 删除用户信息
    void deleteChronicReimById(ChronicReim chronicReim);

    // 查询已经存在的慢性病类别
    List<ChronicReim> findChronicReimByChronicId();

    // 根据地名查询
    List<Echarts> findEcharts(Family family);

}
