package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.mapper.AgencyMapper;
import com.gxuwz.zjh.mxbbx.mapper.ChronicMapper;
import com.gxuwz.zjh.mxbbx.service.IChronicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class ChronicServiceImpl extends ServiceImpl<ChronicMapper, Chronic> implements IChronicService {

    @Autowired
    private ChronicMapper chronicMapper;

    @Override
    public Chronic findChronicById(Chronic chronic) {
        return chronicMapper.findChronicById(chronic);
    }

    @Override
    public List<Chronic> findChronicAll() {
        return chronicMapper.findChronicAll();
    }

    @Override
    public IPage<Chronic> selectChronicPage(Page<Chronic> page, Wrapper<Chronic> wrapper) {
        return chronicMapper.selectChronicPage(page, wrapper);
    }

    @Override
    public void addChronic(Chronic chronic) {
        chronicMapper.addChronic(chronic);
    }

    @Override
    public void updateChronicById(Chronic chronic) {
        chronicMapper.updateChronicById(chronic);
    }

    @Override
    public void deleteChronicById(Chronic chronic) {
        chronicMapper.deleteChronicById(chronic);
    }
}
