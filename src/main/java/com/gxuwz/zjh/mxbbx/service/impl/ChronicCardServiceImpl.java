package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.ChronicCard;
import com.gxuwz.zjh.mxbbx.mapper.ChronicCardMapper;
import com.gxuwz.zjh.mxbbx.service.IChronicCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 慢性病证 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@Service
public class ChronicCardServiceImpl extends ServiceImpl<ChronicCardMapper, ChronicCard> implements IChronicCardService {

    @Autowired
    private ChronicCardMapper chronicCardMapper;

    @Override
    public ChronicCard findChronicCardById(ChronicCard chronicCard) {
        return chronicCardMapper.findChronicCardById(chronicCard);
    }

    @Override
    public List<ChronicCard> findChronicCardAll() {
        return chronicCardMapper.findChronicCardAll();
    }

    @Override
    public List<ChronicCard> findChronicCard(ChronicCard chronicCard) {
        return chronicCardMapper.findChronicCard(chronicCard);
    }

    @Override
    public List<ChronicCard> nowTimesByCard(Date times) {
        return chronicCardMapper.nowTimesByCard(times);
    }

    @Override
    public IPage<ChronicCard> selectChronicCardPage(Page<ChronicCard> page, Wrapper<ChronicCard> wrapper) {
        return chronicCardMapper.selectChronicCardPage(page, wrapper);
    }

    @Override
    public void addChronicCard(ChronicCard chronicCard) {
        chronicCardMapper.addChronicCard(chronicCard);
    }

    @Override
    public void updateChronicCardById(ChronicCard chronicCard) {
        chronicCardMapper.updateChronicCardById(chronicCard);
    }

    @Override
    public void deleteChronicCardById(ChronicCard chronicCard) {
        chronicCardMapper.deleteChronicCardById(chronicCard);
    }
}
