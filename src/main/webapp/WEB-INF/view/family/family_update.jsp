<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/family/editFamily">
            <div class="form-group">
                <div class="label">
                    <label>家庭编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${family.familyId}" name="familyId" data-validate="required:建议不修改" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>县级编码：</label>
                </div>
                <div class="field">
                    <select id="levelId" style="width:200px" name="levelId" class="input w50">
                        <c:forEach items="${regionList}" var="map">
                            <c:if test="${fn:length(map.getRegionId()) == 6}">
                                <option value="${map.getRegionId()}"
                                        <c:if test="${map.getRegionId() eq family.levelId}">
                                            selected
                                        </c:if>
                                >${map.getRegionName()}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <script type="text/javascript">
              $("#levelId").blur(function(){   // 根据选择的省查询下面的市
                var levelId = $('#levelId option:selected').val();  // 获取选择的值
                $("#shipId").empty(); // 清空
                <c:forEach items="${regionList}" var="map" varStatus="item">
                <c:if test="${fn:length(map.getRegionId()) == 8}" >
                if("${map.getRegionId()}".indexOf(levelId) != -1){
                  //先创建好select里面的option元素
                  var option = document.createElement("option");
                  //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                  $(option).val("${map.getRegionId()}");
                  //给option的text赋值,这就是你点开下拉框能够看到的东西
                  $(option).text("${map.getRegionName()}");
                  //获取select 下拉框对象,并将option添加进select
                  $('#shipId').append(option);
                }
                </c:if>
                </c:forEach>
              })
            </script>

            <div class="form-group">
                <div class="label">
                    <label>乡镇编码：</label>
                </div>
                <div class="field">
                    <select id="shipId" style="width:200px" name="shipId" class="input w50">
                        <c:forEach items="${regionList}" var="map">
                            <c:if test="${fn:length(map.getRegionId()) == 8}">
                                <option value="${map.getRegionId()}"
                                        <c:if test="${map.getRegionId() eq family.levelId}">
                                            selected
                                        </c:if>
                                >${map.getRegionName()}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <script type="text/javascript">
              $("#shipId").blur(function(){   // 根据选择的省查询下面的市
                var shipId = $('#shipId option:selected').val();  // 获取选择的值
                $("#villageId").empty(); // 清空
                <c:forEach items="${regionList}" var="map" varStatus="item">
                <c:if test="${fn:length(map.getRegionId()) == 10}" >
                if("${map.getRegionId()}".indexOf(shipId) != -1){
                  //先创建好select里面的option元素
                  var option = document.createElement("option");
                  //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                  $(option).val("${map.getRegionId()}");
                  //给option的text赋值,这就是你点开下拉框能够看到的东西
                  $(option).text("${map.getRegionName()}");
                  //获取select 下拉框对象,并将option添加进select
                  $('#villageId').append(option);
                }
                </c:if>
                </c:forEach>
              })
            </script>

            <div class="form-group">
                <div class="label">
                    <label>村编码：</label>
                </div>
                <div class="field">
                    <select id="villageId" style="width:200px" name="villageId" class="input w50">
                        <c:forEach items="${regionList}" var="map">
                            <c:if test="${fn:length(map.getRegionId()) == 10}">
                                <option value="${map.getRegionId()}"
                                        <c:if test="${map.getRegionId() eq family.villageId}">
                                            selected
                                        </c:if>
                                >${map.getRegionName()}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <script type="text/javascript">
              $("#villageId").blur(function(){   // 根据选择的省查询下面的市
                var villageId = $('#villageId option:selected').val();  // 获取选择的值
                $("#groupId").empty(); // 清空
                <c:forEach items="${regionList}" var="map" varStatus="item">
                <c:if test="${fn:length(map.getRegionId()) == 12}" >
                if("${map.getRegionId()}".indexOf(villageId) != -1){
                  //先创建好select里面的option元素
                  var option = document.createElement("option");
                  //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                  $(option).val("${map.getRegionId()}");
                  //给option的text赋值,这就是你点开下拉框能够看到的东西
                  $(option).text("${map.getRegionName()}");
                  //获取select 下拉框对象,并将option添加进select
                  $('#groupId').append(option);
                }
                </c:if>
                </c:forEach>
              })
            </script>

            <div class="form-group">
                <div class="label">
                    <label>组编号：</label>
                </div>
                <div class="field">
                    <select id="groupId" style="width:200px" name="groupId" class="input w50">
                        <c:forEach items="${regionList}" var="map">
                            <c:if test="${fn:length(map.getRegionId()) == 12}">
                                <option value="${map.getRegionId()}"
                                        <c:if test="${map.getRegionId() eq family.groupId}">
                                            selected
                                        </c:if>
                                >${map.getRegionName()}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭人口数：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" value="${family.personNumber}" name="personNumber" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>农业人口数：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" value="${family.agrNumber}" name="agrNumber" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭住址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${family.address}" name="address" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>创建档案时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${family.establishTime}"
                           id="establishTime" name="establishTime" />
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#establishTime', //指定元素
                  type: 'datetime'
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label>登记员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${family.registrant}" name="registrant" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>户主：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${family.holder}" name="holder" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>户主身份证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${family.number}" name="number" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/family/findFamilyAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
