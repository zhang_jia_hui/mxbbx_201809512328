package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 慢病政策数据模型
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Policy") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Policy对象", description="慢病政策数据模型")
public class Policy implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "慢病年度ID")
    private String policyId;

    @ApiModelProperty(value = "年度")
    private String year;

    @ApiModelProperty(value = "封顶线")
    private String cappLine;

    @ApiModelProperty(value = "报销比例")
    private String reimRatio;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
