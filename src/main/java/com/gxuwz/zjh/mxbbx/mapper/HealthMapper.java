package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.gxuwz.zjh.mxbbx.entity.Health;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * (S201_03)  卫生机构（组织）类别 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface HealthMapper extends BaseMapper<Health> {

    // 根据 id 查询对应用户信息
    Health findHealthById(Health health);

    // 查询全部用户信息
    List<Health> findHealthAll();

    //自定义sql 分页
    IPage<Health> selectHealthPage(Page<Health> page, @Param(Constants.WRAPPER) Wrapper<Health> wrapper);

    // 添加用户信息
    void addHealth(Health health);

    // 修改用户信息
    void updateHealthById(Health health);

    // 删除用户信息
    void deleteHealthById(Health health);

}
