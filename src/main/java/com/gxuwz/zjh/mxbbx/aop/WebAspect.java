package com.gxuwz.zjh.mxbbx.aop;

import com.gxuwz.zjh.mxbbx.entity.User;
import org.aspectj.lang.annotation.Aspect;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Aspect
public class WebAspect {

    @Before(value="within(com.gxuwz.zjh.mxbbx.controller.UserController)")
    public void before() throws ServletException, IOException {
        System.out.println("============前置通知=============");
        String notice = "用户已登录！";
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        HttpServletResponse response = attributes.getResponse();
        User user1 = (User) request.getSession().getAttribute("user");
        if (user1 == null) {
            notice = "用户未登录！！！";
            System.out.println("通知：" + notice);
            request.getSession().setAttribute("result", "repeat");
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
        System.out.println("user1 = " + user1);
        System.out.println("通知：" + notice);
        System.out.println("============前置结束=============");
    }

}