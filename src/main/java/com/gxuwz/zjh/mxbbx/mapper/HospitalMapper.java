package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Hospital;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Level;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 医院表 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface HospitalMapper extends BaseMapper<Hospital> {

    // 根据 id 查询对应用户信息
    Hospital findHospitalById(Hospital hospital);

    // 查询全部用户信息
    List<Hospital> findHospitalAll();

    //自定义sql 分页
    IPage<Hospital> selectHospitalPage(Page<Hospital> page, @Param(Constants.WRAPPER) Wrapper<Hospital> wrapper);

    // 添加用户信息
    void addHospital(Hospital hospital);

    // 修改用户信息
    void updateHospitalById(Hospital hospital);

    // 删除用户信息
    void deleteHospitalById(Hospital hospital);

}
