package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Health;
import com.gxuwz.zjh.mxbbx.entity.Level;
import com.gxuwz.zjh.mxbbx.service.IHealthService;
import com.gxuwz.zjh.mxbbx.service.ILevelService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * (S201_06)  地域级别 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/level")
public class HoLevelController {

    @Autowired
    private ILevelService iLevelService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findLevelAll")
    public ModelAndView findLevelAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                     @Param("levelId") String levelId,
                                      HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Level> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("level_id");
        // 进行模糊查询!!!
        if(levelId != null && levelId != ""){
            wrapper.like("level_id", levelId);
            request.getSession().setAttribute("levelId", levelId);
        }else {
            request.getSession().setAttribute("levelId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Level> levelIPage = iLevelService.selectLevelPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)levelIPage.getPages()];
        for(int i=0; i< (int)levelIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+levelIPage.getTotal());
        System.out.println("总页数"+levelIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)levelIPage.getPages());
        List<Level> levelList = levelIPage.getRecords();
        System.out.println("levelList = "+levelList);
        modelAndView.addObject("levelList", levelList);
        modelAndView.setViewName("level/level_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertLevel")
    public ModelAndView insertLevel(ModelAndView modelAndView) {
        modelAndView.setViewName("level/level_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addLevel")
    public ModelAndView addLevel(ModelAndView modelAndView, Level level, HttpServletRequest request) {
        Level level1 = iLevelService.findLevelById(level);
        if(level1 == null){
            iLevelService.addLevel(level);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("level/level_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateLevel")
    public ModelAndView updateLevel(ModelAndView modelAndView, int levelId) {
        Level level = new Level();
        level.setLevelId(levelId);
        Level level1 = iLevelService.findLevelById(level);
        modelAndView.addObject("level", level1);
        modelAndView.setViewName("level/level_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editLevel")
    public ModelAndView editLevel(ModelAndView modelAndView, Level level, HttpServletRequest request) {
        iLevelService.updateLevelById(level);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("level/level_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteLevelById")
    public ModelAndView deleteLevelById(ModelAndView modelAndView, HttpServletRequest request, int levelId) {
        Level level = new Level();
        level.setLevelId(levelId);
        iLevelService.deleteLevelById(level);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("level/level_list");
        return modelAndView;
    }

}
