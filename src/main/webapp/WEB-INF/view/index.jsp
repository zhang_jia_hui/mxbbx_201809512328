<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="./common/common.jsp"%>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title>慢性病报销系统</title>
<link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
<link rel="stylesheet" href="<%=basePath %>/css/admin.css">
<%--<link rel="stylesheet" href="<%=basePath %>/css/xadmin.css">--%>
<link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
<script src="<%=basePath %>/js/jquery.js"></script>
<%--<script src="<%=basePath %>/lib/layui/layui.js"></script>--%>
<%--<script src="<%=basePath %>/js/xadmin.js"></script>--%>
</head>
<script type="text/javascript">
	function out(){
		var result = confirm("确定要退出吗？");
		if(result){
			location.href="<%=basePath %>/loginEnd";
		}
	}
</script>

<body style="background-color:#f2f9fd;">
	<div class="header bg-main">
		<div class="logo margin-big-left fadein-top">
			<h1>
				<img src="<%=basePath %>/images/logo.jpg" class="radius-circle rotate-hover"
					height="50" alt="" />&nbsp;&nbsp;慢性病报销系统
			</h1>
		</div>
		<div class="head-l">
            <a href="#" style="color:#FFF">
                <span class="icon-times-circle-o"></span> 登录时长：<span id="time"></span> </a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#" style="color:#FFF">
				<span class="icon-user"></span> 欢迎 ${user.userId}&nbsp;&nbsp;${role.roleName}</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a class="button button-little bg-green" href="#">
				<span class="icon-home"></span> 首页</a>&nbsp;&nbsp;
			<a class="button button-little bg-red" style="margin-right: 10px" href="javascript:onclick:out()">
				<span class="icon-power-off"></span> 退出登录</a>
			<a class="button button-little border-dot" style="margin:6px;"
			   href="javascript:window.opener=null;window.open('','_self');window.close();">
				<span class="icon-power-off"></span> 关闭页面</a>
		</div>
	</div>
	<div class="leftnav">
		<div class="leftnav-title">
			<strong><span class="icon-list"></span>菜单列表</strong>
		</div>
		<c:forEach items="${limits}" var="map">
			<c:choose>
				<c:when test="${map == 'Data'}">
					<h2>
						<span class="icon-send"></span>系统管理
					</h2>
				</c:when>
			</c:choose>
		</c:forEach>

		<ul style="display:block">
		<c:forEach items="${limits}" var="map">
			<c:choose>
				<c:when test="${map == 'User'}">
					<li><a href="<%=basePath %>mxbbx/user/findUserAll" target="right"><span
							class="icon-caret-right"></span>用户管理</a></li>
				</c:when>
				<c:when test="${map == 'Role'}">
					<li><a href="<%=basePath %>mxbbx/role/findRoleAll" target="right"><span
							class="icon-caret-right"></span>角色管理</a></li>
				</c:when>
				<c:when test="${map == 'Limit'}">
					<li><a href="<%=basePath %>mxbbx/limit/findLimitAll" target="right"><span
							class="icon-caret-right"></span>权限管理</a></li>
				</c:when>
                <c:when test="${map == 'Region'}">
                    <li><a href="<%=basePath %>mxbbx/region/findRegionAll" target="right"><span
                            class="icon-caret-right"></span>行政区域管理</a></li>
                </c:when>
                <c:when test="${map == 'Agency'}">
                    <li><a href="<%=basePath %>mxbbx/agency/findAgencyAll" target="right"><span
                            class="icon-caret-right"></span>农合机构管理</a></li>
                </c:when>
                <c:when test="${map == 'Chronic'}">
                    <li><a href="<%=basePath %>mxbbx/chronic/findChronicAll" target="right"><span
                            class="icon-caret-right"></span>慢病分类管理</a></li>
                </c:when>
                <c:when test="${map == 'Policy'}">
                    <li><a href="<%=basePath %>mxbbx/policy/findPolicyAll" target="right"><span
                            class="icon-caret-right"></span>慢病政策管理</a></li>
                </c:when>
			</c:choose>
		</c:forEach>
		</ul>


		<c:forEach items="${limits}" var="map">
			<c:choose>
				<c:when test="${map == 'Hospitals'}">
					<h2>
						<span class="icon-hospital-o"></span>医院信息管理
					</h2>
				</c:when>
			</c:choose>
		</c:forEach>

		<%-- display: none 收缩 --%>
		<%-- display: block 打开 --%>
		<ul style="display:none">
			<c:forEach items="${limits}" var="map">
				<c:choose>
					<c:when test="${map == 'Economic'}">
						<li><a href="<%=basePath %>mxbbx/economic/findEconomicAll" target="right"><span
								class="icon-caret-right"></span>机构所属经济类型管理</a></li>
					</c:when>
					<c:when test="${map == 'Sub'}">
						<li><a href="<%=basePath %>mxbbx/sub/findSubAll" target="right"><span
								class="icon-caret-right"></span>隶属关系管理</a></li>
					</c:when>
					<c:when test="${map == 'Health'}">
						<li><a href="<%=basePath %>mxbbx/health/findHealthAll" target="right"><span
								class="icon-caret-right"></span>卫生机构类别管理</a></li>
					</c:when>
					<c:when test="${map == 'Mechanism'}">
						<li><a href="<%=basePath %>mxbbx/mechanism/findMechanismAll" target="right"><span
								class="icon-caret-right"></span>机构级别管理</a></li>
					</c:when>
					<c:when test="${map == 'Level'}">
						<li><a href="<%=basePath %>mxbbx/level/findLevelAll" target="right"><span
								class="icon-caret-right"></span>地域级别管理</a></li>
					</c:when>
					<c:when test="${map == 'Hospital'}">
						<li><a href="<%=basePath %>mxbbx/hospital/findHospitalAll" target="right"><span
								class="icon-caret-right"></span>医院信息管理</a></li>
					</c:when>
				</c:choose>
			</c:forEach>
		</ul>


		<c:forEach items="${limits}" var="map">
			<c:choose>
				<c:when test="${map == 'Participation'}">
					<h2>
						<span class="icon-pencil"></span>参合人员管理
					</h2>
				</c:when>
			</c:choose>
		</c:forEach>

		<ul style="display:none">
			<c:forEach items="${limits}" var="map">
				<c:choose>
					<c:when test="${map == 'Family'}">
						<li><a href="<%=basePath %>mxbbx/family/findFamilyAll" target="right"><span
								class="icon-caret-right"></span>家庭档案信息管理</a></li>
					</c:when>
					<c:when test="${map == 'Families'}">
						<li><a href="<%=basePath %>mxbbx/families/findFamiliesAll" target="right"><span
								class="icon-caret-right"></span>参合农民档案管理</a></li>
					</c:when>
					<c:when test="${map == 'Card'}">
						<li><a href="<%=basePath %>mxbbx/card/findCardAll" target="right"><span
								class="icon-caret-right"></span>慢性病证管理</a></li>
					</c:when>
					<c:when test="${map == 'Personnel'}">
						<li><a href="<%=basePath %>mxbbx/personnel/findPersonnelAll" target="right"><span
								class="icon-caret-right"></span>人员信息管理</a></li>
					</c:when>
				</c:choose>
			</c:forEach>
		</ul>

        <c:forEach items="${limits}" var="map">
            <c:choose>
                <c:when test="${map == 'Money'}">
                    <h2>
                        <span class="icon-money"></span>缴费管理
                    </h2>
                </c:when>
            </c:choose>
        </c:forEach>

		<ul style="display:none">
			<c:forEach items="${limits}" var="map">
				<c:choose>
					<c:when test="${map == 'Pay'}">
						<li><a href="<%=basePath %>mxbbx/pay/findPayAll" target="right"><span
								class="icon-caret-right"></span>参合缴费标准</a></li>
					</c:when>
					<c:when test="${map == 'Pays'}">
						<li><a href="<%=basePath %>mxbbx/pays/findPaysAll" target="right"><span
								class="icon-caret-right"></span>参合缴费登记记录</a></li>
					</c:when>
					<c:when test="${map == 'ChronicReim'}">
						<li><a href="<%=basePath %>mxbbx/reim/findReimAll" target="right"><span
								class="icon-caret-right"></span>慢性病报销信息管理</a></li>
					</c:when>
				</c:choose>
			</c:forEach>
		</ul>


		<h2>
			<span class="icon-user"></span>个人信息管理
		</h2>
		<%-- display: none 收缩 --%>
		<%-- display: block 打开 --%>
		<ul style="display: none;">
			<li><a href="<%=basePath %>mxbbx/user/editUserAndUpdate" target="right"><span
					class="icon-caret-right"></span>个人信息</a></li>
		</ul>

		<%-- 登录默认打开界面 --%>
		<div class="admin">
			<iframe scrolling="auto" rameborder="0" src="<%=basePath %>/hello"
					name="right" width="100%" height="100%"></iframe>
		</div>


	</div>
	<script type="text/javascript">
		$(function() {
			$(".leftnav h2").click(function() {
				$(this).next().slideToggle(200);
				$(this).toggleClass("on");
			});
			$(".leftnav ul li a").click(function() {
				$("#a_leader_txt").text($(this).text());
				$(".leftnav ul li a").removeClass("on");
				$(this).addClass("on");
			});

		});
	</script>
	<ul class="bread">
		<li><a href="{:U('Index/info')}" target="right" class="icon-home">
				首页</a></li>
		<li><a href="##" id="a_leader_txt">欢迎界面</a></li>
		<li><b>当前语言：</b><span style="color:red;">中文</span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;切换语言：<a href="##">中文</a> &nbsp;&nbsp;<a
			href="##">英文</a></li>
	</ul>

</body>
<script type="text/javascript">
	window.onload=function(){
		if("none_no" == "${result}"){
			alert('登录超时!!！');
		}
	};
</script>
<script>
	var i = 0;
	function show(i){
		var result = parseInt(i/60)+"分"+(i%60)+"秒" ;
		document.getElementById("time").innerHTML = result;
	}
	show();
	setInterval("show(i++)",1000);
</script>
</html>