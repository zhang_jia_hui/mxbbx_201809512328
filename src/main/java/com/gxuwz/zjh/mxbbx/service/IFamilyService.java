package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Family;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 家庭档案信息表 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IFamilyService extends IService<Family> {

    // 根据 id 查询对应用户信息
    Family findFamilyById(Family family);

    // 查询全部用户信息
    List<Family> findFamilyAll();

    // 根据 地域 查询对应用户信息
    List<Family> findFamilyByRegion(Family family);

    //自定义sql 分页
    IPage<Family> selectFamilyPage(Page<Family> page, @Param(Constants.WRAPPER) Wrapper<Family> wrapper);

    // 添加用户信息
    void addFamily(Family family);

    // 修改用户信息
    void updateFamilyById(Family family);

    // 删除用户信息
    void deleteFamilyById(Family family);

}
