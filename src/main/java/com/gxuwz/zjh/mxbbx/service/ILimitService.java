package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Limit;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-19
 */
public interface ILimitService extends IService<Limit> {

    // 根据 id 查询对应用户信息
    Limit findLimitById(Limit limit);

    // 根据 grade 查询对应用户信息
    Integer findLimitByGrades(Limit limit);

    // 查询全部用户信息
    List<Limit> findLimitAll();

    //自定义sql 分页
    IPage<Limit> selectLimitPage(Page<Limit> page, @Param(Constants.WRAPPER) Wrapper<Limit> wrapper);

    // 添加用户信息
    void addLimit(Limit limit);

    // 修改用户信息
    void updateLimitById(Limit limit);

    // 删除用户信息
    void deleteLimitById(Limit limit);

}
