package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IChronicService extends IService<Chronic> {

    // 根据id查询信息
    Chronic findChronicById(Chronic chronic);

    // 查询全部信息
    List<Chronic> findChronicAll();

    //自定义sql 分页
    IPage<Chronic> selectChronicPage(Page<Chronic> page, @Param(Constants.WRAPPER) Wrapper<Chronic> wrapper);

    // 添加信息
    void addChronic(Chronic chronic);

    // 修改信息
    void updateChronicById(Chronic chronic);

    // 删除信息
    void deleteChronicById(Chronic chronic);

}
