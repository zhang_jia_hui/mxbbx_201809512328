package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Level;
import com.gxuwz.zjh.mxbbx.mapper.LevelMapper;
import com.gxuwz.zjh.mxbbx.service.ILevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * (S201_06)  地域级别 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class LevelServiceImpl extends ServiceImpl<LevelMapper, Level> implements ILevelService {

    @Autowired
    private LevelMapper levelMapper;

    @Override
    public Level findLevelById(Level level) {
        return levelMapper.findLevelById(level);
    }

    @Override
    public List<Level> findLevelAll() {
        return levelMapper.findLevelAll();
    }

    @Override
    public IPage<Level> selectLevelPage(Page<Level> page, Wrapper<Level> wrapper) {
        return levelMapper.selectLevelPage(page, wrapper);
    }

    @Override
    public void addLevel(Level level) {
        levelMapper.addLevel(level);
    }

    @Override
    public void updateLevelById(Level level) {
        levelMapper.updateLevelById(level);
    }

    @Override
    public void deleteLevelById(Level level) {
        levelMapper.deleteLevelById(level);
    }
}
