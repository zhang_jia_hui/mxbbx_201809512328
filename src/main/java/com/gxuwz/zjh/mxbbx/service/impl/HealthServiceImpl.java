package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Health;
import com.gxuwz.zjh.mxbbx.mapper.HealthMapper;
import com.gxuwz.zjh.mxbbx.service.IHealthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * (S201_03)  卫生机构（组织）类别 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class HealthServiceImpl extends ServiceImpl<HealthMapper, Health> implements IHealthService {

    @Autowired
    private HealthMapper healthMapper;

    @Override
    public Health findHealthById(Health health) {
        return healthMapper.findHealthById(health);
    }

    @Override
    public List<Health> findHealthAll() {
        return healthMapper.findHealthAll();
    }

    @Override
    public IPage<Health> selectHealthPage(Page<Health> page, Wrapper<Health> wrapper) {
        return healthMapper.selectHealthPage(page, wrapper);
    }

    @Override
    public void addHealth(Health health) {
        healthMapper.addHealth(health);
    }

    @Override
    public void updateHealthById(Health health) {
        healthMapper.updateHealthById(health);
    }

    @Override
    public void deleteHealthById(Health health) {
        healthMapper.deleteHealthById(health);
    }
}
