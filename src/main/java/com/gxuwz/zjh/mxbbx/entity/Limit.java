package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-19
 */
@Data
@Alias("Limit") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Limit对象", description="")
public class Limit implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    @ApiModelProperty(value = "权限编号")
    private String limitId;

    @ApiModelProperty(value = "权限等级")
    private String limitGrade;

    @ApiModelProperty(value = "权限描述")
    private String limitText;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
