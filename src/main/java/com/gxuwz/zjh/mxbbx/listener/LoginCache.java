package com.gxuwz.zjh.mxbbx.listener;

import com.gxuwz.zjh.mxbbx.entity.User;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * 用户信息存储实现
 *
 */
public class LoginCache {

    private static LoginCache instance = new LoginCache();

    //单例模型
    private LoginCache() {

    }

    public static LoginCache getInstance() {
        return instance;
    }

    private Map<Object,String>LoginUserSession=new HashMap<Object,String>();
    //key值：登录用户登录名，value值：登录用户sessionId
    private Map<String,HttpSession>LoginSession=new HashMap<String,HttpSession>();
    //key值：登录用户sessionId，value值：登录用户session对象

    /**
     *  通过登录名获取对应登录用户的sessionId
     */
    public String getSessionIdByUsername(User user) {
        return LoginUserSession.get(user);
    }

    /**
     * 通过sessionId获取对应的session对象
     */
    public HttpSession getSessionBySessionId(String sessionId) {
        return LoginSession.get(sessionId);
    }

    /**
     * 存储登录名与相应的登录sessionId至缓存对象
     */
    public void setSessionIdByUsername(User user, String sessionId) {
        LoginUserSession.put(user, sessionId);
    }

    /**
     * 存储sessionId与对应的session对象至缓存对象
     */
    public void setSessionBySessionId(String sessionId, HttpSession session) {
        LoginSession.put(sessionId, session);
    }
}