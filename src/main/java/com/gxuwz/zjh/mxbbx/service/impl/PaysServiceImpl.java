package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import com.gxuwz.zjh.mxbbx.entity.Pays;
import com.gxuwz.zjh.mxbbx.mapper.PayMapper;
import com.gxuwz.zjh.mxbbx.mapper.PaysMapper;
import com.gxuwz.zjh.mxbbx.service.IPayService;
import com.gxuwz.zjh.mxbbx.service.IPaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Service
public class PaysServiceImpl extends ServiceImpl<PaysMapper, Pays> implements IPaysService {

    @Autowired
    private PaysMapper paysMapper;


    @Override
    public Pays findPaysById(Pays pays) {
        return paysMapper.findPaysById(pays);
    }

    @Override
    public Pays findPaysByFamily(Pays pays) {
        return paysMapper.findPaysByFamily(pays);
    }

    @Override
    public List<Pays> findPaysAll() {
        return paysMapper.findPaysAll();
    }

    @Override
    public IPage<Pays> selectPaysPage(Page<Pays> page, Wrapper<Pays> wrapper) {
        return paysMapper.selectPaysPage(page, wrapper);
    }

    @Override
    public void addPays(Pays pays) {
        paysMapper.addPays(pays);
    }

    @Override
    public void updatePaysById(Pays pays) {
        paysMapper.updatePaysById(pays);
    }

    @Override
    public void deletePaysById(Pays pays) {
        paysMapper.deletePaysById(pays);
    }
}
