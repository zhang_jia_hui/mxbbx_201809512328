<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 批量新增权限信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/limit/addLimits">
            <table class="table table-hover text-center" id ="addtest">
                <tr>
                    <th style="width:25%">权限ID</th>
                    <th style="width:25%">权限级别</th>
                    <th style="width:25%">权限描述</th>
                </tr>
                <tr>
                    <td>
                        <div class="form-group">
                            <div class="field" style="width:100%;">
                                <input style="width:60%;margin-left:20%;" type="text" class="input w50" placeholder="请输入权限名称(例: edit)" value="edit" name="limitId" />
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="field" style="width:100%;">
                                <input style="width:60%;margin-left:20%;" type="text" class="input w50" placeholder="请输入权限等级(例: -1)" value="-1" name="limitGrade" />
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="field" style="width:100%;">
                                <input style="width:60%;margin-left:20%;" type="text" class="input w50" placeholder="请输入权限描述(例: 用户管理)" value="用户管理" name="limitText" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="form-group">
                            <div class="field" style="width:100%;">
                                <input style="width:60%;margin-left:20%;" type="text" class="input w50" placeholder="请输入权限名称(例: edit)" value="edit" name="limitId" />
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="field" style="width:100%;">
                                <input style="width:60%;margin-left:20%;" type="text" class="input w50" placeholder="请输入权限等级(例: -1)" value="-1" name="limitGrade" />
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="field" style="width:100%;">
                                <input style="width:60%;margin-left:20%;" type="text" class="input w50" placeholder="请输入权限描述(例: 用户管理)" value="用户管理" name="limitText" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <script type="text/javascript">
              var index = 1;
              function add() {
                var addstep = index + 1;
                $("#addtest tr:last").after("<tr>"
                    + "<td>"
                    + "<div class=\"form-group\">"
                    + "<div class=\"field\" style=\"width:100%;\">"
                    + "<input style=\"width:60%;margin-left:20%;\" type=\"text\" class=\"input w50\" placeholder=\"请输入权限名称(例: edit)\""
                    + "onclick=\"JavaScript:this.value=''\" value=\"edit\" name=\"limitId\" data-validate=\"required:必填\" />"
                    + "</div>"
                    + "</div>"
                    + "</td>"
                    + "<td>"
                    + "<div class=\"form-group\">"
                    + "<div class=\"field\" style=\"width:100%;\">"
                    + "<input style=\"width:60%;margin-left:20%;\" type=\"text\" class=\"input w50\" placeholder=\"请输入权限等级(例: -1)\""
                    + "onclick=\"JavaScript:this.value=''\" value=\"-1\" name=\"limitGrade\" data-validate=\"required:必填\" />"
                    + "</div>"
                    + "</div>"
                    + "</td>"
                    + "<td>"
                    + "<div class=\"form-group\">"
                    + "<div class=\"field\" style=\"width:100%;\">"
                    + "<input style=\"width:60%;margin-left:20%;\" type=\"text\" class=\"input w50\" placeholder=\"请输入权限描述(例: 用户管理)\""
                    + "onclick=\"JavaScript:this.value=''\" value=\"用户管理\" name=\"limitText\" data-validate=\"required:必填\" />"
                    + "</div>"
                    + "</div>"
                    + "</td>"
                    + "</tr>");
                index += 1;
              }
            </script>


            <div class="form-group">
                <div class="field">
                    <div class="tips"></div>
                </div>
            </div>


            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="button" onclick="add()"> 新增栏位</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/limit/findLimitAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
