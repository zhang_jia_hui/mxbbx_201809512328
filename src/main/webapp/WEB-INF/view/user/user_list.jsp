 <%@ page import="com.baomidou.mybatisplus.extension.plugins.pagination.Page" %>
<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,target-densitydpi=low-dpi" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <script src="https://www.layuicdn.com/layui-v2.5.6/css/layui.css"></script>
<%--    <script src="<%=basePath %>/lib/layui/layui.js"></script>--%>
<%--    <script src="<%=basePath %>/js/xadmin.js"></script>--%>
</head>
<script type="text/javascript">
  // 删除
  function tabledelete(id){
    if(confirm("是否删除 userId=" + id + "的人员！")){
      window.location.href ="<%=basePath %>/mxbbx/user/deleteUserById?userId=" + id;
    }
  }
</script>
<body>
<form method="post" action="" id="userForm">
    <div class="panel admin-panel">
        <div class="panel-head">
            <strong class="icon-reorder"> 用户信息列表</strong>
            <a href="" style="float:right; display:none;">添加字段</a>
        </div>
        <div class="padding border-bottom">
            <ul class="search" style="padding-left:10px;">
                <li>搜索：</li>
                <li><input type="text" placeholder="请输入待搜索用户ID" id="keywords" class="input"
                        <c:if test="${userId != null}" >
                            value="${userId}"
                        </c:if>
                           name="keywords" style="width:250px; line-height:17px; display:inline-block" />
                    <a class="button border-main icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query()"> 搜索</a>
                </li>
                <li><a class="button border-main icon-plus-square-o" name="insertUser"
                       href="<%=basePath %>/mxbbx/user/insertUser"> 添加用户</a>
                </li>
                <li>
                    <div class="form-group">
                        <div class="field">
                            <select id="roleId" style="width:200px" name="roleId" class="input w50">
                                <option value="100">请选择身份信息</option>
                                <c:forEach items="${roles}" var="map">
                                    <option value="${map.getRoleId()}>">${map.getRoleName()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </li>
                <li>
                    <a class="button border-gray icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query2()"> 筛查</a>
                </li>
<%--                <li><a class="button border-main icon-plus-square-o" name="addUser" title="添加用户"--%>
<%--                       onclick="xadmin.open('添加用户','<%=basePath %>/mxbbx/user/insertUser',null,null)"--%>
<%--                       href="javascript:;">添加用户</a>--%>
<%--                </li>--%>
<%--                使用layer弹窗，目前有大量bug！！！！！--%>
            </ul>
        </div>
        <table class="table table-hover text-center" >
            <tr>
                <th style="width:25%">账户</th>
<%--                <th style="width:25%">密码</th>--%>
                <th style="width:25%">身份</th>
                <th style="width:25%" class="buttonsTwo">操作</th>
            </tr>
            <tr>
                <c:forEach items="${userList}" var="map">
                    <tr>
                        <td>${map.userId}</td>
<%--                        <td>${map.passWord}</td>--%>
                        <td>
                            <c:forEach items="${roleList}" var="role">
                                <c:if test="${map.roleId eq role.getRoleId()}">
                                    ${role.getRoleName()}
                                </c:if>
                            </c:forEach>
                        </td>
                        <td class="buttonsTwo"><div class="button-group">
                            <a class="button border-main" href="<%=basePath %>/mxbbx/user/updateUser?userId=${map.userId}">
                                <span class="icon-edit"></span> 修改</a>
<%--                            <a class="button border-red" href="<%=basePath %>/mxbbx/user/deleteUserById?userId=${map.userId}">--%>
<%--                                <span class="icon-trash-o"></span> 删除</a>--%>
                            <a class="button border-red tabledelete" onclick="tabledelete(${map.userId})" value="${map.userId}">
                                <span class="icon-trash-o"></span> 删除</a>
                        </div></td>
                    </tr>
                </c:forEach>
            </tr>
            <tr>
                <td colspan="15">
                    <div class="pagelist">
                        <a href="<%=basePath %>/mxbbx/user/findUserAll?pageNumber=1&userId=${userId}">首页</a>
                        <c:choose>
                            <c:when test="${page.current > 1}">
                                <a href="<%=basePath %>/mxbbx/user/findUserAll?pageNumber=${page.current-1}&userId=${userId}">上一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是第一页');">上一页</a>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${page.current > 3}">
                            . . .
                        </c:if>
                        <c:forEach items="${pagesList}" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current == status.index + 1}">
                                    <span class="current">${status.index+1}</span>
                                </c:when>
                                <c:when test="${page.current != status.index + 1}">
                                    <c:if test="${page.current + 2 >= status.index + 1 && page.current - 2 <= status.index + 1}">
                                        <a href="<%=basePath %>/mxbbx/user/findUserAll?pageNumber=${status.index+1}&userId=${userId}">${status.index+1}</a>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${(pages - page.current) > 2}">
                            . . .
                        </c:if>
                        <c:choose>
                            <c:when test="${page.current < pages}">
                                <a href="<%=basePath %>/mxbbx/user/findUserAll?pageNumber=${page.current+1}&userId=${userId}">下一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是最后一页')">下一页</a>
                            </c:otherwise>
                        </c:choose>
                        <a href="<%=basePath %>/mxbbx/user/findUserAll?pageNumber=${pages}&userId=${userId}">尾页</a>
                        <div style="font-size:15px;float:right;margin-right:100px">总页数: <span style="color: red;">${pages}</span> 页</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>
<div style="margin: 10px;font-size:15px;">查询数据条数: <span style="color: red;">${page.total}</span> 条</div>
<script type="text/javascript">
  // 模糊查询
  function query() {
    window.location.href = "<%=basePath %>/mxbbx/user/findUserAll?userId=" + $("#keywords").val();
  }
  // 身份筛查
  function query2() {
    var roleId = parseInt($("#roleId").val());
    window.location.href = "<%=basePath %>/mxbbx/user/findUserAll?roleId=" + roleId;
  }
</script>
</body>
<script type="text/javascript">
  window.onload=function(){
    var win = false;
    if("upMeTrue" == "${result}"){
      win = true;
      alert('√ -个人信息修改成功！');
    }else if("upMeFalse" == "${result}"){
      win = true;
      alert('x -个人信息修改失败！');
    }
    if("addTrue" == "${result1}"){
      win = true;
      alert('√ -添加成功！');
    }else if("addFalse" == "${result1}"){
      win = true;
      alert('x -添加失败,已存在该用户!！');
    }
    if("editTrue" == "${result2}"){
      win = true;
      alert('√ -修改成功！');
    }
    if("deleteTrue" == "${result3}"){
      win = true;
      alert('√ -删除成功！');
    }
    if("jurisdiction" == "${result4}"){
      win = true;
      alert('x -权限不够！');
    }
    if(0 == "${pages}"){
      win = true;
      alert('x -数据数量为零！！');
    }
    if(win == true){
      window.location.href = "<%=basePath %>/mxbbx/user/findUserAll";
    }
  };
</script>
</html>
