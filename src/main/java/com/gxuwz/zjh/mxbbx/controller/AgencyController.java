package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Region;
import com.gxuwz.zjh.mxbbx.entity.User;
import com.gxuwz.zjh.mxbbx.service.IAgencyService;
import com.gxuwz.zjh.mxbbx.service.IRegionService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 农合经办机构信息 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/agency")
public class AgencyController {

    @Autowired
    private IAgencyService iAgencyService;
    @Autowired
    private IRegionService iRegionService;


    /**
     * 根据对应id查询信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/findAgencyAll")
    public ModelAndView findAgencyAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                      @Param("agencyId") String agencyId,
                                      HttpServletRequest request, HttpServletResponse response) {

        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Agency> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("agency_id");
        // 进行模糊查询!!!
        if(agencyId != null && agencyId != ""){
            wrapper.like("agency_id", agencyId);
            request.getSession().setAttribute("agencyId", agencyId);
        }else {
            request.getSession().setAttribute("agencyId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Agency> agencyIPage = iAgencyService.selectAgencyPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)agencyIPage.getPages()];
        for(int i=0; i< (int)agencyIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+agencyIPage.getTotal());
        System.out.println("总页数"+agencyIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)agencyIPage.getPages());
        List<Agency> agencyList = agencyIPage.getRecords();
        System.out.println("agencyList = "+agencyList);
        modelAndView.addObject("agencyList", agencyList);

        // 获取行政区域
        List<Region> regionList = iRegionService.findRegionAll();
        modelAndView.addObject("regionList", regionList);

        modelAndView.setViewName("agency/agency_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertAgency")
    public ModelAndView insertAgency(ModelAndView modelAndView) {
        modelAndView.setViewName("agency/agency_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addAgency")
    public ModelAndView addAgency(ModelAndView modelAndView, Agency agency, HttpServletRequest request) {
        Agency agency1 = iAgencyService.findAgencyById(agency);
        if(agency1 == null){
            iAgencyService.addAgency(agency);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("agency/agency_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateAgency")
    public ModelAndView updateAgency(ModelAndView modelAndView, String agencyId) {
        Agency agency = new Agency();
        agency.setAgencyId(agencyId);
        Agency agency1 = iAgencyService.findAgencyById(agency);
        modelAndView.addObject("agency", agency1);
        modelAndView.setViewName("agency/agency_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editAgency")
    public ModelAndView editAgency(ModelAndView modelAndView, Agency agency, HttpServletRequest request) {
        iAgencyService.updateAgencyById(agency);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("agency/agency_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteAgencyById")
    public ModelAndView deleteAgencyById(ModelAndView modelAndView, HttpServletRequest request, String agencyId) {
        Agency agency = new Agency();
        agency.setAgencyId(agencyId);
        iAgencyService.deleteAgencyById(agency);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("agency/agency_list");
        return modelAndView;
    }

}
