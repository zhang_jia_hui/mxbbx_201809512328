package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Personnel;
import com.gxuwz.zjh.mxbbx.mapper.PersonnelMapper;
import com.gxuwz.zjh.mxbbx.service.IPersonnelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 人员信息表 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@Service
public class PersonnelServiceImpl extends ServiceImpl<PersonnelMapper, Personnel> implements IPersonnelService {

    @Autowired
    private PersonnelMapper personnelMapper;

    @Override
    public Personnel findPersonnelById(Personnel personnel) {
        return personnelMapper.findPersonnelById(personnel);
    }

    @Override
    public List<Personnel> findPersonnelAll() {
        return personnelMapper.findPersonnelAll();
    }

    @Override
    public IPage<Personnel> selectPersonnelPage(Page<Personnel> page, Wrapper<Personnel> wrapper) {
        return personnelMapper.selectPersonnelPage(page, wrapper);
    }

    @Override
    public void addPersonnel(Personnel personnel) {
        personnelMapper.addPersonnel(personnel);
    }

    @Override
    public void updatePersonnelById(Personnel personnel) {
        personnelMapper.updatePersonnelById(personnel);
    }

    @Override
    public void deletePersonnelById(Personnel personnel) {
        personnelMapper.deletePersonnelById(personnel);
    }
}
