package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 家庭档案信息表
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Family") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Family对象", description="家庭档案信息表")
public class Family implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "家庭编号")
    private String familyId;

    @ApiModelProperty(value = "县级编码")
    private String levelId;

    @ApiModelProperty(value = "乡镇编码")
    private String shipId;

    @ApiModelProperty(value = "村编码")
    private String villageId;

    @ApiModelProperty(value = "组编号")
    private String groupId;

    @ApiModelProperty(value = "家庭人口数")
    private Integer personNumber;

    @ApiModelProperty(value = "农业人口数")
    private Integer agrNumber;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "创建档案时间")
    private String establishTime;

    @ApiModelProperty(value = "登记员")
    private String registrant;

    @ApiModelProperty(value = "户主")
    private String holder;

    @ApiModelProperty(value = "户主身份证号")
    private String number;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
