<%@ page import="com.baomidou.mybatisplus.extension.plugins.pagination.Page" %>
<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,target-densitydpi=low-dpi" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<script type="text/javascript">
  // 删除
  function tabledelete(id){
    if(confirm("是否删除 familiesId=" + id + "的信息！")){
      window.location.href ="<%=basePath %>/mxbbx/families/deleteFamiliesById?familiesId=" + id;
    }
  }
</script>
<body>
<form method="post" action="<%=basePath %>mxbbx/families/findFamiliesAll" >
    <div class="panel admin-panel">
        <div class="panel-head">
            <strong class="icon-reorder"> 参合人员档案信息表</strong>
            <a href="" style="float:right; display:none;">添加字段</a>
        </div>
        <div class="padding border-bottom">
            <ul class="search" style="padding-left:10px;">
                <li>搜索：</li>
                <li>
                    <div class="form-group">
                        <div class="field">
                            <select id="nongheId" style="width:250px" name="nongheId" class="input w50">
                                <option value="">请选择农合证号</option>
                                <c:forEach items="${familiesList1}" var="map">
                                    <option <c:if test="${nongheId eq map.nongheId}" >selected</c:if> value="${map.nongheId}">
                                        <c:if test="${fn:substring(map.nongheId, 16, 18) eq '01'}" >${map.nongheId}---${map.name}</c:if>
                                        <c:if test="${fn:substring(map.nongheId, 16, 18) != '01'}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${map.nongheId}---${map.name}</c:if>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </li>
                <li><input type="text" placeholder="请输入姓名" id="name" class="input"
                        <c:if test="${name != null}" >
                            value="${name}"
                        </c:if>
                           name="name" style="width:250px; line-height:17px; display:inline-block" />
                </li>
                <li>
                    <a class="button border-green icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query()"> 搜索</a>
                </li>
                <li><a class="button border-green icon-plus-square-o" name="insertFamilies"
                       href="<%=basePath %>/mxbbx/families/insertFamilies"> 添加信息</a>
                </li>
            </ul>
        </div>
        <table class="table table-hover text-center" >
            <tr>
                <th style="width:17%">家庭成员编号</th>
                <th style="width:12%">家庭编号</th>
                <th style="width:12%">农合证号</th>
                <th style="width:10%">医疗证卡号</th>
                <th style="width:5%">户内编号</th>
                <th style="width:5%">姓名</th>
                <th style="width:8%">与户主关系</th>
                <th style="width:12%">身份证号</th>
                <th style="width:19%" class="buttonsTwo">操作</th>
            </tr>
            <tr>
                <c:forEach items="${familiesList}" var="map">
                    <tr>
                        <td>${map.familiesId}</td>
                        <td>${map.familyId}</td>
                        <td>${map.nongheId}</td>
                        <td>${map.medicalId}</td>
                        <td>${map.indoorId}</td>
                        <td>${map.name}</td>
                        <td>${map.holderRela}</td>
                        <td>${map.number}</td>
                        <td class="buttonsTwo"><div class="button-group">
                            <a class="button border-yellow" href="<%=basePath %>/mxbbx/card/insertCard?familiesId=${map.familiesId}">
                                <span class="icon-money"></span> 慢病证</a>
                            <a class="button border-main" href="<%=basePath %>/mxbbx/families/updateFamilies?familiesId=${map.familiesId}">
                                <span class="icon-edit"></span> 修改</a>
<%--                            <a class="button border-red" href="<%=basePath %>/mxbbx/agency/deleteAgencyById?agencyId=${map.agencyId}">--%>
<%--                                <span class="icon-trash-o"></span> 删除</a>--%>
                            <a class="button border-red tabledelete" onclick="tabledelete(${map.familiesId})" value="${map.familiesId}">
                                <span class="icon-trash-o"></span> 删除</a>
                        </div></td>
                    </tr>
                </c:forEach>
            </tr>
            <tr>
                <td colspan="15">
                    <div class="pagelist">
                        <a href="<%=basePath %>/mxbbx/families/findFamiliesAll?pageNumber=1&familiesId=${familiesId}">首页</a>
                        <c:choose>
                            <c:when test="${page.current > 1}">
                                <a href="<%=basePath %>/mxbbx/families/findFamiliesAll?pageNumber=${page.current-1}&familiesId=${familiesId}">上一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是第一页');">上一页</a>
                            </c:otherwise>
                        </c:choose>

                        <c:if test="${page.current > 3}">
                            . . .
                        </c:if>
                        <c:forEach items="${pagesList}" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current == status.index + 1}">
                                    <span class="current">${status.index+1}</span>
                                </c:when>
                                <c:when test="${page.current != status.index + 1}">
                                    <c:if test="${page.current + 2 >= status.index + 1 && page.current - 2 <= status.index + 1}">
                                        <a href="<%=basePath %>/mxbbx/families/findFamiliesAll?pageNumber=${status.index+1}&familiesId=${familiesId}">${status.index+1}</a>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${(pages - page.current) > 2}">
                            . . .
                        </c:if>

                        <c:choose>
                            <c:when test="${page.current < pages}">
                                <a href="<%=basePath %>/mxbbx/families/findFamiliesAll?pageNumber=${page.current+1}&familiesId=${familiesId}">下一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是最后一页')">下一页</a>
                            </c:otherwise>
                        </c:choose>
                        <a href="<%=basePath %>/mxbbx/families/findFamiliesAll?pageNumber=${pages}&familiesId=${familiesId}">尾页</a>
                        <div style="font-size:15px;float:right;margin-right:100px">总页数: <span style="color: red;">${pages}</span> 页</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>
<div style="margin: 10px;font-size:15px;">查询数据条数: <span style="color: red;">${page.total}</span> 条</div>
<script type="text/javascript">
  // 模糊查询
  function query() {
    var name = $("#name").val();
    var nongheId = $("#nongheId option:selected").val();
    window.location.href = "<%=basePath %>/mxbbx/families/findFamiliesAll?name=" + name + "&nongheId=" + nongheId;
  }
</script>
</body>
<script type="text/javascript">
  window.onload=function(){
    var win = false;
    if("addTrue" == "${result1}"){
      win = true;
      alert('√ -添加成功！');
    }else if("addFalse" == "${result1}"){
      win = true;
      alert('x -添加失败,已存在该编号信息!！');
    }
    if("editTrue" == "${result2}"){
      win = true;
      alert('√ -修改成功！');
    }
    if("deleteTrue" == "${result3}"){
      win = true;
      alert('√ -删除成功！');
    }
    if("jurisdiction" == "${result4}"){
      win = true;
      alert('x -权限不够！');
    }
    if(0 == "${pages}"){
      win = true;
      alert('x -数据数量为零！！');
    }
    if("none" == "${new_card}"){
      alert('x -未找到慢性病证信息，以为您初始化病证，请查看！！');
      window.location.href = "<%=basePath %>/mxbbx/card/findCardAll";
    }else if ("no" == "${new_card}"){
      win = true;
      alert('x -未找到该人员信息，请补充详细信息！！');
    }else if ("have" == "${new_card}"){
      window.location.href = "<%=basePath %>/mxbbx/card/findCardAll";
    }
    if(win == true){
      window.location.href = "<%=basePath %>/mxbbx/families/findFamiliesAll";
    }
  };
</script>
</html>
