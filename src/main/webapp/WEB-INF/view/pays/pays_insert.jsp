<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/pays/addPays">
            <div class="form-group">
                <div class="label">
                    <label>缴费记录编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入缴费标准编号(例: 4504210101012066)"
                           value="${pays.paysId}" name="PaysId" data-validate="required:必填" \
                           <c:if test="${!(pays.paysId eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭编号(例: 4504210101010048)"
                           value="${pays.familyId}" name="familyId" data-validate="required:必填"
                           <c:if test="${!(pays.familyId eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>当次缴费人：</label>
                </div>
                <div class="label">
                    <c:forEach items="${names1}" var="map" >
                        <label><input type="checkbox" class="checkbox" name="checkNames" onclick="checkBox()"
                                      value="${map}" data-validate="required:必选">${map}</label>
                    </c:forEach>
                    <div class="tips"></div>
                </div>
            </div>
            <script type="text/javascript">
              function checkBox() {
                var checkboxs = document.getElementsByName("checkNames");
                var num = 0;
                for(var i = 0;i < checkboxs.length;i++){
                  if(checkboxs[i].checked==true){
                    num++ ;
                  }
                }
                var money = num * ${pay.money};
                document.getElementById("checkMoney").value = money;
              }
            </script>

            <div class="form-group">
                <div class="label">
                    <label>当前需要缴费金额：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" id="checkMoney" name = "checkMoney" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭已缴费人员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭已缴费人员(例: 钟海清,蔡云妮,钟艳婷,钟艳玲)"
                           value="${pays.familyName}" name="familyName" readonly="readonly"
                           <c:if test="${!(pays.familyName eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭未缴费人员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭未缴费人员(例: 钟子峰)"
                           value="${pays.familyNoName}" name="familyNoName"
                           <c:if test="${!(pays.familyNoName eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>未缴费人数：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入缴费金额(例: 4)"
                           value="${pays.familyNum}" name="familyNum" data-validate="required:必填"
                           <c:if test="${!(pays.familyNum eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>当次缴费(元)：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入当次缴费(例: 4000)"
                           value="${pays.money}" name="money" data-validate="required:必填"
                           <c:if test="${!(pays.money eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费总金额(元)：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入缴费总金额(例: 4000)"
                           value="${pays.moneys}" name="moneys" data-validate="required:必填"
                           <c:if test="${!(pays.moneys eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费年份：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入缴费金额(例: 1000)"
                           value="${pays.year}" name="year" data-validate="required:必填"
                           <c:if test="${!(pays.year eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入缴费时间" id="time"
                           name="time" data-validate="required:必填" value="${pays.time}"
                            <c:if test="${!(pays.time eq null)}">readonly</c:if> />
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                laydate.render({
                  elem: '#time', //指定元素
                  type: 'datetime'
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="Paysmit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/pays/findPaysAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
<script type="text/javascript">
  window.onload=function(){
    var date = new Date()
    var second = date.getSeconds()
    var minute = date.getMinutes()
    var hour = date.getHours()
    var day = date.getDate()
    var month = date.getMonth() + 1
    var year = date.getFullYear()
    var time = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    document.getElementById("time").value = time;
  }
</script>
</html>
