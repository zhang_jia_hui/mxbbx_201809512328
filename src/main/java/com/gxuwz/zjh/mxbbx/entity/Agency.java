package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 经办机构信息
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Agency") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Agency对象", description="经办机构信息")
public class Agency implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "农合机构编码")
    private String agencyId;

    @ApiModelProperty(value = "所属行政区域编码")
    private String regionId;

    @ApiModelProperty(value = "所属行政编码")
    private String admId;

    @ApiModelProperty(value = "机构名称")
    private String mechanismName;

    @ApiModelProperty(value = "级别")
    private Integer levels;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
