package com.gxuwz.zjh.mxbbx.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 参合缴费登记记录
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Data
@Alias("Pays") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Pays对象", description="")
public class Pays implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "参合缴费记录编号")
    private String paysId;

    @ApiModelProperty(value = "家庭编号")
    private String familyId;

    @ApiModelProperty(value = "当次缴费人姓名")
    private String nowName;

    @ApiModelProperty(value = "家庭已缴费人员姓名")
    private String familyName;

    @ApiModelProperty(value = "家庭未缴费人员姓名")
    private String familyNoName;

    @ApiModelProperty(value = "家庭未缴费人数")
    private int familyNum;

    @ApiModelProperty(value = "当次缴费金额")
    private double money;

    @ApiModelProperty(value = "家庭缴费总金额")
    private double moneys;

    @ApiModelProperty(value = "家庭缴费年份")
    private String year;

    @ApiModelProperty(value = "缴费时间")
    private String time;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}