package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.entity.ChronicCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 慢性病证 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@Mapper
@Repository
public interface ChronicCardMapper extends BaseMapper<ChronicCard> {

    // 根据 id 查询对应用户信息
    ChronicCard findChronicCardById(ChronicCard chronicCard);

    // 查询全部用户信息
    List<ChronicCard> findChronicCardAll();

    // 根据 address 查询对应用户信息
    List<ChronicCard> findChronicCard(ChronicCard chronicCard);

    // 判断 times 是否在慢性病证时间内
    List<ChronicCard> nowTimesByCard(Date times);

    //自定义sql 分页
    IPage<ChronicCard> selectChronicCardPage(Page<ChronicCard> page, @Param(Constants.WRAPPER) Wrapper<ChronicCard> wrapper);

    // 添加用户信息
    void addChronicCard(ChronicCard chronicCard);

    // 修改用户信息
    void updateChronicCardById(ChronicCard chronicCard);

    // 删除用户信息
    void deleteChronicCardById(ChronicCard chronicCard);

}
