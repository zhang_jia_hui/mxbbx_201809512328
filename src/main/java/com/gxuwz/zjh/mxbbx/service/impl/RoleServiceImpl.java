package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Role;
import com.gxuwz.zjh.mxbbx.mapper.RoleMapper;
import com.gxuwz.zjh.mxbbx.mapper.UserMapper;
import com.gxuwz.zjh.mxbbx.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Role findRoleById(Role role) {
        return roleMapper.findRoleById(role);
    }

    @Override
    public List<Role> findRoleAll() {
        return roleMapper.findRoleAll();
    }

    @Override
    public IPage<Role> selectRolePage(Page<Role> page, Wrapper<Role> wrapper) {
        return roleMapper.selectRolePage(page, wrapper);
    }

    @Override
    public void addRole(Role role) {
        roleMapper.addRole(role);
    }

    @Override
    public void updateRoleById(Role role) {
        roleMapper.updateRoleById(role);
    }

    @Override
    public void deleteRoleById(Role role) {
        roleMapper.deleteRoleById(role);
    }
}
