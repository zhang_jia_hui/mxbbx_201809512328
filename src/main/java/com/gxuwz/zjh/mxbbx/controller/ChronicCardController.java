package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.*;
import com.gxuwz.zjh.mxbbx.service.*;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 慢性病证 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@RestController
@RequestMapping("/mxbbx/card")
public class ChronicCardController {


    @Autowired
    private IChronicCardService iChronicCardService;
    @Autowired
    private IChronicService iChronicService;
    @Autowired
    private IFamiliesService iFamiliesService;
    @Autowired
    private IPersonnelService iPersonnelService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findCardAll")
    public ModelAndView findCardAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                    HttpServletRequest request, HttpServletResponse response,
                                    @Param("cardId") String cardId,
                                    @Param("number") String number) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<ChronicCard> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("card_id");
        // 进行模糊查询!!!
        if(cardId != null && cardId != ""){
            wrapper.like("card_id", cardId);
            request.getSession().setAttribute("cardId", cardId);
        }else {
            request.getSession().setAttribute("cardId", "");
        }
        if(number != null && number != ""){
            System.out.println(" number number = " + number);
            // 根据身份证号码，查询对应的人员信息
            Families families = new Families();
            families.setNumber(number);
            families = iFamiliesService.findFamiliesByNumber(families);
            // 通过农合证号进行搜索
            if(families != null){
                wrapper.like("card_id", families.getNongheId());
                request.getSession().setAttribute("number", number);
            }else {
                request.setAttribute("card_pay", "none");
            }
        }else {
            request.getSession().setAttribute("number", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<ChronicCard> chronicCardIPage = iChronicCardService.selectChronicCardPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)chronicCardIPage.getPages()];
        for(int i=0; i< (int)chronicCardIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+chronicCardIPage.getTotal());
        System.out.println("总页数"+chronicCardIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)chronicCardIPage.getPages());
        List<ChronicCard> chronicCardList = chronicCardIPage.getRecords();
        System.out.println("chronicCardList = "+chronicCardList);
        modelAndView.addObject("chronicCardList", chronicCardList);

        // 慢性病查询
        List<Chronic> chronicList = iChronicService.findChronicAll();
        modelAndView.addObject("chronicList", chronicList);

        // 身份证号码查询
        List<Families> familiesList = iFamiliesService.findFamiliesAll();
        modelAndView.addObject("familiesList", familiesList);

        modelAndView.setViewName("card/card_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertCard")
    public ModelAndView insertCard(ModelAndView modelAndView, HttpServletRequest request,
                                   @Param("familiesId") String familiesId) {
        Families families = new Families();
        families.setFamiliesId(familiesId);
        families = iFamiliesService.findFamiliesById(families);
        if(families != null){
            // 根据姓名，查询该人员的信息
            Personnel personnel = new Personnel();
            personnel.setNumber(families.getNumber());
            personnel = iPersonnelService.findPersonnelById(personnel);
            System.out.println("personnel = " + personnel);
            if(personnel != null){
                // 查看是否存在对应的慢性病证
                ChronicCard chronicCard = new ChronicCard();
                chronicCard.setCardId(families.getNongheId());
                if(iChronicCardService.findChronicCardById(chronicCard) != null){
                    request.setAttribute("new_card", "have");
                    modelAndView.setViewName("families/families_list");
                    return modelAndView;
                }else {
                    chronicCard.setName(families.getName());
                    chronicCard.setAddress(personnel.getHomeAddress());
                    chronicCard.setChronicId(null);
                    chronicCard.setChronicStartTime(null);
                    chronicCard.setChronicEndTime(null);
                    chronicCard.setProve(null);
                    iChronicCardService.addChronicCard(chronicCard);
                    request.setAttribute("new_card", "none");
                    modelAndView.setViewName("families/families_list");
                    return modelAndView;
                }
            }else {
                request.setAttribute("new_card", "no");
                modelAndView.setViewName("families/families_list");
                return modelAndView;
            }

        }
        // 慢性病查询
        List<Chronic> chronicList = iChronicService.findChronicAll();
        modelAndView.addObject("chronicList", chronicList);

        modelAndView.setViewName("card/card_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addCard")
    public ModelAndView addCard(ModelAndView modelAndView, ChronicCard chronicCard, HttpServletRequest request,
                                @Param("chronicStartTime") String chronicStartTime,
                                @Param("chronicEndTime") String chronicEndTime) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String chronicStartTime1 = null;
        String chronicEndTime1 = null;
        if(chronicStartTime != null && chronicStartTime != ""){
            Date time1 = formatter.parse(chronicStartTime);
            chronicStartTime1 =formatter.format(time1);
        }
        if(chronicEndTime != null && chronicEndTime != ""){
            Date time1 = formatter.parse(chronicEndTime);
            chronicEndTime1 =formatter.format(time1);
        }
        System.out.println("chronicStartTime1 = " + chronicStartTime1);
        System.out.println("chronicEndTime1 = " + chronicEndTime1);

        chronicCard.setChronicStartTime(chronicStartTime1);
        chronicCard.setChronicEndTime(chronicEndTime1);

        ChronicCard chronicCard1 = iChronicCardService.findChronicCardById(chronicCard);
        if(chronicCard1 == null){
            iChronicCardService.addChronicCard(chronicCard);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("card/card_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateCard")
    public ModelAndView updateCard(ModelAndView modelAndView, String cardId) {
        // 慢性病查询
        List<Chronic> chronicList = iChronicService.findChronicAll();
        modelAndView.addObject("chronicList", chronicList);

        ChronicCard chronicCard = new ChronicCard();
        chronicCard.setCardId(cardId);
        ChronicCard chronicCard1 = iChronicCardService.findChronicCardById(chronicCard);
        modelAndView.addObject("chronicCard", chronicCard1);
        modelAndView.setViewName("card/card_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editCard")
    public ModelAndView editCard(ModelAndView modelAndView, ChronicCard chronicCard, HttpServletRequest request,
                                 @Param("chronicStartTime") String chronicStartTime,
                                 @Param("chronicEndTime") String chronicEndTime) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String chronicStartTime1 = null;
        String chronicEndTime1 = null;
        if(chronicStartTime != null && chronicStartTime != ""){
            Date time1 = formatter.parse(chronicStartTime);
            chronicStartTime1 =formatter.format(time1);
        }
        if(chronicEndTime != null && chronicEndTime != ""){
            Date time1 = formatter.parse(chronicEndTime);
            chronicEndTime1 =formatter.format(time1);
        }
        System.out.println("chronicStartTime1 = " + chronicStartTime1);
        System.out.println("chronicEndTime1 = " + chronicEndTime1);

        chronicCard.setChronicStartTime(chronicStartTime1);
        chronicCard.setChronicEndTime(chronicEndTime1);

        iChronicCardService.updateChronicCardById(chronicCard);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("card/card_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteCardById")
    public ModelAndView deleteCardById(ModelAndView modelAndView, HttpServletRequest request, String cardId) {
        ChronicCard chronicCard = new ChronicCard();
        chronicCard.setCardId(cardId);
        iChronicCardService.deleteChronicCardById(chronicCard);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("card/card_list");
        return modelAndView;
    }

}
