package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Health;
import com.gxuwz.zjh.mxbbx.entity.Sub;
import com.gxuwz.zjh.mxbbx.service.IHealthService;
import com.gxuwz.zjh.mxbbx.service.ISubService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * (S201_03)  卫生机构（组织）类别 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/health")
public class HoHealthController {

    @Autowired
    private IHealthService iHealthService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findHealthAll")
    public ModelAndView findHealthAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                      @Param("healthId") String healthId,
                                   HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Health> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("health_id");
        // 进行模糊查询!!!
        if(healthId != null && healthId != ""){
            wrapper.like("health_id", healthId);
            request.getSession().setAttribute("healthId", healthId);
        }else {
            request.getSession().setAttribute("healthId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Health> healthIPage = iHealthService.selectHealthPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)healthIPage.getPages()];
        for(int i=0; i< (int)healthIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+healthIPage.getTotal());
        System.out.println("总页数"+healthIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)healthIPage.getPages());
        List<Health> healthList = healthIPage.getRecords();
        System.out.println("healthList = "+healthList);
        modelAndView.addObject("healthList", healthList);
        modelAndView.setViewName("health/health_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertHealth")
    public ModelAndView insertHealth(ModelAndView modelAndView) {
        modelAndView.setViewName("health/health_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addHealth")
    public ModelAndView addHealth(ModelAndView modelAndView, Health health, HttpServletRequest request) {
        Health health1 = iHealthService.findHealthById(health);
        if(health1 == null){
            iHealthService.addHealth(health);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("health/health_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateHealth")
    public ModelAndView updateHealth(ModelAndView modelAndView, String healthId) {
        Health health = new Health();
        health.setHealthId(healthId);
        Health health1 = iHealthService.findHealthById(health);
        modelAndView.addObject("health", health1);
        modelAndView.setViewName("health/health_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editHealth")
    public ModelAndView editHealth(ModelAndView modelAndView, Health health, HttpServletRequest request) {
        iHealthService.updateHealthById(health);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("health/health_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteHealthById")
    public ModelAndView deleteHealthById(ModelAndView modelAndView, HttpServletRequest request, String healthId) {
        Health health = new Health();
        health.setHealthId(healthId);
        iHealthService.deleteHealthById(health);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("health/health_list");
        return modelAndView;
    }

}
