package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.mapper.AgencyMapper;
import com.gxuwz.zjh.mxbbx.service.IAgencyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 农合经办机构信息 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class AgencyServiceImpl extends ServiceImpl<AgencyMapper, Agency> implements IAgencyService {

    @Autowired
    private AgencyMapper agencyMapper;

    @Override
    public Agency findAgencyById(Agency agency) {
        return agencyMapper.findAgencyById(agency);
    }

    @Override
    public List<Agency> findAgencyAll() {
        return agencyMapper.findAgencyAll();
    }

    @Override
    public IPage<Agency> selectAgencyPage(Page<Agency> page, Wrapper<Agency> wrapper) {
        return agencyMapper.selectAgencyPage(page, wrapper);
    }

    @Override
    public void addAgency(Agency agency) {
        agencyMapper.addAgency(agency);
    }

    @Override
    public void updateAgencyById(Agency agency) {
        agencyMapper.updateAgencyById(agency);
    }

    @Override
    public void deleteAgencyById(Agency agency) {
        agencyMapper.deleteAgencyById(agency);
    }
}
