package com.gxuwz.zjh.mxbbx.config;

import com.gxuwz.zjh.mxbbx.interceptor.AdminInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class LoginConfig implements WebMvcConfigurer {

    // 这个方法用来注册拦截器，我们自己写好的拦截器需要通过这里添加注册才能生效
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(new AdminInterceptor());
        registration.addPathPatterns("/**");//所有路径都被拦截
        registration.excludePathPatterns("/"); // 登录请求不拦截
        registration.excludePathPatterns("/login"); // 登录页面不拦截
        registration.excludePathPatterns("" +
                "/images/**",           // images文件夹里文件不拦截
                "/js/**",              // js静态资源不拦截
                "/css/**"             // css静态资源不拦截
        );
    }

}