package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Hospital;
import com.gxuwz.zjh.mxbbx.mapper.HospitalMapper;
import com.gxuwz.zjh.mxbbx.service.IHospitalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 医院表 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class HospitalServiceImpl extends ServiceImpl<HospitalMapper, Hospital> implements IHospitalService {

    @Autowired
    private HospitalMapper hospitalMapper;

    @Override
    public Hospital findHospitalById(Hospital hospital) {
        return hospitalMapper.findHospitalById(hospital);
    }

    @Override
    public List<Hospital> findHospitalAll() {
        return hospitalMapper.findHospitalAll();
    }

    @Override
    public IPage<Hospital> selectHospitalPage(Page<Hospital> page, Wrapper<Hospital> wrapper) {
        return hospitalMapper.selectHospitalPage(page, wrapper);
    }

    @Override
    public void addHospital(Hospital hospital) {
        hospitalMapper.addHospital(hospital);
    }

    @Override
    public void updateHospitalById(Hospital hospital) {
        hospitalMapper.updateHospitalById(hospital);
    }

    @Override
    public void deleteHospitalById(Hospital hospital) {
        hospitalMapper.deleteHospitalById(hospital);
    }
}
