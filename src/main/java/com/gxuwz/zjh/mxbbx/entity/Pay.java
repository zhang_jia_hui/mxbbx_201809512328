package com.gxuwz.zjh.mxbbx.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 参合缴费标准
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Data
@Alias("Pay") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Pay对象", description="")
public class Pay implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "缴费标准编号")
    private String payId;

    @ApiModelProperty(value = "参合缴费标准")
    private String payText;

    @ApiModelProperty(value = "参合缴费年份")
    private String year;

    @ApiModelProperty(value = "缴费金额")
    private double money;

    @ApiModelProperty(value = "起始时间")
    private String startTime;

    @ApiModelProperty(value = "终止时间")
    private String endTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}