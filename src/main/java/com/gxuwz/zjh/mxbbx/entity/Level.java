package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * (S201_06)  机构级别
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Level") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Level对象", description="(S201_06)  机构级别")
public class Level implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "机构级别编号")
    private Integer levelId;

    @ApiModelProperty(value = "机构级别名称")
    private String levelName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
