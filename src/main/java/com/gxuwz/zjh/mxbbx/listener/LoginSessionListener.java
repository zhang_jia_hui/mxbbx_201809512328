package com.gxuwz.zjh.mxbbx.listener;

import com.gxuwz.zjh.mxbbx.entity.User;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

// 监听器
@WebListener
public class LoginSessionListener implements HttpSessionAttributeListener {

    private static final String LOGIN_USER = "user";

    @Override
    public void attributeAdded(HttpSessionBindingEvent hsbe) {

        System.out.println("===============监听器-session新增开始==============");

        // 监听到session属性值发生添加操作,获取对应操作的属性名
        String attname = hsbe.getName();
        System.out.println("session属性名 : " + attname);

        if (LOGIN_USER.equals(attname)) {
            // 获取添加的属性值，即用户登录名
            User attrVal = (User) hsbe.getValue();
            System.out.println("用户登录名 : " + attrVal);
            // 获取该次操作的session对象
            HttpSession session = hsbe.getSession();
            // 该次操作的session对象ID
            String sessionId = session.getId();
            System.out.println("该次操作的session对象ID : " + sessionId);
            // 从缓存对象里面，获得该用户登录名对应的sessionID值
            String sessionId2 = LoginCache.getInstance().getSessionIdByUsername(attrVal);
            System.out.println("缓存对象的sessionID : " + sessionId2);

            if (null == sessionId2) {// 未获得结果，不需要清理前次登录用户会话信息

            } else {
                HttpSession session2 = LoginCache.getInstance().getSessionBySessionId(sessionId2);// 获取前次该用户登录对应的session对象
                session2.invalidate();// 清理前次登录用户会话存储信息，使得前次登录失效
            }

            // 完成该次登录用户登录名、sessionID，session对象的缓存对象存储
            LoginCache.getInstance().setSessionIdByUsername(attrVal, sessionId);
            LoginCache.getInstance().setSessionBySessionId(sessionId, session);
        }

        System.out.println("===============监听器-session新增开始==============");

    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent hsbe) {
        // TODO Auto-generated method stub
        System.out.println("===============监听器-session删除开始==============");

        System.out.println("===============监听器-session删除开始==============");
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent hsbe) {
        // TODO Auto-generated method stub
        System.out.println("===============监听器-session修改开始==============");

//        // 监听到session属性值发生添加操作,获取对应操作的属性名
//        String attname = hsbe.getName();
//        System.out.println("session属性名 : " + attname);
//
//        if (LOGIN_USER.equals(attname)) {
//            // 获取添加的属性值，即用户登录名
//            User attrVal = (User) hsbe.getValue();
//            System.out.println("用户登录名 : " + attrVal);
//            // 获取该次操作的session对象
//            HttpSession session = hsbe.getSession();
//            // 该次操作的session对象ID
//            String sessionId = session.getId();
//            System.out.println("该次操作的session对象ID : " + sessionId);
//            // 从缓存对象里面，获得该用户登录名对应的sessionID值
//            String sessionId2 = LoginCache.getInstance().getSessionIdByUsername(attrVal);
//            System.out.println("缓存对象的sessionID : " + sessionId2);
//
//            if (null == sessionId2) {// 未获得结果，不需要清理前次登录用户会话信息
//
//            } else {
//                HttpSession session2 = LoginCache.getInstance().getSessionBySessionId(sessionId2);// 获取前次该用户登录对应的session对象
//                session2.invalidate();// 清理前次登录用户会话存储信息，使得前次登录失效
//            }
//
//            // 完成该次登录用户登录名、sessionID，session对象的缓存对象存储
//            LoginCache.getInstance().setSessionIdByUsername(attrVal, sessionId);
//            LoginCache.getInstance().setSessionBySessionId(sessionId, session);
//        }

        System.out.println("===============监听器-session修改结束==============");
    }

}


