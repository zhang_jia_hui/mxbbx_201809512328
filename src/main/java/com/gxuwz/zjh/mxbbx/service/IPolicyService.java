package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.entity.Policy;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 慢病政策数据模型 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IPolicyService extends IService<Policy> {

    // 根据id查询信息
    Policy findPolicyById(Policy policy);

    // 查询全部信息
    List<Policy> findPolicyAll();

    // 根据 year 查询是否存在当年的信息
    Policy findPolicyByYear(Policy policy);

    //自定义sql 分页
    IPage<Policy> selectPolicyPage(Page<Policy> page, @Param(Constants.WRAPPER) Wrapper<Policy> wrapper);

    // 添加信息
    void addPolicy(Policy policy);

    // 修改信息
    void updatePolicyById(Policy policy);

    // 删除信息
    void deletePolicyById(Policy policy);

}
