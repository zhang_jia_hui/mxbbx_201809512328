package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Sub;
import com.gxuwz.zjh.mxbbx.mapper.SubMapper;
import com.gxuwz.zjh.mxbbx.service.ISubService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * (S201_02)  隶属关系 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class SubServiceImpl extends ServiceImpl<SubMapper, Sub> implements ISubService {

    @Autowired
    private SubMapper subMapper;

    @Override
    public Sub findSubById(Sub sub) {
        return subMapper.findSubById(sub);
    }

    @Override
    public List<Sub> findSubAll() {
        return subMapper.findSubAll();
    }

    @Override
    public IPage<Sub> selectSubPage(Page<Sub> page, Wrapper<Sub> wrapper) {
        return subMapper.selectSubPage(page, wrapper);
    }

    @Override
    public void addSub(Sub sub) {
        subMapper.addSub(sub);
    }

    @Override
    public void updateSubById(Sub sub) {
        subMapper.updateSubById(sub);
    }

    @Override
    public void deleteSubById(Sub sub) {
        subMapper.deleteSubById(sub);
    }
}
