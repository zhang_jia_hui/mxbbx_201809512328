package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * (S201_01)  机构所属经济类型
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Economic") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Economic对象", description="(S201_01)  机构所属经济类型")
public class Economic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "机构所属经济类型编号")
    private Integer economicId;

    @ApiModelProperty(value = "机构所属经济类型名称")
    private String economicName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
