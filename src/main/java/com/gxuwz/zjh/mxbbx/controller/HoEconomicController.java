package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.gxuwz.zjh.mxbbx.service.IChronicService;
import com.gxuwz.zjh.mxbbx.service.IEconomicService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * (S201_01)  机构所属经济类型 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/economic")
public class HoEconomicController {

    @Autowired
    private IEconomicService iEconomicService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Pointcut
    @RequestMapping(value = "/findEconomicAll")
    public ModelAndView findEconomicAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                        @Param("economicId") String economicId,
                                       HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Economic> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("economic_id");
        // 进行模糊查询!!!
        if(economicId != null && economicId != ""){
            wrapper.like("economic_id", economicId);
            request.getSession().setAttribute("economicId", economicId);
        }else {
            request.getSession().setAttribute("economicId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Economic> economicIPage = iEconomicService.selectEconomicPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)economicIPage.getPages()];
        for(int i=0; i< (int)economicIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+economicIPage.getTotal());
        System.out.println("总页数"+economicIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)economicIPage.getPages());
        List<Economic> economicList = economicIPage.getRecords();
        System.out.println("economicList = "+economicList);
        modelAndView.addObject("economicList", economicList);
        modelAndView.setViewName("economic/economic_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertEconomic")
    public ModelAndView insertEconomic(ModelAndView modelAndView) {
        modelAndView.setViewName("economic/economic_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addEconomic")
    public ModelAndView addEconomic(ModelAndView modelAndView, Economic economic, HttpServletRequest request) {
        Economic economic1 = iEconomicService.findEconomicById(economic);
        if(economic1 == null){
            iEconomicService.addEconomic(economic);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("economic/economic_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateEconomic")
    public ModelAndView updateEconomic(ModelAndView modelAndView, int economicId) {
        Economic economic = new Economic();
        economic.setEconomicId(economicId);
        Economic economic1 = iEconomicService.findEconomicById(economic);
        modelAndView.addObject("economic", economic1);
        modelAndView.setViewName("economic/economic_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editEconomic")
    public ModelAndView editEconomic(ModelAndView modelAndView, Economic economic, HttpServletRequest request) {
        iEconomicService.updateEconomicById(economic);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("economic/economic_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteEconomicById")
    public ModelAndView deleteEconomicById(ModelAndView modelAndView, HttpServletRequest request, int economicId) {
        Economic economic = new Economic();
        economic.setEconomicId(economicId);
        iEconomicService.deleteEconomicById(economic);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("economic/economic_list");
        return modelAndView;
    }

}
