package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 医院表
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Hospital") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Hospital对象", description="医院表")
public class Hospital implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医院ID")
    private String hospitalId;

    @ApiModelProperty(value = "机构所属经济类型编号")
    private String economicId;

    @ApiModelProperty(value = "隶属关系编码")
    private String subId;

    @ApiModelProperty(value = "卫生机构（组织）类别编码")
    private String healthId;

    @ApiModelProperty(value = "机构级别编号")
    private String mechanismId;

    @ApiModelProperty(value = "机构级别编号")
    private String levelId;

    @ApiModelProperty(value = "名称")
    private String name;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
