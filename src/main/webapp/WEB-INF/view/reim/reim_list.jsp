<%@ page import="com.baomidou.mybatisplus.extension.plugins.pagination.Page" %>
<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,target-densitydpi=low-dpi" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <%--  引入可视化界面  --%>
    <script src="<%=basePath %>/js/echarts.min.js"></script>
</head>
<script type="text/javascript">
  // 删除
  function tabledelete(id){
    if(confirm("是否删除 invoice=" + id + "的信息！")){
      window.location.href ="<%=basePath %>/mxbbx/reim/deleteReimById?invoice=" + id;
    }
  }
</script>
<body>
<form method="post" action="<%=basePath %>mxbbx/reim/findReimAll" >
    <div class="panel admin-panel">
        <div class="panel-head">
            <strong class="icon-reorder"> 慢性病报销信息列表</strong>
            <a href="" style="float:right; display:none;">添加字段</a>
        </div>
        <div class="padding border-bottom">
            <ul class="search" style="padding-left:10px;">
                <li>搜索：</li>
                <li>
                    <div class="form-group">
                        <div class="field">
                            <select id="region" style="width:250px" name="region" class="input w50">
                                <option value="">请选择区域</option>
                                <c:forEach items="${regionList}" var="map">
                                    <option <c:if test="${region eq map.regionId}" >selected</c:if> value="${map.regionId}">
                                        <c:if test="${fn:length(map.regionId) == 6}" >${map.regionName}</c:if>
                                        <c:if test="${fn:length(map.regionId) == 8}" >&nbsp;&nbsp;&nbsp;${map.regionName}</c:if>
                                        <c:if test="${fn:length(map.regionId) == 10}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${map.regionName}</c:if>
                                        <c:if test="${fn:length(map.regionId) == 12}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${map.regionName}</c:if>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="form-group">
                        <div class="field">
                            <select id="chronic" style="width:250px" name="chronic" class="input w50">
                                <option value="">请选择疾病</option>
                                <c:forEach items="${chronicList}" var="map">
                                    <option <c:if test="${chronic eq map.chronicId}" >selected</c:if> value="${map.chronicId}">
                                        ${map.chronicName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </li>
                <li><input type="text" placeholder="请输入待搜慢性病报销ID" id="keywords" class="input"
                        <c:if test="${invoice != null}" >
                            value="${invoice}"
                        </c:if>
                           name="keywords" style="width:250px; line-height:17px; display:inline-block" />
                    <a class="button border-green icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query()"> 搜索</a>
                </li>
<%--                <li><a class="button border-green icon-plus-square-o" name="insertReim"--%>
<%--                       href="<%=basePath %>/mxbbx/reim/insertReim"> 添加信息</a>--%>
<%--                </li>--%>
                <li>
                    <a class="button border-main icon-exchange" name="cardExcelDownloads"
                       href="<%=basePath %>/mxbbx/excel/cardExcelDownloads?excel='reim'"> 导出excel</a>
                </li>
            </ul>
        </div>
        <table class="table table-hover text-center" >
            <tr>
                <th style="width:10%">医院发票号</th>
                <th style="width:10%">医院花费</th>
                <th style="width:7%">实际报销金额</th>
                <th style="width:8%">报销总金额</th>
                <th style="width:15%">参合农民身份证号</th>
                <th style="width:7%">慢性病</th>
                <th style="width:15%">领款时间</th>
                <th style="width:6%">操作员</th>
                <th style="width:7%">状态</th>
                <th style="width:15%" class="buttonsTwo">操作</th>
            </tr>
            <tr>
                <c:forEach items="${chronicReimList}" var="map">
                    <tr>
                        <td>${map.invoice}</td>
                        <td>${map.wantPay}</td>
                        <td>${map.nowPay}</td>
                        <td>${map.payAll}</td>
                        <td>
                            <c:forEach items="${familiesList}" var="families">
                                <c:if test="${map.number eq families.getNumber()}">
                                    ${families.getName()}
                                </c:if>
                            </c:forEach>
                        </td>
                        <td>
                            <c:forEach items="${chronicList}" var="chronic">
                                <c:if test="${map.chronicId eq chronic.getChronicId()}">
                                    ${chronic.getChronicName()}
                                </c:if>
                            </c:forEach>
                        </td>
                        <td>${map.time}</td>
                        <td>${map.person}</td>
                        <c:if test="${map.start eq 0}"><td style="color: #bbbbbb">未批准</td></c:if>
                        <c:if test="${map.start eq 1}"><td style="color: #00aaee">批准</td></c:if>
                        <c:if test="${map.start eq 2}"><td style="color: red">拒批</td></c:if>
                        <td class="buttonsTwo"><div class="button-group">
                            <c:if test="${role.roleId != 104}">
                                <a class="button border-main" href="<%=basePath %>/mxbbx/reim/updateReim?invoice=${map.invoice}">
                                    <span class="icon-edit"></span> 审批</a>
                            </c:if>
                            <c:choose>
                                <c:when test="${role.roleId != 104}">
                                    <a class="button border-red tabledelete" onclick="tabledelete(${map.invoice})" value="${map.invoice}">
                                        <span class="icon-trash-o"></span> 删除</a>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${map.start eq 0}">
                                        <a class="button border-red tabledelete" onclick="tabledelete(${map.invoice})" value="${map.invoice}">
                                            <span class="icon-trash-o"></span> 删除</a>
                                    </c:if>
                                    <c:if test="${map.start == 1}">
                                        <a class="button"><span class="icon-arrow-circle-up"></span> 无效</a>
                                    </c:if>
                                    <c:if test="${map.start == 2}">
                                        <a class="button"><span class="icon-arrow-circle-down"></span> 无效</a>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </div></td>
                    </tr>
                </c:forEach>
            </tr>
            <tr>
                <td colspan="10">
                    <div class="pagelist">
                        <a href="<%=basePath %>/mxbbx/reim/findReimAll?pageNumber=1&invoice=${invoice}">首页</a>
                        <c:choose>
                            <c:when test="${page.current > 1}">
                                <a href="<%=basePath %>/mxbbx/reim/findReimAll?pageNumber=${page.current-1}&invoice=${invoice}">上一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是第一页');">上一页</a>
                            </c:otherwise>
                        </c:choose>

                        <c:if test="${page.current > 3}">
                            . . .
                        </c:if>
                        <c:forEach items="${pagesList}" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current == status.index + 1}">
                                    <span class="current">${status.index+1}</span>
                                </c:when>
                                <c:when test="${page.current != status.index + 1}">
                                    <c:if test="${page.current + 2 >= status.index + 1 && page.current - 2 <= status.index + 1}">
                                        <a href="<%=basePath %>/mxbbx/reim/findReimAll?pageNumber=${status.index+1}&invoice=${invoice}">${status.index+1}</a>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${(pages - page.current) > 2}">
                            . . .
                        </c:if>

                        <c:choose>
                            <c:when test="${page.current < pages}">
                                <a href="<%=basePath %>/mxbbx/reim/findReimAll?pageNumber=${page.current+1}&invoice=${invoice}">下一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是最后一页')">下一页</a>
                            </c:otherwise>
                        </c:choose>
                        <a href="<%=basePath %>/mxbbx/reim/findReimAll?pageNumber=${pages}&invoice=${invoice}">尾页</a>
                        <div style="font-size:15px;float:right;margin-right:100px">总页数: <span style="color: red;">${pages}</span> 页</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>
<div style="margin: 10px;font-size:15px;">查询数据条数: <span style="color: red;">${page.total}</span> 条</div>

<br><br>

<tr>
    <td>
        <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
        <div id="main" style="width:500px;height:450px;float:left"></div>
    </td>
    <td>
        <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
        <div id="main2" style="width:800px;height:450px;float:right;margin-right:100px"></div>
    </td>
</tr>


<script type="text/javascript">
  var myChart = echarts.init(document.getElementById('main'));
  // 显示标题，图例和空的坐标轴
  myChart.setOption({
    title: {
      text: '慢性病报销情况'
    },
    tooltip: {},
    legend: {
      data:['人数']
    },
    xAxis: {
      data: []
    },
    yAxis: {},
    series: [{
      name: '人数',
      type: 'pie',
      radius: '55%',
      data: [],
      itemStyle: {
        normal: {
          shadowBlur: 100,
          shadowColor: 'rgba(0, 0, 0, 0.3)'
        }
      }
    }]
  });

  myChart.showLoading();    //数据加载完之前先显示一段简单的loading动画

  $.ajax({
    type : "post",
    async : true,            //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
    url : "<%=basePath %>/mxbbx/reim/getcountbydisName",    //请求发送到TestServlet处
    data : {},
    dataType : "json",        //返回数据形式为json
    success : function(result) {
      //请求成功时执行该函数内容，result即为服务器返回的json对象
      if (result) {
        myChart.hideLoading();    //隐藏加载动画
        myChart.setOption({        //加载数据图表
          // xAxis: {
          //   data: names
          // },
          series: [{
            // 根据名字对应到相应的系列
            name: '人数',
            data: result
          }]
        });

      }

    },
    error : function(errorMsg) {
      //请求失败时执行该函数
      alert("图表请求数据失败!");
      myChart.hideLoading();
    }
  })
</script>

<script type="text/javascript">
  var myChart2 = echarts.init(document.getElementById('main2'));
  // 显示标题，图例和空的坐标轴
  myChart2.setOption({
    title: {
      text: '慢性病报销情况'
    },
    tooltip: {},
    legend: {
      data:['人数']
    },
    xAxis: {
      data: []
    },
    yAxis: {},
    series: [{
      name: '人数',
      type: 'sunburst',
      radius: '60%',
      data: []
    }]
  });

  myChart2.showLoading();    //数据加载完之前先显示一段简单的loading动画

  var names2=[];    //类别数组（实际用来盛放X轴坐标值）
  var nums2=[];    //销量数组（实际用来盛放Y坐标值）

  $.ajax({
    type : "post",
    async : true,            //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
    url : "<%=basePath %>/mxbbx/reim/getcountbydisName2",    //请求发送到TestServlet处
    data : {},
    dataType : "json",        //返回数据形式为json
    success : function(result) {
      //请求成功时执行该函数内容，result即为服务器返回的json对象
      if (result) {
        myChart2.hideLoading();    //隐藏加载动画
        myChart2.setOption({        //加载数据图表
          xAxis: {
            data: names2
          },
          series: [{
            // 根据名字对应到相应的系列
            name: '人数',
            data: result
          }]
        });

      }

    },
    error : function(errorMsg) {
      //请求失败时执行该函数
      alert("图表请求数据失败!");
      myChart.hideLoading();
    }
  })
</script>



<script type="text/javascript">
  // 模糊查询
  function query() {
    var invoice = $("#keywords").val();
    var region = $("#region option:selected").val();
    var chronic = $("#chronic option:selected").val();
    window.location.href = "<%=basePath %>/mxbbx/reim/findReimAll?invoice=" + invoice + "&region=" + region + "&chronic=" + chronic;
  }
</script>
</body>
<script type="text/javascript">
  window.onload=function(){
    var win = false;
    if("addTrue" == "${result1}"){
      win = true;
      alert('√ -添加成功！');
    }else if("addFalse" == "${result1}"){
      win = true;
      alert('x -添加失败,已存在该编号信息!！');
    }
    if("editTrue" == "${result2}"){
      win = true;
      alert('√ -修改成功！');
    }
    if("deleteTrue" == "${result3}"){
      win = true;
      alert('√ -删除成功！');
    }
    if("jurisdiction" == "${result4}"){
      win = true;
      alert('x -权限不够！');
    }
    if(0 == "${pages}"){
      win = true;
      alert('x -数据数量为零！！');
      win = false;
    }
    if("yes" == "${reim_radio}"){
      win = true;
      alert('√ -报销成功！！');
    }else if ("none" == "${reim_radio}"){
      win = true;
      alert('x -报销金额超出最大金额，以为您报销剩余金额！！');
    }
    if(win == true){
      window.location.href = "<%=basePath %>/mxbbx/reim/findReimAll";
    }
  };
</script>
</html>
