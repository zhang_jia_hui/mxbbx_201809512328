package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.gxuwz.zjh.mxbbx.entity.Sub;
import com.gxuwz.zjh.mxbbx.service.IEconomicService;
import com.gxuwz.zjh.mxbbx.service.ISubService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * (S201_02)  隶属关系 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/sub")
public class HoSubController {

    @Autowired
    private ISubService iSubService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findSubAll")
    public ModelAndView findSubAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                   @Param("subId") String subId,
                                        HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Sub> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("sub_id");
        // 进行模糊查询!!!
        if(subId != null && subId != ""){
            wrapper.like("sub_id", subId);
            request.getSession().setAttribute("subId", subId);
        }else {
            request.getSession().setAttribute("subId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Sub> subIPage = iSubService.selectSubPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)subIPage.getPages()];
        for(int i=0; i< (int)subIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+subIPage.getTotal());
        System.out.println("总页数"+subIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)subIPage.getPages());
        List<Sub> subList = subIPage.getRecords();
        System.out.println("subList = "+subList);
        modelAndView.addObject("subList", subList);
        modelAndView.setViewName("sub/sub_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertSub")
    public ModelAndView insertSub(ModelAndView modelAndView) {
        modelAndView.setViewName("sub/sub_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addSub")
    public ModelAndView addSub(ModelAndView modelAndView, Sub sub, HttpServletRequest request) {
        Sub sub1 = iSubService.findSubById(sub);
        if(sub1 == null){
            iSubService.addSub(sub);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("sub/sub_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateSub")
    public ModelAndView updateSub(ModelAndView modelAndView, String subId) {
        Sub sub = new Sub();
        sub.setSubId(subId);
        Sub sub1 = iSubService.findSubById(sub);
        modelAndView.addObject("sub", sub1);
        modelAndView.setViewName("sub/sub_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editSub")
    public ModelAndView editSub(ModelAndView modelAndView, Sub sub, HttpServletRequest request) {
        iSubService.updateSubById(sub);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("sub/sub_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteSubById")
    public ModelAndView deleteSubById(ModelAndView modelAndView, HttpServletRequest request, String subId) {
        Sub sub = new Sub();
        sub.setSubId(subId);
        iSubService.deleteSubById(sub);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("sub/sub_list");
        return modelAndView;
    }

}
