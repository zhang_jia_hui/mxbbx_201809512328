<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/family/addFamily" name="myForm" >
            <!-- 家庭档案基本信息 -->
            <div class="form-group">
                <div class="field">
                    <div class="tips"></div>
                </div>
                <div class="label">
                    <label style="size:24px;">请输入家庭档案基本信息：</label>
                </div>
                <div class="field">
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭成员编号(例: 4504210101010048)"
                           value="4504210101010048(需唯一,请修改)" name="familyId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>县级编码：</label>
                </div>
                <div class="field">
                    <select id="levelId" style="width:200px" name="levelId" class="input w50">
                        <option value="430201">请选择所属县级编码</option>
                        <c:forEach items="${regionList}" var="map">
                            <c:if test="${fn:length(map.getRegionId()) == 6}" >
                                <option value="${map.getRegionId()}">${map.getRegionName()}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <script type="text/javascript">
              $("#levelId").blur(function(){   // 根据选择的省查询下面的市
                var levelId = $('#levelId option:selected').val();  // 获取选择的值
                // $("#addtest tr:last").after("<div class=\"form-group\">"
                //     + "<div class=\"label\">"
                //     + "<label>乡镇编码：</label>"
                //     + "</div>"
                //     + "<div class=\"field\">"
                //     + "<select id=\"shipId\" style=\"width:200px\" name=\"shipId\" class=\"input w50\">"
                //     + "</select>"
                //     + "<div class=\"tips\"></div> "
                //     + "</div>"
                //     + "</div>");
                // 显示对应id
                document.getElementById("shipIdNone").style.display="inline";
                $("#shipId").empty(); // 清空
                <c:forEach items="${regionList}" var="map" varStatus="item">
                <c:if test="${fn:length(map.getRegionId()) == 8}" >
                if("${map.getRegionId()}".indexOf(levelId) != -1){
                  //先创建好select里面的option元素
                  var option = document.createElement("option");
                  //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                  $(option).val("${map.getRegionId()}");
                  //给option的text赋值,这就是你点开下拉框能够看到的东西
                  $(option).text("${map.getRegionName()}");
                  //获取select 下拉框对象,并将option添加进select
                  $('#shipId').append(option);
                }
                </c:if>
                </c:forEach>
              })
            </script>

            <div id="shipIdNone" style ="display:none">
                <div class="form-group" style="margin-bottom:24px">
                    <div class="label">
                        <label>乡镇编码：</label>
                    </div>
                    <div class="field">
                        <select id="shipId" style="width:200px" name="shipId" class="input w50">
                            <option value="43020101">请选择所属县级编码</option>
                            <c:forEach items="${regionList}" var="map">
                                <c:if test="${fn:length(map.getRegionId()) == 8}" >
                                    <option value="${map.getRegionId()}">${map.getRegionName()}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <div class="tips"></div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
              $("#shipId").blur(function(){   // 根据选择的省查询下面的市
                var shipId = $('#shipId option:selected').val();  // 获取选择的值
                // 显示对应id
                document.getElementById("villageIdNone").style.display="inline";
                $("#villageId").empty(); // 清空
                <c:forEach items="${regionList}" var="map" varStatus="item">
                <c:if test="${fn:length(map.getRegionId()) == 10}" >
                if("${map.getRegionId()}".indexOf(shipId) != -1){
                  //先创建好select里面的option元素
                  var option = document.createElement("option");
                  //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                  $(option).val("${map.getRegionId()}");
                  //给option的text赋值,这就是你点开下拉框能够看到的东西
                  $(option).text("${map.getRegionName()}");
                  //获取select 下拉框对象,并将option添加进select
                  $('#villageId').append(option);
                }
                </c:if>
                </c:forEach>
              })
            </script>

            <div id="villageIdNone" style ="display:none">
                <div class="form-group" style="margin-bottom:24px">
                    <div class="label">
                        <label>村编码：</label>
                    </div>
                    <div class="field">
                        <select id="villageId" style="width:200px" name="villageId" class="input w50">
                            <option value="4302010101">请选择所属县级编码</option>
                            <c:forEach items="${regionList}" var="map">
                                <c:if test="${fn:length(map.getRegionId()) == 10}" >
                                    <option value="${map.getRegionId()}">${map.getRegionName()}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <div class="tips"></div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
              $("#villageId").blur(function(){   // 根据选择的省查询下面的市
                var villageId = $('#villageId option:selected').val();  // 获取选择的值
                // 显示对应id
                document.getElementById("groupIdNone").style.display="inline";
                $("#groupId").empty(); // 清空
                <c:forEach items="${regionList}" var="map" varStatus="item">
                <c:if test="${fn:length(map.getRegionId()) == 12}" >
                if("${map.getRegionId()}".indexOf(villageId) != -1){
                  //先创建好select里面的option元素
                  var option = document.createElement("option");
                  //转换DOM对象为JQ对象,好用JQ里面提供的方法 给option的value赋值
                  $(option).val("${map.getRegionId()}");
                  //给option的text赋值,这就是你点开下拉框能够看到的东西
                  $(option).text("${map.getRegionName()}");
                  //获取select 下拉框对象,并将option添加进select
                  $('#groupId').append(option);
                }
                </c:if>
                </c:forEach>
              })
            </script>

            <div id="groupIdNone" style ="display:none">
                <div class="form-group" style="margin-bottom:24px">
                    <div class="label">
                        <label>组编号：</label>
                    </div>
                    <div class="field">
                        <select id="groupId" style="width:200px" name="groupId" class="input w50">
                            <option value="450421010101">请选择所属县级编码</option>
                            <c:forEach items="${regionList}" var="map">
                                <c:if test="${fn:length(map.getRegionId()) == 12}" >
                                    <option value="${map.getRegionId()}">${map.getRegionName()}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <div class="tips"></div>
                    </div>
                </div>
            </div>


            <div class="form-group" >
                <div class="label">
                    <label>家庭人口数：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入家庭人口数(例: 5)"
                           value="5" name="personNumber" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group" >
                <div class="label">
                    <label>农业人口数：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入农业人口数(例: 3)"
                           value="3" name="agrNumber" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group" >
                <div class="label">
                    <label>家庭住址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭住址(例: 广西省梧州市苍梧县龙圩镇)"
                           value="广西省梧州市苍梧县龙圩镇" name="address" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group" >
                <div class="label">
                    <label>创建档案时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入创建档案时间" value="" id="establishTime" name="establishTime" />
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#establishTime', //指定元素
                  type: 'datetime'
                });
              });
            </script>

            <div class="form-group" >
                <div class="label">
                    <label>登记员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入登记员姓名(例: 张佳辉)"
                           value="张佳辉" name="registrant" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>



            <!-- 户主基本信息 -->
            <div class="form-group">
                <div class="field">
                    <div class="tips"></div>
                </div>
                <div class="label">
                    <label style="size:24px;">请输入户主基本信息：</label>
                </div>
                <div class="field">
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭成员编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭成员编号(例: 450421010101004801070)"
                           value="450421010101004801070(需唯一,请修改)" name="familiesId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>农合证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入农合证号(例: 450421010101004801)"
                           value="450421010101004801" name="nongheId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>医疗证卡号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入医疗证卡号(例: 4504210067270)"
                           value="4504210067270" name="medicalId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>户内编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入户内编号(例: 01)"
                           value="01" name="indoorId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>与户主关系：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入与户主关系(例: 户主)"
                           value="户主" name="holderRela" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <!-- 人员基本信息 -->
            <div class="form-group">
                <div class="label">
                    <label>姓名：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入姓名"
                           value="张三" name="name" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>性别：</label>
                </div>
                <div class="label">
                    <label><input type="radio" name="gender" value="1" checked="checked" data-validate="required:必选">男</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="gender" value="0">女</label>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>年龄：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入年龄"
                           value="20" name="age" data-validate="required:必填" maxlength="3"
                           onkeyup="value=value.replace(/[^0-9]/g,'')" onpaste="value=value.replace(/[^0-9]/g,'')" oncontextmenu = "value=value.replace(/[^0-9]/g,'')"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>身份证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入身份证号" maxlength="18"
                           value="429006200007135719(需唯一,请修改)" name="number" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>出生日期：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入出生日期" id="birthday"
                           name="birthday" data-validate="required:必填" value="2021-05-01"/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#birthday' //指定元素
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label>民族：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入民族"
                           value="汉族" name="nation" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>人员属性：</label>
                </div>
                <div class="field">
                    <select id="personAttributes" style="width:200px" name="personAttributes" class="input w50">
                        <option value="">请选择政治面貌</option>
                        <option value="中共党员">中共党员</option>
                        <option value="中共预备党员">中共预备党员</option>
                        <option value="共青团员">共青团员</option>
                        <option value="民革党员">民革党员</option>
                        <option value="民盟盟员">民盟盟员</option>
                        <option value="民建会员">民建会员</option>
                        <option value="民进会员">民进会员</option>
                        <option value="农工党党员">农工党党员</option>
                        <option value="致公党党员">致公党党员</option>
                        <option value="九三学社社员">九三学社社员</option>
                        <option value="台盟盟员">台盟盟员</option>
                        <option value="无党派人士">无党派人士</option>
                        <option value="群众">群众</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>文化程度：</label>
                </div>
                <div class="field">
                    <select id="degree" style="width:200px" name="degree" class="input w50">
                        <option value="">请选择学历</option>
                        <option value="博士">博士</option>
                        <option value="硕士">硕士</option>
                        <option value="本科">本科</option>
                        <option value="大专">大专</option>
                        <option value="中专和中技">中专和中技</option>
                        <option value="技工学校">技工学校</option>
                        <option value="高中">高中</option>
                        <option value="初中">初中</option>
                        <option value="小学">小学</option>
                        <option value="文盲与半文盲">文盲与半文盲</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>健康状况：</label>
                </div>
                <div class="field">
                    <select id="health" style="width:200px" name="health" class="input w50">
                        <option value="">请选择健康状况</option>
                        <option value="健康">健康</option>
                        <option value="轻微疾病">轻微疾病</option>
                        <option value="重大疾病">重大疾病</option>
                        <option value="遗传疾病">遗传疾病</option>
                        <option value="流行性疾病">流行性疾病</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>是否是农村户口：</label>
                </div>
                <div class="label">
                    <label><input type="radio" name="countryside" value="1" checked="checked" data-validate="required:必选">是</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="countryside" value="0">否</label>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>职业：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入职业"value="工人" name="occupation"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>工作单位：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入工作单位" value="梧州学院" name="workUnit"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>联系方式：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入联系方式" value="17683864164" name="contact" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>联系电话：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入联系电话" value="17683864164" name="telephone" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>常住家庭住址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入常住家庭住址" value="湖北省天门市" name="homeAddress" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>婚姻状况：</label>
                </div>
                <div class="field">
                    <select id="marriage" style="width:200px" name="marriage" class="input w50">
                        <option value=" ">请选择婚姻状况</option>
                        <option value="未婚">未婚</option>
                        <option value="初婚">初婚</option>
                        <option value="再婚">再婚</option>
                        <option value="复婚">复婚</option>
                        <option value="丧偶">丧偶</option>
                        <option value="离婚">离婚</option>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>结婚时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入结婚时间" id="marriageTime"
                           name="marriageTime" value=""/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#marriageTime' //指定元素
                });
              });
            </script>


            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/family/findFamilyAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
