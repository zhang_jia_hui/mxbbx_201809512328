package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * (S201_01)  机构所属经济类型 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IEconomicService extends IService<Economic> {

    // 根据 id 查询对应用户信息
    Economic findEconomicById(Economic economic);

    // 查询全部用户信息
    List<Economic> findEconomicAll();

    //自定义sql 分页
    IPage<Economic> selectEconomicPage(Page<Economic> page, @Param(Constants.WRAPPER) Wrapper<Economic> wrapper);

    // 添加用户信息
    void addEconomic(Economic economic);

    // 修改用户信息
    void updateEconomicById(Economic economic);

    // 删除用户信息
    void deleteEconomicById(Economic economic);

}
