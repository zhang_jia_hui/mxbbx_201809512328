<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <%--  导入dtree  --%>
    <link rel="StyleSheet" href="<%=basePath %>/css/dtree.css" type="text/css" />
    <script type="text/javascript" src="<%=basePath %>/js/dtree.js"></script>
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/agency/addAgency" onsubmit="return findChecks()">
            <div class="form-group">
                <div class="label">
                    <label>农合机构编码：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入农合机构编码" onclick="JavaScript:this.value=''" value="A450421" name="agencyId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>所属行政区域编码：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入所属行政区域编码" onclick="JavaScript:this.value=''" value="450421" name="regionId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>所属行政编码：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入所属行政编码"  onclick="JavaScript:this.value=''" value="667022796" name="admId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>机构名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入机构名称"  onclick="JavaScript:this.value=''" value="龙圩镇合管办" name="mechanismName" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>级别：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入级别"  onclick="JavaScript:this.value=''" value="1" name="levels" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit" > 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/agency/findAgencyAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
