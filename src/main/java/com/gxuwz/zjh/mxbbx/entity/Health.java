package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * (S201_03)  卫生机构（组织）类别
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Health") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Health对象", description="(S201_03)  卫生机构（组织）类别")
public class Health implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "卫生机构（组织）类别编码")
    private String healthId;

    @ApiModelProperty(value = "卫生机构（组织）类别名称")
    private String healthName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
