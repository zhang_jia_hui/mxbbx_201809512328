package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import com.gxuwz.zjh.mxbbx.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
public interface IPayService extends IService<Pay> {

    // 根据 id 查询对应用户信息
    Pay findPayById(Pay pay);

    // 根据 year 查询对应用户信息
    Pay findPayByYear(Pay pay);

    // 查询全部用户信息
    List<Pay> findPayAll();

    //自定义sql 分页
    IPage<Pay> selectPayPage(Page<Pay> page, @Param(Constants.WRAPPER) Wrapper<Pay> wrapper);

    // 添加用户信息
    void addPay(Pay pay);

    // 修改用户信息
    void updatePayById(Pay pay);

    // 删除用户信息
    void deletePayById(Pay pay);

}
