<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/reim/addReim">
            <div class="form-group">
                <div class="label">
                    <label>医院发票号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入医院发票号" value="" name="invoice" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>医院花费：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入预报销金额"  value="" name="wantPay" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>实际报销金额：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入实际报销金额"  value="" name="nowPay" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>报销总金额：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入报销总金额"  value="" name="payAll" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>参合农民身份证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入参合农民身份证号"  value="" name="number" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>慢性病：</label>
                </div>
                <div class="field">
                    <select id="chronicId" style="width:200px" name="chronicId" class="input w50">
                        <option value="100">请选慢性病</option>
                        <c:forEach items="${chronicList}" var="map">
                            <option value="${map.getChronicId()}">${map.getChronicName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>领款时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入领款时间"  value="" id="time" name="time"  />
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                laydate.render({
                  elem: '#time', //指定元素
                  type: 'datetime'
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label>操作员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入操作员姓名"  value="" name="person" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>状态：</label>
                </div>
                <div class="label">
                    <label><input type="radio" name="start" value="0" data-validate="required:必选">未审批</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="start" value="1" checked="checked" >批准</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="start" value="2" >拒批</label>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/reim/findReimAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
