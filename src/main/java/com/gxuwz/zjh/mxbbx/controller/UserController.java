package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.gxuwz.zjh.mxbbx.entity.Role;
import com.gxuwz.zjh.mxbbx.entity.User;
import com.gxuwz.zjh.mxbbx.service.IRoleService;
import com.gxuwz.zjh.mxbbx.service.IUserService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Controller
@RequestMapping("/mxbbx/user")
public class UserController {

    @Autowired
    private IUserService iUserService;
    @Autowired
    private IRoleService iRoleService;


    /**
     * 查询全部用户信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Pointcut
    @RequestMapping(value = "/findUserAll")
    public ModelAndView findUserAll(ModelAndView modelAndView, Page page, Integer pageNumber, User user,
                                    @Param("userId") String userId,
                                    HttpServletRequest request, HttpServletResponse response) {
        // 已经存放到aop 切面类中
        // User user1 = (User) request.getSession().getAttribute("user");
        // if (user1 == null) {
        //     request.setAttribute("result", "none_no");
        //     request.getRequestDispatcher("/login.jsp").forward(request, response);
        // }
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("user_id");
        // 进行模糊查询!!!
        if(userId != null && userId != ""){
            wrapper.like("user_id", userId);
            request.getSession().setAttribute("userId", userId);
        }else {
            request.getSession().setAttribute("userId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 下拉框选择器
        if(user != null & user.getRoleId() != null){
            if(user.getRoleId() != 100){
                wrapper.eq("role_id", user.getRoleId());
            }
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<User> userIPage = iUserService.selectUserPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+userIPage.getTotal());
        System.out.println("总页数"+userIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)userIPage.getPages());
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)userIPage.getPages()];
        for(int i=0; i< (int)userIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        List<User> userList = userIPage.getRecords();
        System.out.println("userList = "+userList);
        modelAndView.addObject("userList", userList);

        // role
        List<Role> roleList = iRoleService.findRoleAll();
        modelAndView.addObject("roleList", roleList);

        modelAndView.setViewName("user/user_list");
        return modelAndView;
    }

    /**
     * 跳转进入新增用户界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertUser")
    public ModelAndView insertUser(ModelAndView modelAndView) {
        modelAndView.setViewName("user/user_insert");
        return modelAndView;
    }

    /**
     * 添加用户信息
     * @param modelAndView
     * @param user
     * @return
     */
    @RequestMapping(value = "/addUser")
    public ModelAndView addUser(ModelAndView modelAndView, User user, HttpServletRequest request) {
        User user1 = iUserService.findUserById(user);
        if(user1 == null){
            iUserService.addUser(user);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("user/user_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改用户界面
     * @param modelAndView
     * @param userId
     * @return
     */
    @RequestMapping(value = "/updateUser")
    public ModelAndView updateUser(ModelAndView modelAndView, int userId) {
        User user = new User();
        user.setUserId(userId);
        User user1 = iUserService.findUserById(user);
        modelAndView.addObject("user", user1);
        modelAndView.setViewName("user/user_update");
        return modelAndView;
    }

    /**
     * 修改用户信息
     * @param modelAndView
     * @param user
     * @return
     */
    @RequestMapping(value = "/editUser")
    public ModelAndView editUser(ModelAndView modelAndView, User user, HttpServletRequest request) {
        iUserService.updateUserById(user);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("user/user_list");
        return modelAndView;
    }

    /**
     * 修改个人信息 -> 跳转到updateUser界面
     * @param modelAndView
     * @param request
     * @return
     */
    @RequestMapping(value = "/editUserAndUpdate")
    public ModelAndView editUserAndUpdate(ModelAndView modelAndView, HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        User user1 = iUserService.findUserById(user);
        modelAndView.addObject("user", user1);
        modelAndView.setViewName("user/user_edit");
        return modelAndView;
    }

    /**
     * 修改个人信息
     * @param modelAndView
     * @param user
     * @param typeName
     * @return
     */
    @RequestMapping(value = "/editUserAndLook")
    public ModelAndView editUserAndLook(ModelAndView modelAndView, User user, String typeName, HttpServletRequest request) {
        int roleId;
        try{
            if(typeName.equals("超级管理员")){
                roleId = 101;
            }else if(typeName.equals("县合管办领导")){
                roleId = 102;
            }else if(typeName.equals("县合管办经办人")){
                roleId = 103;
            }else if(typeName.equals("乡镇农合经办人")){
                roleId = 104;
            }else{
                roleId = 104;
            }
            user.setRoleId(roleId);
            iUserService.updateUserById(user);
            request.setAttribute("result", "upMeTrue");
        }catch (Exception e){
            request.setAttribute("result", "upMeFalse");
        }
        modelAndView.setViewName("user/user_list");
        return modelAndView;
    }

    /**
     * 删除用户信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/deleteUserById")
    public ModelAndView deleteUserById(ModelAndView modelAndView, HttpServletRequest request, int userId) {
        User user = new User();
        user.setUserId(userId);
        iUserService.deleteUserById(user);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("user/user_list");
        return modelAndView;
    }

}
