package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * (S201_02)  隶属关系
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Sub") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Sub对象", description="(S201_02)  隶属关系")
public class Sub implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "隶属关系编码")
    private String subId;

    @ApiModelProperty(value = "隶属关系名称")
    private String subName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
