package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Mapper
@Repository
public interface PayMapper extends BaseMapper<Pay> {

    // 根据 id 查询对应信息
    Pay findPayById(Pay pay);

    // 根据 year 查询对应信息
    Pay findPayByYear(Pay pay);

    // 查询全部信息
    List<Pay> findPayAll();

    //自定义sql 分页
    IPage<Pay> selectPayPage(Page<Pay> page, @Param(Constants.WRAPPER) Wrapper<Pay> wrapper);

    // 添加信息
    void addPay(Pay pay);

    // 修改信息
    void updatePayById(Pay pay);

    // 删除信息
    void deletePayById(Pay pay);

}
