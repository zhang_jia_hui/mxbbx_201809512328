package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 慢性病证
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@Data
@Alias("ChronicCard") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ChronicCard对象", description="慢性病证")
public class ChronicCard implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "慢性病证ID")
    private String cardId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "慢性病名称")
    private String chronicId;

//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    // 只有使用json类型才有效果
//    @JsonFormat(shape =JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd",timezone ="GMT+8")
    @ApiModelProperty(value = "慢性病起始时间")
    private String chronicStartTime;

    @ApiModelProperty(value = "慢性病结束时间")
    private String chronicEndTime;

    @ApiModelProperty(value = "医院证明")
    private String prove;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
