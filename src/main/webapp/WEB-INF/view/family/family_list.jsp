<%@ page import="com.baomidou.mybatisplus.extension.plugins.pagination.Page" %>
<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,target-densitydpi=low-dpi" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<script type="text/javascript">
  // 删除
  function tabledelete(id){
    if(confirm("是否删除 familyId=" + id + "的信息！")){
      window.location.href ="<%=basePath %>/mxbbx/family/deleteFamilyById?familyId=" + id;
    }
  }
</script>
<body>
<form method="post" action="<%=basePath %>mxbbx/family/findFamilyAll" >
    <div class="panel admin-panel">
        <div class="panel-head">
            <strong class="icon-reorder"> 家庭档案信息表</strong>
            <a href="" style="float:right; display:none;">添加字段</a>
        </div>
        <div class="padding border-bottom">
            <ul class="search" style="padding-left:10px;">
                <li>搜索：</li>
                <li>
                    <div class="form-group">
                        <div class="field">
                            <select id="region" style="width:250px" name="region" class="input w50">
                                <option value="">请选择区域</option>
                                <c:forEach items="${regionList}" var="map">
                                    <option <c:if test="${region eq map.regionId}" >selected</c:if> value="${map.regionId}">
                                        <c:if test="${fn:length(map.regionId) == 6}" >${map.regionName}</c:if>
                                        <c:if test="${fn:length(map.regionId) == 8}" >&nbsp;&nbsp;&nbsp;${map.regionName}</c:if>
                                        <c:if test="${fn:length(map.regionId) == 10}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${map.regionName}</c:if>
                                        <c:if test="${fn:length(map.regionId) == 12}" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${map.regionName}</c:if>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </li>
                <li><input type="text" placeholder="请输入待搜索家庭编号" id="keywords" class="input"
                        <c:if test="${familyId != null}" >
                            value="${familyId}"
                        </c:if>
                           name="keywords" style="width:250px; line-height:17px; display:inline-block" />
                    <input type="text" placeholder="请输入户主姓名" id="holder" class="input"
                            <c:if test="${holder != null}" >
                                value="${holder}"
                            </c:if>
                           name="holder" style="width:250px; line-height:17px; display:inline-block" />
                </li>
                <li>
                    <a class="button border-green icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query()"> 搜索</a>
                </li>
                <li><a class="button border-green icon-plus-square-o" name="insertFamily"
                       href="<%=basePath %>/mxbbx/family/insertFamily"> 添加信息</a>
                </li>
            </ul>
        </div>
        <table class="table table-hover text-center" >
            <tr>
                <th style="width:10%">家庭编号</th>
                <th style="width:10%">县级编码</th>
                <th style="width:10%">乡镇编码</th>
                <th style="width:10%">村编码</th>
                <th style="width:10%">组编号</th>
                <th style="width:10%">户主</th>
                <th style="width:5%"></th>
                <th style="width:25%" class="buttonsTwo">操作</th>
            </tr>
            <tr>
                <c:forEach items="${familyList}" var="map">
                    <tr>
                        <td>${map.familyId}</td>
                        <c:forEach items="${regionList}" var="region">
                            <c:if test="${region.getRegionId() eq map.levelId}">
                                <td>${region.getRegionName()}</td>
                            </c:if>
                            <c:if test="${region.getRegionId() eq map.shipId}">
                                <td>${region.getRegionName()}</td>
                            </c:if>
                            <c:if test="${region.getRegionId() eq map.villageId}">
                                <td>${region.getRegionName()}</td>
                            </c:if>
                            <c:if test="${region.getRegionId() eq map.groupId}">
                                <td>${region.getRegionName()}</td>
                            </c:if>
                        </c:forEach>
                        <td>${map.holder}</td>
                        <td class="buttonsTwo"><div class="button-group">
                            <a class="button border-yellow" href="<%=basePath %>/mxbbx/pays/insertPays?familyId=${map.familyId}">
                                <span class="icon-money"></span> 参合缴费</a>
                        </div></td>
                        <td class="buttonsTwo"><div class="button-group">
                            <a class="button border-green" href="<%=basePath %>/mxbbx/families/insertFamilies?familyId=${map.familyId}">
                                <span class="icon-square-o"></span> 新增人员</a>
                            <a class="button border-main" href="<%=basePath %>/mxbbx/family/updateFamily?familyId=${map.familyId}">
                                <span class="icon-edit"></span> 修改</a>
<%--                            <a class="button border-red" href="<%=basePath %>/mxbbx/agency/deleteAgencyById?agencyId=${map.agencyId}">--%>
<%--                                <span class="icon-trash-o"></span> 删除</a>--%>
                            <a class="button border-red tabledelete" onclick="tabledelete(${map.familyId})" value="${map.familyId}">
                                <span class="icon-trash-o"></span> 删除</a>
                        </div></td>
                    </tr>
                </c:forEach>
            </tr>
            <tr>
                <td colspan="15">
                    <div class="pagelist">
                        <a href="<%=basePath %>/mxbbx/family/findFamilyAll？pageNumber=1&familyId=${familyId}">首页</a>
                        <c:choose>
                            <c:when test="${page.current > 1}">
                                <a href="<%=basePath %>/mxbbx/family/findFamilyAll?pageNumber=${page.current-1}&familyId=${familyId}">上一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是第一页');">上一页</a>
                            </c:otherwise>
                        </c:choose>

                        <c:if test="${page.current > 3}">
                            . . .
                        </c:if>
                        <c:forEach items="${pagesList}" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current == status.index + 1}">
                                    <span class="current">${status.index+1}</span>
                                </c:when>
                                <c:when test="${page.current != status.index + 1}">
                                    <c:if test="${page.current + 2 >= status.index + 1 && page.current - 2 <= status.index + 1}">
                                        <a href="<%=basePath %>/mxbbx/family/findFamilyAll?pageNumber=${status.index+1}&familyId=${familyId}">${status.index+1}</a>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${(pages - page.current) > 2}">
                            . . .
                        </c:if>

                        <c:choose>
                            <c:when test="${page.current < pages}">
                                <a href="<%=basePath %>/mxbbx/family/findFamilyAll?pageNumber=${page.current+1}&familyId=${familyId}">下一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是最后一页')">下一页</a>
                            </c:otherwise>
                        </c:choose>
                        <a href="<%=basePath %>/mxbbx/family/findFamilyAll?pageNumber=${pages}&familyId=${familyId}">尾页</a>
                        <div style="font-size:15px;float:right;margin-right:100px">总页数: <span style="color: red;">${pages}</span> 页</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>
<div style="margin: 10px;font-size:15px;">查询数据条数: <span style="color: red;">${page.total}</span> 条</div>
<script type="text/javascript">
  // 模糊查询
  function query() {
    var familyId = $("#keywords").val();
    var holder = $("#holder").val();
    var region = $("#region option:selected").val();
    window.location.href = "<%=basePath %>/mxbbx/family/findFamilyAll?familyId=" + familyId + "&holder=" + holder + "&region=" + region;
  }
</script>
</body>
<script type="text/javascript">
  window.onload=function(){
    var win = false;
    if("addTrue" == "${result1}"){
      win = true;
      alert('√ -添加成功！');
    }else if("addFalse" == "${result1}"){
      win = true;
      alert('x -添加失败,已存在该编号信息!！');
    }
    if("editTrue" == "${result2}"){
      win = true;
      alert('√ -修改成功！');
    }
    if("deleteTrue" == "${result3}"){
      win = true;
      alert('√ -删除成功！');
    }
    if("jurisdiction" == "${result4}"){
      win = true;
      alert('x -权限不够！');
    }
    if(0 == "${pages}"){
      win = true;
      alert('x -数据数量为零！！');
    }
    if("none" == "${pay_why}"){
      win = true;
      alert('x -该家庭户主尚未开启本年度缴费！！');
    }
    if(win == true){
      window.location.href = "<%=basePath %>/mxbbx/family/findFamilyAll";
    }
  };
</script>

<script type="text/javascript">
  window.onload=function(){
    if("none" == "${pay_none}"){
      alert('x -您在本年度不存在缴费记录，已为您添加模板！！');
      window.location.href = "<%=basePath %>/mxbbx/pays/insertPays?familyId=" + ${familyId};
    }
  };
</script>

</html>
