package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Level;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Sub;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * (S201_06)  地域级别 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository

public interface LevelMapper extends BaseMapper<Level> {

    // 根据 id 查询对应用户信息
    Level findLevelById(Level level);

    // 查询全部用户信息
    List<Level> findLevelAll();

    //自定义sql 分页
    IPage<Level> selectLevelPage(Page<Level> page, @Param(Constants.WRAPPER) Wrapper<Level> wrapper);

    // 添加用户信息
    void addLevel(Level level);

    // 修改用户信息
    void updateLevelById(Level level);

    // 删除用户信息
    void deleteLevelById(Level level);

}
