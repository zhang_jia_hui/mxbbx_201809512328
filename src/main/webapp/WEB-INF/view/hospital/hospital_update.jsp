<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/hospital/editHospital">

            <div class="form-group">
                <div class="label">
                    <label>医院ID：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50"  value="${hospital.hospitalId}" name="hospitalId" data-validate="required:建议不修改" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>机构所属经济类型编号：</label>
                </div>
                <div class="field">
                    <select id="economicId" style="width:200px" name="economicId" class="input w50">
                        <c:forEach items="${economicList}" var="map">
                            <option value="${map.getEconomicId()}"
                                    <c:if test="${map.getEconomicId() eq hospital.economicId}">
                                        selected
                                    </c:if>
                            >${map.getEconomicName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>隶属关系编码：</label>
                </div>
                <div class="field">
                    <select id="subId" style="width:200px" name="subId" class="input w50">
                        <c:forEach items="${subList}" var="map">
                            <option value="${map.getSubId()}"
                                    <c:if test="${map.getSubId() eq hospital.subId}">
                                        selected
                                    </c:if>
                            >${map.getSubName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>卫生机构类别编码：</label>
                </div>
                <div class="field">
                    <select id="healthId" style="width:200px" name="healthId" class="input w50">
                        <c:forEach items="${healthList}" var="map">
                            <option value="${map.getHealthId()}"
                                    <c:if test="${map.getHealthId() eq hospital.healthId}">
                                        selected
                                    </c:if>
                            >${map.getHealthName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>机构级别编号：</label>
                </div>
                <div class="field">
                    <select id="mechanismId" style="width:200px" name="mechanismId" class="input w50">
                        <c:forEach items="${mechanismList}" var="map">
                            <option value="${map.getMechanismId()}"
                                    <c:if test="${map.getMechanismId() eq hospital.mechanismId}">
                                        selected
                                    </c:if>
                            >${map.getMechanismName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>地域级别编号：</label>
                </div>
                <div class="field">
                    <select id="levelId" style="width:200px" name="levelId" class="input w50">
                        <c:forEach items="${levelList}" var="map">
                            <option value="${map.getLevelId()}"
                                    <c:if test="${map.getLevelId() eq hospital.levelId}">
                                        selected
                                    </c:if>
                            >${map.getLevelName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>医院名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50"  value="${hospital.name}" name="name" data-validate="required:建议不修改" />
                    <div class="tips"></div>
                </div>
            </div>


            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/hospital/findHospitalAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
