package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Health;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * (S201_03)  卫生机构（组织）类别 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IHealthService extends IService<Health> {

    // 根据 id 查询对应用户信息
    Health findHealthById(Health health);

    // 查询全部用户信息
    List<Health> findHealthAll();

    //自定义sql 分页
    IPage<Health> selectHealthPage(Page<Health> page, @Param(Constants.WRAPPER) Wrapper<Health> wrapper);

    // 添加用户信息
    void addHealth(Health health);

    // 修改用户信息
    void updateHealthById(Health health);

    // 删除用户信息
    void deleteHealthById(Health health);

}
