package com.gxuwz.zjh.mxbbx.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 慢性病报销信息表
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Data
@Alias("ChronicReim") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ChronicReim对象", description="")
public class ChronicReim implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医院发票号")
    private String invoice;

    @ApiModelProperty(value = "医院花费")
    private double wantPay;

    @ApiModelProperty(value = "实际报销金额")
    private double nowPay;

    @ApiModelProperty(value = "报销总金额")
    private double payAll;

    @ApiModelProperty(value = "参合农民身份证号")
    private String number;

    @ApiModelProperty(value = "慢性病编号")
    private String chronicId;

    @ApiModelProperty(value = "领款时间")
    private String time;

    @ApiModelProperty(value = "操作员")
    private String person;

    @ApiModelProperty(value = "状态")
    private int start;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}
