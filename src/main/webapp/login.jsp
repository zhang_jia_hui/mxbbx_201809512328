<%@ page contentType="text/html; charset=utf-8" %>
<%@ include file="./WEB-INF/view/common/common.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>登录界面</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>

<%-- 4位随机数字字母验证码 --%>
<%--<script type='text/javascript'>--%>
<%--  var code ; //在全局定义验证码--%>
<%--  function createCode(){--%>
<%--    code = "";--%>
<%--    var codeLength = 4;//验证码的长度--%>
<%--    var checkCode = document.getElementById("code");--%>
<%--    var random = new Array(0,1,2,3,4,5,6,7,8,9,--%>
<%--        'A','B','C','D','E','F','G','H','I','J',--%>
<%--        'K','L','M','N','O','P','Q','R','S','T',--%>
<%--        'U','V','W','X','Y','Z');//随机数--%>
<%--    for(var i = 0; i < codeLength; i++) {//循环操作--%>
<%--      var index = Math.floor(Math.random()*36);//取得随机数的索引（0~35）--%>
<%--      code += random[index];//根据索引取得随机数加到code上--%>
<%--    }--%>
<%--    checkCode.value = code;//把code值赋给验证码--%>
<%--  }--%>
<%--  //校验验证码--%>
<%--  function validate(){--%>
<%--    var inputCode = document.getElementById("input").value.toUpperCase(); //取得输入的验证码并转化为大写--%>
<%--    if(inputCode.length < 4) { //若输入的验证码长度小于4--%>
<%--      alert("请正确输入验证码！");--%>
<%--      return false;--%>
<%--    }else if(inputCode != code ) { //若输入的验证码与产生的验证码不一致时--%>
<%--      alert("验证码错误！");--%>
<%--      createCode();//刷新验证码--%>
<%--      document.getElementById("input").value = "";//清空文本框--%>
<%--      return false;--%>
<%--    }--%>
<%--    return true;--%>
<%--  }--%>
<%--</script>--%>

<%-- 算术验证码 --%>
<script type='text/javascript'>
  var code ; //在全局定义验证码
  function createCode(){
    code = "";
    var checkCode = document.getElementById("code");
    var index1 =  Math.floor(Math.random()*10);//取得随机数（0~9）
    var index3 = Math.floor(Math.random()*10);//取得随机数（0~9）
    var timestamp = (new Date()).valueOf();// 获取当前时间戳
    var staps = parseInt(timestamp%10);// 求余得到个位数，进行伪随机事件

    // 进行验证码拼接
    code +=index1;
    // 伪随机算法
    if(staps == 1 | staps == 4 |staps == 7 |staps == 0){
      code += "+";
    }else if(staps == 2 | staps == 5 |staps == 8) {
      code += "-";
    }else if(staps == 3 | staps == 6 |staps == 9) {
      code += "×";
    }
    code +=index3 + "=";

    checkCode.value = code; // 把code值赋给验证码
  }
  //校验验证码
  function validate(){
    var codeString = document.getElementById("code").value.toString();
    if(codeString == "点击刷新"){
      alert("请刷新验证码!");
      return false;
    }
    var number;
    var inputCode = document.getElementById("input").value;
    if(inputCode != null && inputCode != ""){
      var split1 = code.split("=")[0];
      if(split1.split("+")[1] != "" && split1.split("+")[1] != null){
        number = parseInt(split1.split("+")[0]) + parseInt(split1.split("+")[1]);
      }
      if(split1.split("-")[1] != "" && split1.split("-")[1] != null){
        number = parseInt(split1.split("-")[0]) - parseInt(split1.split("-")[1]);
      }
      if(split1.split("×")[1] != "" && split1.split("×")[1] != null){
        number = parseInt(split1.split("×")[0]) * parseInt(split1.split("×")[1]);
      }
      if(number != inputCode ) {
        alert("验证码错误!");
        createCode();//刷新验证码
        document.getElementById("input").value = "";//清空文本框
        return false;
      }
      return true;
    }else {
      alert("请输入验证码!");
      return false;
    }
  }
</script>
<script type="text/javascript">
  window.onload=function(){
    createCode();
  }
</script>
<style type='text/css'>
    #code{
        font-family:Arial,宋体;
        font-style:italic;
        color:green;
        border:0;
        padding:2px 3px;
        letter-spacing:3px;
        font-weight:bolder;
    }
</style>
<body onload='createCode()'>
<div class="bg"></div>
<div class="container">
    <div class="line bouncein">
        <div class="xs6 xm4 xs3-move xm4-move" >
            <div style="margin-top:25%"></div>
            <div class="media media-y margin-big-bottom">
            </div>
            <form action="<%=basePath %>/login" method="post" onsubmit="return validate()">
                <div class="panel loginbox">
                    <div class="text-center margin-big padding-big-top"><h1>慢性病报销系统</h1></div>
                    <div class="panel-body" style="padding:100px; padding-bottom:10px; padding-top:10px;">
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="text" class="input input-big" name="userId" id="userId" placeholder="登录账号"  autocomplete="off"
                                       onclick="JavaScript:this.value=''" value="123" data-validate="required:请填写账号" maxlength="8"
                                       onkeyup="value=value.replace(/[^0-9]/g,'')" onpaste="value=value.replace(/[^0-9]/g,'')" oncontextmenu = "value=value.replace(/[^0-9]/g,'')"/>
                                <span class="icon icon-user margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="password" class="input input-big" name="passWord" id="passWord" placeholder="登录密码"  autocomplete="off"
                                       onclick="JavaScript:this.value=''" value="123" data-validate="required:请填写密码" />
                                <span class="icon icon-eye-slash margin-small" id="eye" onclick="hideShowPsw()"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <div class="field">
                                    <input type="text" class="input input-big" id="input"
                                           placeholder="填写右侧的验证码" value="" maxlength="4"
                                           data-validate="required:请填写验证码" />
                                    <input width="100" height="32" class="passcode"
                                           style="height:43px;cursor:pointer;width:80px"
                                           type="button" id="code" onclick="createCode()"
                                           value="点击刷新"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style="padding:100px; padding-bottom:60px; padding-top:20px;">
                        <input type="submit" name="button" class="button button-block bg-main text-big input-big" value="登录">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function chkinput(form) {
      if(form.userId.value=="") {
        alert("请输入用户名!");//如果为空弹出提示消息
        form.username.focus();//返回指定位置
        return false;
      }

      if(form.passWord.value=="") {
        alert("请输入密码！");//如果为空弹出提示消息
        form.password.focus();//返回指定位置
        return false;
      }

      return true;
    }
</script>

<script type="text/javascript">
  window.onload=function(){
    if (window != top){
      top.location.href = location.href;
      alert('登录超时!或者账号在异地登陆，请重新登陆！');
    }
    if ("none" == "${result}") {
      alert('密码错误！');
    }else if("full" == "${result}") {
      alert('该账号不存在！');
    }
  };　　
</script>

<%-- 显示/隐藏 密码 --%>
<script type="text/javascript">
  var password = document.getElementById("passWord");
  var eye = document.getElementById("eye");
  //隐藏text block，显示password block
  function hideShowPsw(){
    if (password.type == "password") {
      password.type = "text";
      eye.className='icon icon-eye margin-small'
    }else {
      password.type = "password";
      eye.className='icon icon-eye-slash margin-small'
    }
  }
</script>
</html>
