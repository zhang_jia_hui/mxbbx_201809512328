<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/pays/editPays">

            <div class="form-group">
                <div class="label">
                    <label>缴费记录编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${pays.paysId}" name="PaysId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${pays.familyId}" name="familyId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>当次缴费人：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${pays.nowName}" name="nowName" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭已缴费人员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${pays.familyName}" name="familyName" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭未缴费人员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${pays.familyNoName}" name="familyNoName" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>未缴费人数：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" value="${pays.familyNum}" name="familyNum" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>当次缴费：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" value="${pays.money}" name="money" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费总金额：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" value="${pays.moneys}" name="moneys" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费年份：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" value="${pays.year}" name="year" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${pays.time}" name="time" data-validate="required:必填" value="2021-05-01"/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                laydate.render({
                  elem: '#time' //指定元素
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="paymit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/pays/findPaysAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
