package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-19
 */
public interface IRoleService extends IService<Role> {

    // 根据 id 查询对应用户信息
    Role findRoleById(Role role);

    // 查询全部用户信息
    List<Role> findRoleAll();

    //自定义sql 分页
    IPage<Role> selectRolePage(Page<Role> page, @Param(Constants.WRAPPER) Wrapper<Role> wrapper);

    // 添加用户信息
    void addRole(Role role);

    // 修改用户信息
    void updateRoleById(Role role);

    // 删除用户信息
    void deleteRoleById(Role role);

}
