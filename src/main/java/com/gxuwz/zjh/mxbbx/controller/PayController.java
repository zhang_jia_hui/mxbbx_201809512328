package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import com.gxuwz.zjh.mxbbx.service.IPayService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 参合缴费标准 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/pay")
public class PayController {

    @Autowired
    private IPayService iPayService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findPayAll")
    public ModelAndView findPayAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                   @Param("PayId") String payId,
                                        HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Pay> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("pay_id");
        // 进行模糊查询!!!
        if(payId != null && payId != ""){
            wrapper.like("pay_id", payId);
            request.getSession().setAttribute("payId", payId);
        }else {
            request.getSession().setAttribute("payId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Pay> payIPage = iPayService.selectPayPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)payIPage.getPages()];
        for(int i=0; i< (int)payIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+payIPage.getTotal());
        System.out.println("总页数"+payIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)payIPage.getPages());
        List<Pay> payList = payIPage.getRecords();
        System.out.println("payList = "+payList);
        modelAndView.addObject("payList", payList);
        modelAndView.setViewName("pay/pay_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertPay")
    public ModelAndView insertPay(ModelAndView modelAndView) {
        modelAndView.setViewName("pay/pay_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addPay")
    public ModelAndView addPay(ModelAndView modelAndView, Pay pay, HttpServletRequest request) {
        Pay Pay1 = iPayService.findPayById(pay);
        if(Pay1 == null){
            iPayService.addPay(pay);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("pay/pay_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updatePay")
    public ModelAndView updatePay(ModelAndView modelAndView, String payId) {
        Pay pay = new Pay();
        pay.setPayId(payId);
        Pay pay1 = iPayService.findPayById(pay);
        modelAndView.addObject("pay", pay1);
        modelAndView.setViewName("pay/pay_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editPay")
    public ModelAndView editPay(ModelAndView modelAndView, Pay pay, HttpServletRequest request) {
        iPayService.updatePayById(pay);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("pay/pay_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deletePayById")
    public ModelAndView deletePayById(ModelAndView modelAndView, HttpServletRequest request, String payId) {
        Pay pay = new Pay();
        pay.setPayId(payId);
        iPayService.deletePayById(pay);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("pay/pay_list");
        return modelAndView;
    }

}
