<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/pay/addPay">
            <div class="form-group">
                <div class="label">
                    <label>缴费标准编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入缴费标准编号(例: N4325202105101929)"
                           value="N4325202105101929(需唯一,请修改)" name="payId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>参合缴费标准：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入参合缴费标准(例: 1000*n)"
                           value="1000*n" name="payText" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>参合缴费年份：</label>
                </div>
                <div class="field">
                    <textarea cols="1" class="text input w50" name="year" placeholder="请输入参合缴费年份(例: 2020)"></textarea>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>缴费金额：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入缴费金额(例: 1000)"
                           value="1000" name="money" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>


            <div class="form-group">
                <div class="label">
                    <label>起始时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入起始时间" id="startTime"
                           name="startTime" data-validate="required:必填" value="2021-05-01"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>终止时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入终止时间" id="endTime"
                           name="endTime" data-validate="required:必填" value="2021-05-01"/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#startTime' //指定元素
                });
                laydate.render({
                  elem: '#endTime' //指定元素
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="paymit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/pay/findPayAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
