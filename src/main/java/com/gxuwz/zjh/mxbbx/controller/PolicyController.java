package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.entity.Policy;
import com.gxuwz.zjh.mxbbx.service.IAgencyService;
import com.gxuwz.zjh.mxbbx.service.IPolicyService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 慢病政策数据模型 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/policy")
public class PolicyController {

    @Autowired
    private IPolicyService iPolicyService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Pointcut
    @RequestMapping(value = "/findPolicyAll")
    public ModelAndView findPolicyAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                      @Param("year") String year,
                                      HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Policy> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("year");
        // 进行模糊查询!!!
        if(year != null && year != ""){
            wrapper.like("year", year);
            request.getSession().setAttribute("year", year);
        }else {
            request.getSession().setAttribute("year", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Policy> policyIPage = iPolicyService.selectPolicyPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)policyIPage.getPages()];
        for(int i=0; i< (int)policyIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+policyIPage.getTotal());
        System.out.println("总页数"+policyIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)policyIPage.getPages());
        List<Policy> policyList = policyIPage.getRecords();
        System.out.println("policyList = "+policyList);
        modelAndView.addObject("policyList", policyList);
        modelAndView.setViewName("policy/policy_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertPolicy")
    public ModelAndView insertPolicy(ModelAndView modelAndView) {
        modelAndView.setViewName("policy/policy_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addPolicy")
    public ModelAndView addPolicy(ModelAndView modelAndView, Policy policy, HttpServletRequest request) {
        Policy policy1 = iPolicyService.findPolicyById(policy);
        if(policy1 == null){
            iPolicyService.addPolicy(policy);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("policy/policy_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updatePolicy")
    public ModelAndView updatePolicy(ModelAndView modelAndView, String policyId) {
        Policy policy = new Policy();
        policy.setPolicyId(policyId);
        Policy policy1 = iPolicyService.findPolicyById(policy);
        modelAndView.addObject("policy", policy1);
        modelAndView.setViewName("policy/policy_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editPolicy")
    public ModelAndView editChronic(ModelAndView modelAndView, Policy policy, HttpServletRequest request) {
        iPolicyService.updatePolicyById(policy);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("policy/policy_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deletePolicyById")
    public ModelAndView deletePolicyById(ModelAndView modelAndView, HttpServletRequest request, String policyId) {
        Policy policy = new Policy();
        policy.setPolicyId(policyId);
        iPolicyService.deletePolicyById(policy);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("policy/policy_list");
        return modelAndView;
    }

}
