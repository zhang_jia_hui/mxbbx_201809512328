package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Level;
import com.gxuwz.zjh.mxbbx.entity.Limit;
import com.gxuwz.zjh.mxbbx.entity.Personnel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 人员信息表 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@Mapper
@Repository
public interface PersonnelMapper extends BaseMapper<Personnel> {

    // 根据 id 查询对应用户信息
    Personnel findPersonnelById(Personnel personnel);

    // 查询全部用户信息
    List<Personnel> findPersonnelAll();

    //自定义sql 分页
    IPage<Personnel> selectPersonnelPage(Page<Personnel> page, @Param(Constants.WRAPPER) Wrapper<Personnel> wrapper);

    // 添加用户信息
    void addPersonnel(Personnel personnel);

    // 修改用户信息
    void updatePersonnelById(Personnel personnel);

    // 删除用户信息
    void deletePersonnelById(Personnel personnel);

}
