package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.*;
import com.gxuwz.zjh.mxbbx.service.*;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 家庭档案信息表 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/family")
public class FamilyController {

    @Autowired
    private IFamilyService iFamilyService;
    @Autowired
    private IRegionService iRegionService;
    @Autowired
    private IPersonnelService iPersonnelService;
    @Autowired
    private IFamiliesService iFamiliesService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findFamilyAll")
    public ModelAndView findFamilyAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                      HttpServletRequest request, HttpServletResponse response,
                                      @Param("region") String region,
                                      @Param("familyId") String familyId,
                                      @Param("holder") String holder) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Family> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("family_id");
        // 进行模糊查询!!!
        if(region != null && region != ""){
            switch (region.length()){
                case 6:
                    wrapper.like("level_id", region);
                    break;
                case 8:
                    wrapper.like("ship_id", region);
                    break;
                case 10:
                    wrapper.like("village_id", region);
                    break;
                case 12:
                    wrapper.like("group_id", region);
                    break;
            }
            request.getSession().setAttribute("region", region);
        }else {
            request.getSession().setAttribute("region", "");
        }
        if(familyId != null && familyId != ""){
            wrapper.like("family_id", familyId);
            request.getSession().setAttribute("familyId", familyId);
        }else {
            request.getSession().setAttribute("familyId", "");
        }
        if(holder != null && holder != ""){
            wrapper.like("holder", holder);
            request.getSession().setAttribute("holder", holder);
        }else {
            request.getSession().setAttribute("holder", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Family> familyIPage = iFamilyService.selectFamilyPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)familyIPage.getPages()];
        for(int i=0; i< (int)familyIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+familyIPage.getTotal());
        System.out.println("总页数"+familyIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)familyIPage.getPages());
        List<Family> familyList = familyIPage.getRecords();
        System.out.println("familyList = "+familyList);
        modelAndView.addObject("familyList", familyList);

        // 通过iRegionService获取对应的地址信息
        List<Region> regionList = iRegionService.findRegionAll();
        modelAndView.addObject("regionList", regionList);

        modelAndView.setViewName("family/family_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertFamily")
    public ModelAndView insertFamily(ModelAndView modelAndView, HttpServletRequest request) {
        List<Region> regionList = iRegionService.findRegionAll();
        request.getSession().setAttribute("regionList", regionList);
        modelAndView.setViewName("family/family_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addFamily")
    public ModelAndView addFamily(ModelAndView modelAndView, Family family, Personnel personnel, Families families,
                                  HttpServletRequest request,
                                  @Param("establishTime") String establishTime,
                                  @Param("birthday") String birthday,
                                  @Param("marriageTime") String marriageTime ) throws ParseException {
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        String establishTime1 = null;
        String birthday1 = null;
        String marriageTime1 = null;
        if(establishTime != null && establishTime != ""){
            Date time1 = formatter1.parse(establishTime);
            establishTime1 =formatter1.format(time1);
        }
        if(birthday != null && birthday != ""){
            Date time1 = formatter2.parse(birthday);
            birthday1 =formatter2.format(time1);
        }
        if(marriageTime != null && marriageTime != ""){
            Date time1 = formatter1.parse(marriageTime);
            marriageTime1 =formatter1.format(time1);
        }
        System.out.println("establishTime1 = " + establishTime1);
        System.out.println("birthday1 = " + birthday1);
        System.out.println("marriageTime1 = " + marriageTime1);

        family.setEstablishTime(establishTime1);
        personnel.setBirthday(birthday1);
        personnel.setMarriageTime(marriageTime1);

        family.setHolder(personnel.getName());
        family.setNumber(personnel.getNumber());
        Family family1 = iFamilyService.findFamilyById(family);
        Personnel personnel1 = iPersonnelService.findPersonnelById(personnel);
        Families families1 = iFamiliesService.findFamiliesById(families);
        if(family1 == null && personnel1 == null && families1 == null){
            iFamilyService.addFamily(family);
            iPersonnelService.addPersonnel(personnel);
            iFamiliesService.addFamilies(families);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("family/family_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateFamily")
    public ModelAndView updateFamily(ModelAndView modelAndView, String familyId, HttpServletRequest request) {
        List<Region> regionList = iRegionService.findRegionAll();
        request.getSession().setAttribute("regionList", regionList);
        Family family = new Family();
        family.setFamilyId(familyId);
        Family family1 = iFamilyService.findFamilyById(family);
        modelAndView.addObject("family", family1);
        modelAndView.setViewName("family/family_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editFamily")
    public ModelAndView editFamily(ModelAndView modelAndView, Family family, HttpServletRequest request,
                                   @Param("establishTime") String establishTime) throws ParseException {

        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String establishTime1 = null;
        if(establishTime != null && establishTime1 != ""){
            Date time1 = formatter1.parse(establishTime);
            establishTime1 =formatter1.format(time1);
        }
        System.out.println("establishTime1 = " + establishTime1);

        family.setEstablishTime(establishTime1);

        iFamilyService.updateFamilyById(family);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("family/family_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteFamilyById")
    public ModelAndView deleteFamilyById(ModelAndView modelAndView, HttpServletRequest request, String familyId) {
        Family family = new Family();
        family.setFamilyId(familyId);
        iFamilyService.deleteFamilyById(family);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("family/family_list");
        return modelAndView;
    }

}
