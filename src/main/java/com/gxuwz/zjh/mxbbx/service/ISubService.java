package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Sub;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * (S201_02)  隶属关系 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface ISubService extends IService<Sub> {

    // 根据 id 查询对应用户信息
    Sub findSubById(Sub sub);

    // 查询全部用户信息
    List<Sub> findSubAll();

    //自定义sql 分页
    IPage<Sub> selectSubPage(Page<Sub> page, @Param(Constants.WRAPPER) Wrapper<Sub> wrapper);

    // 添加用户信息
    void addSub(Sub sub);

    // 修改用户信息
    void updateSubById(Sub sub);

    // 删除用户信息
    void deleteSubById(Sub sub);

}
