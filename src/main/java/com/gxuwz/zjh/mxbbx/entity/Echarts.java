package com.gxuwz.zjh.mxbbx.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;


/**
 * 临时的图表类
 */
@Data
@Alias("Echarts") //别名
public class Echarts {

    private String eid;
    private String name;
    private int value;
    private List<Echarts> children;

}
