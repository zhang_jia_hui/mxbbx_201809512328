package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Region;
import com.gxuwz.zjh.mxbbx.service.IRegionService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 行政区域信息模型 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/region")
public class RegionController {

    @Autowired
    private IRegionService iRegionService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Pointcut
    @RequestMapping(value = "/findRegionAll")
    public ModelAndView findRegionAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                      @Param("regionId") String regionId,
                                    HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Region> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("region_id");
        // 进行模糊查询!!!
        if(regionId != null && regionId != ""){
            wrapper.like("region_id", regionId);
            request.getSession().setAttribute("regionId", regionId);
        }else {
            request.getSession().setAttribute("regionId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Region> regionIPage = iRegionService.selectRegionPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)regionIPage.getPages()];
        for(int i=0; i< (int)regionIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+regionIPage.getTotal());
        System.out.println("总页数"+regionIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)regionIPage.getPages());
        List<Region> regionList = regionIPage.getRecords();
        System.out.println("regionList = "+regionList);
        modelAndView.addObject("regionList", regionList);
        modelAndView.setViewName("region/region_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertRegion")
    public ModelAndView insertRegion(ModelAndView modelAndView) {
        modelAndView.setViewName("region/region_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addRegion")
    public ModelAndView addRegion(ModelAndView modelAndView, Region region, HttpServletRequest request) {
        Region region1 = iRegionService.findRegionById(region);
        if(region1 == null){
            iRegionService.addRegion(region);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("region/region_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateRegion")
    public ModelAndView updateRegion(ModelAndView modelAndView, String regionId) {
        Region region = new Region();
        region.setRegionId(regionId);
        Region region1 = iRegionService.findRegionById(region);
        modelAndView.addObject("region", region1);
        modelAndView.setViewName("region/region_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editRegion")
    public ModelAndView editRegion(ModelAndView modelAndView, Region region, HttpServletRequest request) {
        iRegionService.updateRegionById(region);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("region/region_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteRegionById")
    public ModelAndView deleteRegionById(ModelAndView modelAndView, HttpServletRequest request, String regionId) {
        Region region = new Region();
        region.setRegionId(regionId);
        iRegionService.deleteRegionById(region);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("region/region_list");
        return modelAndView;
    }

}
