package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Region;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface ChronicMapper extends BaseMapper<Chronic> {

    // 根据 id 查询对应用户信息
    Chronic findChronicById(Chronic chronic);

    // 查询全部用户信息
    List<Chronic> findChronicAll();

    //自定义sql 分页
    IPage<Chronic> selectChronicPage(Page<Chronic> page, @Param(Constants.WRAPPER) Wrapper<Chronic> wrapper);

    // 添加用户信息
    void addChronic(Chronic chronic);

    // 修改用户信息
    void updateChronicById(Chronic chronic);

    // 删除用户信息
    void deleteChronicById(Chronic chronic);

}
