<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/hospital/addHospital">
            <div class="form-group">
                <div class="label">
                    <label>医院ID：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入医院ID(例: 1110A10015)" onclick="JavaScript:this.value=''" value="1110A10015" name="hospitalId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>机构所属经济类型编号：</label>
                </div>
                <div class="field">
                    <select id="economicId" style="width:200px" name="economicId" class="input w50">
                        <option value="1110A10015">请选择所属经济类型</option>
                        <c:forEach items="${economicList}" var="map">
                            <option value="${map.getEconomicId()}">${map.getEconomicName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>隶属关系编码：</label>
                </div>
                <div class="field">
                    <select id="subId" style="width:200px" name="subId" class="input w50">
                        <option value="100">请选择隶属关系</option>
                        <c:forEach items="${subList}" var="map">
                            <option value="${map.getSubId()}">${map.getSubName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>卫生机构类别编码：</label>
                </div>
                <div class="field">
                    <select id="healthId" style="width:200px" name="healthId" class="input w50">
                        <option value="100">请选择卫生机构类别</option>
                        <c:forEach items="${healthList}" var="map">
                            <option value="${map.getHealthId()}">${map.getHealthName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>机构级别编号：</label>
                </div>
                <div class="field">
                    <select id="mechanismId" style="width:200px" name="mechanismId" class="input w50">
                        <option value="100">请选择机构级别</option>
                        <c:forEach items="${mechanismList}" var="map">
                            <option value="${map.getMechanismId()}">${map.getMechanismName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>地域级别编号：</label>
                </div>
                <div class="field">
                    <select id="levelId" style="width:200px" name="levelId" class="input w50">
                        <option value="100">请选择地域级别</option>
                        <c:forEach items="${levelList}" var="map">
                            <option value="${map.getLevelId()}">${map.getLevelName()}</option>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入名称(例: 中央医院)" onclick="JavaScript:this.value=''" value="中央医院" name="name" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/hospital/findHospitalAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
