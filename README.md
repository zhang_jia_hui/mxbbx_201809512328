 **慢性病报销系统** 

--------------------
技术：SpringBoot + Html/css

--------------------
个人制作模块介绍：

- 权限模块：前端权限模块 + 后端权限模块
- 登录监听：监听器 + 拦截器
- 业务流程：接口设计
- 查询分页：MyBatis-Plus
- 前端显示：layui + Bootstrap
- 数据库：mysql
- 登录验证码：个人js前端制作

--------------------

#### 项目部署

1. 将项目导入到idea中
2. 修改数据库配置文件
3. 修改maven路径并刷新maven仓库

#### 页面展示


<table>
    <tr>
        <td><img src="src/main/webapp/images/admin/%E7%99%BB%E5%BD%95.png"/></td>
        <td><img src="src/main/webapp/images/admin/%E6%AC%A2%E8%BF%8E%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="src/main/webapp/images/admin/%E7%94%A8%E6%88%B7%E4%BF%A1%E6%81%AF.png"/></td>
        <td><img src="src/main/webapp/images/admin/%E6%9D%83%E9%99%90%E7%95%8C%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="src/main/webapp/images/admin/%E5%AE%B6%E5%BA%AD%E6%A1%A3%E6%A1%88.png"/></td>
        <td><img src="src/main/webapp/images/admin/%E5%8C%BB%E7%96%97%E6%9C%BA%E6%9E%84.png"/></td>
    </tr>
    <tr>
        <td><img src="src/main/webapp/images/admin/%E5%8F%82%E5%90%88%E5%86%9C%E6%B0%91.png"/></td>
        <td><img src="src/main/webapp/images/admin/%E5%8F%82%E5%90%88%E7%BC%B4%E8%B4%B9.png"/></td>
    </tr>
    <tr> 
        <td><img src="src/main/webapp/images/admin/%E6%85%A2%E6%80%A7%E7%97%85%E8%AF%81.png"/></td>
        <td><img src="src/main/webapp/images/admin/%E6%85%A2%E6%80%A7%E7%97%85%E6%8A%A5%E9%94%80%E7%BB%9F%E8%AE%A1.png"/></td>
    </tr>
</table>