package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Policy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Region;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 慢病政策数据模型 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface PolicyMapper extends BaseMapper<Policy> {

    // 根据 id 查询对应用户信息
    Policy findPolicyById(Policy policy);

    // 查询全部用户信息
    List<Policy> findPolicyAll();

    // 根据 year 查询是否存在当年的信息
    Policy findPolicyByYear(Policy policy);

    //自定义sql 分页
    IPage<Policy> selectPolicyPage(Page<Policy> page, @Param(Constants.WRAPPER) Wrapper<Policy> wrapper);

    // 添加用户信息
    void addPolicy(Policy policy);

    // 修改用户信息
    void updatePolicyById(Policy policy);

    // 删除用户信息
    void deletePolicyById(Policy policy);

}
