<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改角色信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/limit/editLimit">
            <div class="form-group">
                <div class="label">
                    <label>权限名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${limit.limitId}" name="limitId" data-validate="required:建议不修改" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>上一级：</label>
                </div>
                <div class="field">
                    <select id="limitGrade" style="width:300px" name="limitGrade" class="input w50">
                        <option value="-1">请选择上一级权限(若无，则默认为第一级)</option>
                        <c:forEach items="${limits}" var="map">
                            <c:if test="${map.limitGrade eq '-1'}">
                                <option value="${map.id}" <c:if test="${map.id eq limit.limitGrade}">
                                    selected
                                </c:if>>①级权限-----${map.limitText}</option>
                            </c:if>
                            <c:if test="${map.limitGrade != '-1'}">
                                <option value="${map.id}" <c:if test="${map.id eq limit.limitGrade}">
                                    selected
                                </c:if>>②级权限-----${map.limitText}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>权限描述：</label>
                </div>
                <div class="field">
                    <textarea cols="8" style="height: 300px" class="text input w50" name="limitText" data-validate="required:" >${limit.limitText}</textarea>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/limit/findLimitAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
