package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Families;
import com.gxuwz.zjh.mxbbx.mapper.FamiliesMapper;
import com.gxuwz.zjh.mxbbx.service.IFamiliesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参合人员档案信息表 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class FamiliesServiceImpl extends ServiceImpl<FamiliesMapper, Families> implements IFamiliesService {

    @Autowired
    private FamiliesMapper familiesMapper;

    @Override
    public Families findFamiliesById(Families families) {
        return familiesMapper.findFamiliesById(families);
    }

    @Override
    public List<Families> findFamiliesByFamily(Families families) {
        return familiesMapper.findFamiliesByFamily(families);
    }

    @Override
    public Families findFamiliesByNonghe(Families families) {
        return familiesMapper.findFamiliesByNonghe(families);
    }

    @Override
    public Families findFamiliesByNumber(Families families) {
        return familiesMapper.findFamiliesByNumber(families);
    }

    @Override
    public List<Families> findFamiliesAll() {
        return familiesMapper.findFamiliesAll();
    }

    @Override
    public IPage<Families> selectFamiliesPage(Page<Families> page, Wrapper<Families> wrapper) {
        return familiesMapper.selectFamiliesPage(page, wrapper);
    }

    @Override
    public void addFamilies(Families families) {
        familiesMapper.addFamilies(families);
    }

    @Override
    public void updateFamiliesById(Families families) {
        familiesMapper.updateFamiliesById(families);
    }

    @Override
    public void deleteFamiliesById(Families families) {
        familiesMapper.deleteFamiliesById(families);
    }
}
