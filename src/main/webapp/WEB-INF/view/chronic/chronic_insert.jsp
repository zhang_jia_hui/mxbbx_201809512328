<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <%--  导入dtree  --%>
    <link rel="StyleSheet" href="<%=basePath %>/css/dtree.css" type="text/css" />
    <script type="text/javascript" src="<%=basePath %>/js/dtree.js"></script>
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/chronic/addChronic" onsubmit="return findChecks()">
            <div class="form-group">
                <div class="label">
                    <label>经办机构编码：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入疾病编码" onclick="JavaScript:this.value=''" value="K74.1512" name="chronicId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>所属行政编码：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入拼音码"  onclick="JavaScript:this.value=''" value="gyh" name="nameId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>机构名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入疾病名称"  onclick="JavaScript:this.value=''" value="肝硬化" name="chronicName" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit" > 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/chronic/findChronicAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
