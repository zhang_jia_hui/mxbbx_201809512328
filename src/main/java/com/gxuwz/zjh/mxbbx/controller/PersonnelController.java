package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.ChronicCard;
import com.gxuwz.zjh.mxbbx.entity.Personnel;
import com.gxuwz.zjh.mxbbx.service.IChronicCardService;
import com.gxuwz.zjh.mxbbx.service.IPersonnelService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 人员信息表 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@RestController
@RequestMapping("/mxbbx/personnel")
public class PersonnelController {

    @Autowired
    private IPersonnelService iPersonnelService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findPersonnelAll")
    public ModelAndView findPersonnelAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                         @Param("number") String number,
                                           HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Personnel> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("number");
        // 进行模糊查询!!!
        if(number != null && number != ""){
            wrapper.like("number", number);
            request.getSession().setAttribute("number", number);
        }else {
            request.getSession().setAttribute("number", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Personnel> personnelIPage = iPersonnelService.selectPersonnelPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)personnelIPage.getPages()];
        for(int i=0; i< (int)personnelIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+personnelIPage.getTotal());
        System.out.println("总页数"+personnelIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)personnelIPage.getPages());
        List<Personnel> personnelList = personnelIPage.getRecords();
        System.out.println("personnelList = "+personnelList);
        modelAndView.addObject("personnelList", personnelList);
        modelAndView.setViewName("personnel/personnel_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertPersonnel")
    public ModelAndView insertPersonnel(ModelAndView modelAndView) {
        modelAndView.setViewName("personnel/personnel_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addPersonnel")
    public ModelAndView addPersonnel(ModelAndView modelAndView, Personnel personnel, HttpServletRequest request,
                                     @Param("birthday") String birthday,
                                     @Param("marriageTime") String marriageTime) throws ParseException {

        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        String birthday1 = null;
        String marriageTime1 = null;
        if(birthday != null && birthday != ""){
            Date time1 = formatter2.parse(birthday);
            birthday1 =formatter2.format(time1);
        }
        if(marriageTime != null && marriageTime != ""){
            Date time1 = formatter1.parse(marriageTime);
            marriageTime1 =formatter1.format(time1);
        }
        System.out.println("birthday1 = " + birthday1);
        System.out.println("marriageTime1 = " + marriageTime1);

        personnel.setBirthday(birthday1);
        personnel.setMarriageTime(marriageTime1);

        Personnel personnel1 = iPersonnelService.findPersonnelById(personnel);
        if(personnel1 == null){
            iPersonnelService.addPersonnel(personnel);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("personnel/personnel_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updatePersonnel")
    public ModelAndView updatePersonnel(ModelAndView modelAndView, String number) {
        Personnel personnel = new Personnel();
        personnel.setNumber(number);
        Personnel personnel1 = iPersonnelService.findPersonnelById(personnel);
        modelAndView.addObject("personnel", personnel1);
        modelAndView.setViewName("personnel/personnel_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editPersonnel")
    public ModelAndView editPersonnel(ModelAndView modelAndView, Personnel personnel, HttpServletRequest request,
                                      @Param("birthday") String birthday,
                                      @Param("marriageTime") String marriageTime) throws ParseException {

        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        String birthday1 = null;
        String marriageTime1 = null;
        if(birthday != null && birthday != ""){
            Date time1 = formatter2.parse(birthday);
            birthday1 =formatter2.format(time1);
        }
        if(marriageTime != null && marriageTime != ""){
            Date time1 = formatter1.parse(marriageTime);
            marriageTime1 =formatter1.format(time1);
        }
        System.out.println("birthday1 = " + birthday1);
        System.out.println("marriageTime1 = " + marriageTime1);

        personnel.setBirthday(birthday1);
        personnel.setMarriageTime(marriageTime1);

        iPersonnelService.updatePersonnelById(personnel);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("personnel/personnel_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deletePersonnelById")
    public ModelAndView deletePersonnelById(ModelAndView modelAndView, HttpServletRequest request, String number) {
        Personnel personnel = new Personnel();
        personnel.setNumber(number);
        iPersonnelService.deletePersonnelById(personnel);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("personnel/personnel_list");
        return modelAndView;
    }

}
