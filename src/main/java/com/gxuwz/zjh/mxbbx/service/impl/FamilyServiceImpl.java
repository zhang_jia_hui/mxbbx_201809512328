package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Family;
import com.gxuwz.zjh.mxbbx.mapper.FamilyMapper;
import com.gxuwz.zjh.mxbbx.service.IFamilyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 家庭档案信息表 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class FamilyServiceImpl extends ServiceImpl<FamilyMapper, Family> implements IFamilyService {

    @Autowired
    private FamilyMapper familyMapper;

    @Override
    public Family findFamilyById(Family family) {
        return familyMapper.findFamilyById(family);
    }

    @Override
    public List<Family> findFamilyAll() {
        return familyMapper.findFamilyAll();
    }

    @Override
    public List<Family> findFamilyByRegion(Family family) {
        return familyMapper.findFamilyByRegion(family);
    }

    @Override
    public IPage<Family> selectFamilyPage(Page<Family> page, Wrapper<Family> wrapper) {
        return familyMapper.selectFamilyPage(page, wrapper);
    }

    @Override
    public void addFamily(Family family) {
        familyMapper.addFamily(family);
    }

    @Override
    public void updateFamilyById(Family family) {
        familyMapper.updateFamilyById(family);
    }

    @Override
    public void deleteFamilyById(Family family) {
        familyMapper.deleteFamilyById(family);
    }
}
