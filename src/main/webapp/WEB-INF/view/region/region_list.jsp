<%@ page import="com.baomidou.mybatisplus.extension.plugins.pagination.Page" %>
<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,target-densitydpi=low-dpi" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<script type="text/javascript">
  // 删除
  function tabledelete(id){
    if(confirm("是否删除 regionId=" + id + "的信息！")){
      window.location.href ="<%=basePath %>/mxbbx/region/deleteRegionById?regionId=" + id;
    }
  }
</script>
<body>
<form method="post" action="<%=basePath %>mxbbx/region/findRegionAll" >
    <div class="panel admin-panel">
        <div class="panel-head">
            <strong class="icon-reorder"> 行政区域信息列表</strong>
            <a href="" style="float:right; display:none;">添加字段</a>
        </div>
        <div class="padding border-bottom">
            <ul class="search" style="padding-left:10px;">
                <li>搜索：</li>
                <li><input type="text" placeholder="请输入待搜索行政区域ID" id="keywords" class="input"
                        <c:if test="${regionId != null}" >
                            value="${regionId}"
                        </c:if>
                           name="keywords" style="width:250px; line-height:17px; display:inline-block" />
                    <a class="button border-main icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query()"> 搜索</a>
                </li>
                <li><a class="button border-main icon-plus-square-o" name="insertUser"
                       href="<%=basePath %>/mxbbx/region/insertRegion"> 添加信息</a>
                </li>
            </ul>
        </div>
        <table class="table table-hover text-center" >
            <tr>
                <th style="width:25%">区域编码</th>
                <th style="width:25%">区域名称</th>
                <th style="width:25%">区域级别</th>
                <th style="width:25%" class="buttonsTwo">操作</th>
            </tr>
            <tr>
                <c:forEach items="${regionList}" var="map">
                    <tr>
                        <td>${map.regionId}</td>
                        <td>${map.regionName}</td>
                        <td>${map.levels}</td>
                        <td class="buttonsTwo"><div class="button-group">
                            <a class="button border-main" href="<%=basePath %>/mxbbx/region/updateRegion?regionId=${map.regionId}">
                                <span class="icon-edit"></span> 修改</a>
<%--                            <a class="button border-red" href="<%=basePath %>/mxbbx/region/deleteRegionById?regionId=${map.regionId}">--%>
<%--                                <span class="icon-trash-o"></span> 删除</a>--%>
                            <a class="button border-red tabledelete" onclick="tabledelete(${map.regionId})" value="${map.regionId}">
                                <span class="icon-trash-o"></span> 删除</a>
                        </div></td>
                    </tr>
                </c:forEach>
            </tr>
            <tr>
                <td colspan="15">
                    <div class="pagelist">
                        <a href="<%=basePath %>/mxbbx/region/findRegionAll?pageNmuber=1&regionId=${regionId}">首页</a>
                        <c:choose>
                            <c:when test="${page.current > 1}">
                                <a href="<%=basePath %>/mxbbx/region/findRegionAll?pageNumber=${page.current-1}&regionId=${regionId}">上一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是第一页');">上一页</a>
                            </c:otherwise>
                        </c:choose>

                        <c:if test="${page.current > 3}">
                            . . .
                        </c:if>
                        <c:forEach items="${pagesList}" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current == status.index + 1}">
                                    <span class="current">${status.index+1}</span>
                                </c:when>
                                <c:when test="${page.current != status.index + 1}">
                                    <c:if test="${page.current + 2 >= status.index + 1 && page.current - 2 <= status.index + 1}">
                                        <a href="<%=basePath %>/mxbbx/region/findRegionAll?pageNumber=${status.index+1}&regionId=${regionId}">${status.index+1}</a>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${(pages - page.current) > 2}">
                            . . .
                        </c:if>

                        <c:choose>
                            <c:when test="${page.current < pages}">
                                <a href="<%=basePath %>/mxbbx/region/findRegionAll?pageNumber=${page.current+1}&regionId=${regionId}">下一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是最后一页')">下一页</a>
                            </c:otherwise>
                        </c:choose>
                        <a href="<%=basePath %>/mxbbx/region/findRegionAll?pageNumber=${pages}&regionId=${regionId}">尾页</a>
                        <div style="font-size:15px;float:right;margin-right:100px">总页数: <span style="color: red;">${pages}</span> 页</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>
<div style="margin: 10px;font-size:15px;">查询数据条数: <span style="color: red;">${page.total}</span> 条</div>
<script type="text/javascript">
  // 模糊查询
  function query() {
    window.location.href = "<%=basePath %>/mxbbx/region/findRegionAll?regionId=" + $("#keywords").val();
  }
</script>
</body>
<script type="text/javascript">
  window.onload=function(){
    var win = false;
    if("addTrue" == "${result1}"){
      win = true;
      alert('√ -添加成功！');
    }else if("addFalse" == "${result1}"){
      win = true;
      alert('x -添加失败,已存在该编号信息!！');
    }
    if("editTrue" == "${result2}"){
      win = true;
      alert('√ -修改成功！');
    }
    if("deleteTrue" == "${result3}"){
      win = true;
      alert('√ -删除成功！');
    }
    if("jurisdiction" == "${result4}"){
      win = true;
      alert('x -权限不够！');
    }
    if(0 == "${pages}"){
      win = true;
      alert('x -数据数量为零！！');
    }
    if(win == true){
      window.location.href = "<%=basePath %>/mxbbx/region/findRegionAll";
    }
  };
</script>
</html>
