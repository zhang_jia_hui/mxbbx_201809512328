package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Family;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Health;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 家庭档案信息表 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface FamilyMapper extends BaseMapper<Family> {

    // 根据 id 查询对应用户信息
    Family findFamilyById(Family family);

    // 查询全部用户信息
    List<Family> findFamilyAll();

    // 根据 地域 查询对应用户信息
    List<Family> findFamilyByRegion(Family family);

    //自定义sql 分页
    IPage<Family> selectFamilyPage(Page<Family> page, @Param(Constants.WRAPPER) Wrapper<Family> wrapper);

    // 添加用户信息
    void addFamily(Family family);

    // 修改用户信息
    void updateFamilyById(Family family);

    // 删除用户信息
    void deleteFamilyById(Family family);

}
