<%@ page import="com.baomidou.mybatisplus.extension.plugins.pagination.Page" %>
<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,target-densitydpi=low-dpi" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<script type="text/javascript">
  // 删除
  function tabledelete(id){
    if(confirm("是否删除 paysId=" + id + "的信息！")){
      window.location.href ="<%=basePath %>/mxbbx/pays/deletePaysById?paysId=" + id;
    }
  }
</script>
<body>
<form method="post" action="<%=basePath %>mxbbx/pays/findPaysAll" >
    <div class="panel admin-panel">
        <div class="panel-head">
            <strong class="icon-reorder"> 参合缴费登记记录</strong>
            <a href="" style="float:right; display:none;">添加字段</a>
        </div>
        <div class="padding border-bottom">
            <ul class="search" style="padding-left:10px;">
                <li>搜索：</li>
                <li><input type="text" placeholder="请输入待搜索ID" id="keywords" class="input"
                        <c:if test="${paysId != null}" >
                            value="${paysId}"
                        </c:if>
                           name="keywords" style="width:250px; line-height:17px; display:inline-block" />
                    <a class="button border-green icon-search" href="javascript:;" name="b-iframe utton-s"
                       onclick="query()"> 搜索</a>
                </li>
<%--                <li><a class="button border-green icon-plus-square-o" name="insertPays"--%>
<%--                       href="<%=basePath %>/mxbbx/pays/insertPays"> 添加信息</a>--%>
<%--                </li>--%>
            </ul>
        </div>
        <table class="table table-hover text-center" >
            <tr>
                <th style="width:10%">缴费记录编号</th>
                <th style="width:10%">家庭编号</th>
                <th style="width:10%">当次缴费人</th>
                <th style="width:10%">家庭已缴费人员</th>
                <th style="width:15%">家庭未缴费人员</th>
                <th style="width:7%">未缴费人数</th>
                <th style="width:8%">当次缴费</th>
                <th style="width:7%">缴费总金额</th>
                <th style="width:8%">缴费年份</th>
                <th style="width:15%">缴费时间</th>
<%--                <th style="width:13%" class="buttonsTwo">操作</th>--%>
            </tr>
            <tr>
                <c:forEach items="${paysList}" var="map">
                    <tr>
                        <td>${map.paysId}</td>
                        <td>${map.familyId}</td>
                        <td>${map.nowName}</td>
                        <td>${map.familyName}</td>
                        <td>${map.familyNoName}</td>
                        <td>${map.familyNum}</td>
                        <td>${map.money}</td>
                        <td>${map.moneys}</td>
                        <td>${map.year}</td>
                        <td>${map.time}</td>
<%--                        <td class="buttonsTwo"><div class="button-group">--%>
<%--                            <a class="button border-main" href="<%=basePath %>/mxbbx/pays/updatePays?paysId=${map.paysId}">--%>
<%--                                <span class="icon-edit"></span> 修改</a>--%>
<%--                            <a class="button border-red tabledelete" onclick="tabledelete(${map.paysId})" value="${map.paysId}">--%>
<%--                                <span class="icon-trash-o"></span> 删除</a>--%>
<%--                        </div></td>--%>
                    </tr>
                </c:forEach>
            </tr>
            <tr>
                <td colspan="11">
                    <div class="pagelist">
                        <a href="<%=basePath %>/mxbbx/pays/findPaysAll?pageNumber=1&paysId=${paysId}">首页</a>
                        <c:choose>
                            <c:when test="${page.current > 1}">
                                <a href="<%=basePath %>/mxbbx/pays/findPaysAll?pageNumber=${page.current-1}&paysId=${paysId}">上一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是第一页');">上一页</a>
                            </c:otherwise>
                        </c:choose>

                        <c:if test="${page.current > 3}">
                            . . .
                        </c:if>
                        <c:forEach items="${pagesList}" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current == status.index + 1}">
                                    <span class="current">${status.index+1}</span>
                                </c:when>
                                <c:when test="${page.current != status.index + 1}">
                                    <c:if test="${page.current + 2 >= status.index + 1 && page.current - 2 <= status.index + 1}">
                                        <a href="<%=basePath %>/mxbbx/pays/findPaysAll?pageNumber=${status.index+1}&paysId=${paysId}">${status.index+1}</a>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${(pages - page.current) > 2}">
                            . . .
                        </c:if>

                        <c:choose>
                            <c:when test="${page.current < pages}">
                                <a href="<%=basePath %>/mxbbx/pays/findPaysAll?pageNumber=${page.current+1}&paysId=${paysId}">下一页</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('已经是最后一页')">下一页</a>
                            </c:otherwise>
                        </c:choose>
                        <a href="<%=basePath %>/mxbbx/pays/findPaysAll?pageNumber=${pages}&paysId=${paysId}">尾页</a>
                        <div style="font-size:15px;float:right;margin-right:100px">总页数: <span style="color: red;">${pages}</span> 页</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>
<div style="margin: 10px;font-size:15px;">查询数据条数: <span style="color: red;">${page.total}</span> 条</div>
<script type="text/javascript">
  // 模糊查询
  function query() {
    window.location.href = "<%=basePath %>/mxbbx/pays/findPaysAll?paysId=" + $("#keywords").val();
  }
</script>
</body>
<script type="text/javascript">
  window.onload=function(){
    var win = false;
    if("addTrue" == "${result1}"){
      win = true;
      alert('√ -添加成功！');
    }else if("addFalse" == "${result1}"){
      win = true;
      alert('x -添加失败,已存在该编号信息!！');
    }
    if("editTrue" == "${result2}"){
      win = true;
      alert('√ -缴费/修改 成功！');
    }
    if("deleteTrue" == "${result3}"){
      win = true;
      alert('√ -删除成功！');
    }
    if("jurisdiction" == "${result4}"){
      win = true;
      alert('x -权限不够！');
    }
    if(0 == "${pages}"){
      win = true;
      alert('x -数据数量为零！！');
    }
    if("none" == "${result_pay}"){
      alert('x -本年度缴费方案不存在，请添加！！');
      window.location.href = "<%=basePath %>/mxbbx/pay/findPayAll";
    }
    if("none" == "${no_person_pay}"){
      alert('! -该家庭本年度已全部缴费！！');
      window.location.href = "<%=basePath %>/mxbbx/family/findFamilyAll";
    }
    if(win == true){
      window.location.href = "<%=basePath %>/mxbbx/pays/findPaysAll";
    }
  };
</script>
</html>
