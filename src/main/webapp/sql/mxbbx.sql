/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50645
 Source Host           : localhost:3306
 Source Schema         : mxbbx

 Target Server Type    : MySQL
 Target Server Version : 50645
 File Encoding         : 65001

 Date: 27/05/2021 11:56:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for agency
-- ----------------------------
DROP TABLE IF EXISTS `agency`;
CREATE TABLE `agency`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '农合机构编码',
  `region_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属行政区域编码',
  `adm_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经办机构编码',
  `mechanism_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构名称',
  `levels` int(11) NULL DEFAULT NULL COMMENT '级别',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '农合经办机构信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of agency
-- ----------------------------
INSERT INTO `agency` VALUES (1, 'A450421', '450421', '667022793', '苍梧县新农合管理中心', 1, '2021-03-20 21:32:52', '2021-03-20 21:32:52');
INSERT INTO `agency` VALUES (2, 'A45042101', '45042101', '667022793', '龙圩镇合管办', 2, '2021-03-20 21:33:20', '2021-03-20 21:33:20');

-- ----------------------------
-- Table structure for chronic
-- ----------------------------
DROP TABLE IF EXISTS `chronic`;
CREATE TABLE `chronic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chronic_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '疾病编码',
  `name_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拼音码',
  `chronic_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '疾病名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '慢性疾病信息登记表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chronic
-- ----------------------------
INSERT INTO `chronic` VALUES (1, 'F29 01', 'jsb', '精神病', '2021-03-21 10:25:19', '2021-03-21 10:25:19');
INSERT INTO `chronic` VALUES (2, 'K74.151', 'gyh', '肝硬化', '2021-03-21 10:25:28', '2021-03-21 10:25:28');
INSERT INTO `chronic` VALUES (3, 'I10 05', 'gxys', '高血压Ⅲ', '2021-03-21 10:25:38', '2021-03-21 10:25:38');
INSERT INTO `chronic` VALUES (4, 'G20 02', 'pjssb', '帕金森氏病', '2021-03-21 10:25:50', '2021-03-21 10:25:50');
INSERT INTO `chronic` VALUES (5, 'M32.901', 'xtxhblc', '系统性红斑狼疮 NOS', '2021-03-21 10:26:00', '2021-03-21 10:26:00');
INSERT INTO `chronic` VALUES (6, 'N04.903', 'sbzhz', '肾病综合征', '2021-03-21 10:26:11', '2021-03-21 10:26:11');
INSERT INTO `chronic` VALUES (7, 'M06.991', 'lfsxgjy', '类风湿性关节炎 NOS', '2021-03-21 10:26:27', '2021-03-21 10:26:27');
INSERT INTO `chronic` VALUES (8, 'C00-C97>', 'exzl', '恶性肿瘤', '2021-03-21 10:26:38', '2021-03-21 10:26:38');
INSERT INTO `chronic` VALUES (9, 'A15.001', 'fjhxwjjzs', '肺结核，显微镜检证实', '2021-03-21 10:26:48', '2021-03-21 10:26:48');
INSERT INTO `chronic` VALUES (10, 'I10 04', 'gxye', '高血压Ⅱ', '2021-03-21 10:26:56', '2021-03-21 10:26:56');
INSERT INTO `chronic` VALUES (11, 'I09.901', 'fsxxzb', '风湿性心脏病(RHD)', '2021-03-21 10:27:04', '2021-03-21 10:27:04');
INSERT INTO `chronic` VALUES (12, 'I25.101', 'gxb', '冠心病', '2021-03-21 10:27:16', '2021-03-21 10:27:16');
INSERT INTO `chronic` VALUES (13, 'N03.903', 'mxsy', '慢性肾炎', '2021-03-21 10:27:24', '2021-03-21 10:27:24');
INSERT INTO `chronic` VALUES (14, 'E14.901', 'tnb', '糖尿病 NOS', '2021-03-21 10:27:33', '2021-03-21 10:27:33');
INSERT INTO `chronic` VALUES (15, 'E05.901', 'jzxjnkj', '甲状腺机能亢进', '2021-03-21 10:27:42', '2021-03-21 10:27:42');
INSERT INTO `chronic` VALUES (16, 'D61.905', 'zszaxpx', '再生障碍性贫血 NOS', '2021-03-21 10:27:51', '2021-03-21 10:27:51');
INSERT INTO `chronic` VALUES (17, 'D56.001', 'xdzhpx', 'α型地中海贫血', '2021-03-21 10:28:09', '2021-03-21 10:28:09');
INSERT INTO `chronic` VALUES (18, 'D66 02', 'xyb', '血友病', '2021-03-21 10:28:20', '2021-03-21 10:28:20');
INSERT INTO `chronic` VALUES (19, 'zmqsb', 'zmqsb', '终末期肾病（尿毒症）', '2021-03-21 10:28:27', '2021-03-21 10:28:27');

-- ----------------------------
-- Table structure for chronic_card
-- ----------------------------
DROP TABLE IF EXISTS `chronic_card`;
CREATE TABLE `chronic_card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '慢性病证ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭住址',
  `chronic_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '慢性病名称',
  `chronic_start_time` date NULL DEFAULT NULL COMMENT '慢性病起始时间',
  `chronic_end_time` date NULL DEFAULT NULL COMMENT '慢性病结束时间',
  `prove` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院证明',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '慢性病证' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chronic_card
-- ----------------------------
INSERT INTO `chronic_card` VALUES (1, '450421010101004801', '钟海清', '广西省苍梧县龙圩镇恩义村多一组', 'E14.901', '2018-11-23', '2021-12-19', NULL, '2021-05-02 11:22:11', '2021-05-23 23:21:17');
INSERT INTO `chronic_card` VALUES (7, '450421010101234501', '测试人员', '湖北省天门市', 'E14.901', '2018-11-23', '2021-12-19', NULL, '2021-05-22 11:35:18', '2021-05-23 23:21:12');
INSERT INTO `chronic_card` VALUES (10, '450421010101005101', '张三', '湖北省天门市', 'G20 02', '2021-05-23', '2020-10-23', NULL, '2021-05-23 21:47:04', '2021-05-23 23:16:35');
INSERT INTO `chronic_card` VALUES (13, '450421010101234502', '傅凌湘', '湖北省天门市', 'M32.901', '2021-05-03', '2021-05-29', NULL, '2021-05-26 20:28:23', '2021-05-26 20:28:30');
INSERT INTO `chronic_card` VALUES (14, '123213', '123123', '湖北省天门市', 'M06.991', '2021-05-10', '2021-05-29', NULL, '2021-05-27 11:52:54', '2021-05-27 11:53:06');

-- ----------------------------
-- Table structure for chronic_reim
-- ----------------------------
DROP TABLE IF EXISTS `chronic_reim`;
CREATE TABLE `chronic_reim`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院发票号',
  `want_pay` decimal(32, 8) NULL DEFAULT NULL COMMENT '医院花费',
  `now_pay` decimal(32, 8) NULL DEFAULT NULL COMMENT '实际报销金额',
  `pay_all` double(10, 0) NULL DEFAULT NULL COMMENT '报销总金额',
  `number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参合农民身份证号',
  `chronic_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '慢性病编号',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '领款时间',
  `person` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `start` int(1) NULL DEFAULT NULL COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '慢性病报销信息表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chronic_reim
-- ----------------------------
INSERT INTO `chronic_reim` VALUES (1, '21312321421412', 2000.00000000, 2000.00000000, 2000, '450421195108247000', 'E14.901', '2021-05-21 10:23:19', '张佳辉', 0, '2021-05-21 10:23:25', '2021-05-26 20:02:22');
INSERT INTO `chronic_reim` VALUES (5, '2000052125942021', 500.00000000, 200.00000000, 200, '429006200005212594', 'E14.901', '2021-05-23 22:05:17', '张佳辉', 1, '2021-05-23 22:05:19', '2021-05-26 20:02:23');
INSERT INTO `chronic_reim` VALUES (8, '2003121312342021', 1000.00000000, 400.00000000, 400, '429006200312131234', 'M32.901', '2021-05-26 20:50:35', '张佳辉', 0, '2021-05-26 20:50:37', '2021-05-26 20:50:37');
INSERT INTO `chronic_reim` VALUES (9, '1231231231232021', 500.00000000, 200.00000000, 200, '123123123123123123', 'M06.991', '2021-05-27 11:53:31', '张佳辉', 0, '2021-05-27 11:53:32', '2021-05-27 11:53:32');

-- ----------------------------
-- Table structure for economic
-- ----------------------------
DROP TABLE IF EXISTS `economic`;
CREATE TABLE `economic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `economic_id` int(11) NULL DEFAULT NULL COMMENT '机构所属经济类型编号',
  `economic_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构所属经济类型名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '(S201_01)  机构所属经济类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of economic
-- ----------------------------
INSERT INTO `economic` VALUES (1, 10, '内资', '2021-03-20 22:09:31', '2021-03-20 22:09:31');
INSERT INTO `economic` VALUES (2, 11, '国有全资', '2021-03-20 22:09:35', '2021-03-20 22:09:35');
INSERT INTO `economic` VALUES (3, 12, '集体全资', '2021-03-20 22:09:39', '2021-03-20 22:09:39');
INSERT INTO `economic` VALUES (4, 13, '股份合作', '2021-03-20 22:09:43', '2021-03-20 22:09:43');
INSERT INTO `economic` VALUES (5, 14, '联营', '2021-03-20 22:09:49', '2021-03-20 22:09:49');
INSERT INTO `economic` VALUES (6, 15, '有限责任公司', '2021-03-20 22:09:54', '2021-03-20 22:09:54');
INSERT INTO `economic` VALUES (7, 16, '股份有限公司', '2021-03-20 22:09:58', '2021-03-20 22:09:58');
INSERT INTO `economic` VALUES (8, 17, '私有', '2021-03-20 22:09:59', '2021-03-20 22:09:59');
INSERT INTO `economic` VALUES (9, 19, '其他内资', '2021-03-20 22:10:10', '2021-03-20 22:10:10');
INSERT INTO `economic` VALUES (10, 20, '港、澳、台投资', '2021-03-20 22:10:15', '2021-03-20 22:10:15');
INSERT INTO `economic` VALUES (11, 21, '内地和港、澳、台合资', '2021-03-20 22:10:21', '2021-03-20 22:10:21');
INSERT INTO `economic` VALUES (12, 22, '内地和港、澳、台合作', '2021-03-20 22:10:27', '2021-03-20 22:10:27');
INSERT INTO `economic` VALUES (13, 23, '港、澳、台独资', '2021-03-20 22:10:32', '2021-03-20 22:10:32');
INSERT INTO `economic` VALUES (14, 24, '港、澳、台投资股份有限公司', '2021-03-20 22:10:37', '2021-03-20 22:10:37');
INSERT INTO `economic` VALUES (15, 29, '其他港、澳、台投资', '2021-03-20 22:10:41', '2021-03-20 22:10:41');
INSERT INTO `economic` VALUES (16, 30, '国外投资', '2021-03-20 22:10:46', '2021-03-20 22:10:46');
INSERT INTO `economic` VALUES (17, 31, '中外合资', '2021-03-20 22:10:51', '2021-03-20 22:10:51');
INSERT INTO `economic` VALUES (18, 32, '中外合作', '2021-03-20 22:10:54', '2021-03-20 22:10:54');
INSERT INTO `economic` VALUES (19, 33, '外资', '2021-03-20 22:11:03', '2021-03-20 22:11:03');
INSERT INTO `economic` VALUES (20, 34, '国外投资股份有限公司', '2021-03-20 22:11:12', '2021-03-20 22:11:12');
INSERT INTO `economic` VALUES (21, 39, '其他国外投资', '2021-03-20 22:11:16', '2021-03-20 22:11:16');
INSERT INTO `economic` VALUES (22, 90, '其他', '2021-03-20 22:11:21', '2021-03-20 22:11:21');
INSERT INTO `economic` VALUES (24, 1000001, '权限试用', '2021-04-18 22:38:48', '2021-04-18 22:38:48');

-- ----------------------------
-- Table structure for families
-- ----------------------------
DROP TABLE IF EXISTS `families`;
CREATE TABLE `families`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `families_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭成员编号',
  `family_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭编号',
  `nonghe_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '农合证号',
  `medical_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医疗证卡号',
  `indoor_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户内编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `holder_rela` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '与户主关系',
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参合人员档案信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of families
-- ----------------------------
INSERT INTO `families` VALUES (1, '450421010101004801070', '4504210101010048', '450421010101004801', '4504210067270', '01', '钟海清', '户主', '450421195108247000', '2021-03-21 11:44:20', '2021-03-21 11:44:20');
INSERT INTO `families` VALUES (2, '450421010101004802073', '4504210101010048', '450421010101004802', '4504210067273', '02', '蔡云妮', '配偶', '450421199807121000', '2021-03-21 11:51:23', '2021-03-21 11:51:23');
INSERT INTO `families` VALUES (3, '450421010101004803076', '4504210101010048', '450421010101004803', '4504210067276', '03', '钟艳玲', '女', '450421199703121000', '2021-03-21 11:51:54', '2021-03-21 11:51:54');
INSERT INTO `families` VALUES (4, '450421010101004804080', '4504210101010048', '450421010101004804', '4504210067280', '04', '钟艳婷', '女', '450421199910261000', '2021-03-21 11:52:27', '2021-03-21 11:52:27');
INSERT INTO `families` VALUES (5, '450421010101004805090', '4504210101010048', '450421010101004805', '4504210067290', '05', '钟子峰', '子', '45042119691120152X', '2021-03-21 11:52:28', '2021-03-21 11:52:28');
INSERT INTO `families` VALUES (6, '450421010101234560011', '4504210101012345', '450421010101234501', '4504210062357', '01', '测试人员', '户主', '429006200007135721', '2021-05-16 22:29:33', '2021-05-22 11:34:44');
INSERT INTO `families` VALUES (7, '450421010101234560042', '4504210101012345', '450421010101234502', '4504210062393', '02', '傅凌湘', '妻子', '429006200312131234', '2021-05-16 23:26:00', '2021-05-22 11:34:47');
INSERT INTO `families` VALUES (8, '450421010101005101021', '4504210101010051', '450421010101005101', '450421010101', '01', '张三', '户主', '429006200005212594', '2021-05-23 15:27:53', '2021-05-23 15:27:53');
INSERT INTO `families` VALUES (9, '45042101010101231071', '4504210101010123', '45042101010101231', '4504210101010', '01', '王五', '户主', '429006200007248954', '2021-05-27 11:34:34', '2021-05-27 11:34:34');
INSERT INTO `families` VALUES (10, '123123123', '123123', '123213', '123213123123', '01', '123123', '户主', '123123123123123123', '2021-05-27 11:51:13', '2021-05-27 11:52:40');

-- ----------------------------
-- Table structure for family
-- ----------------------------
DROP TABLE IF EXISTS `family`;
CREATE TABLE `family`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `family_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭编号',
  `level_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '县级编码',
  `ship_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乡镇编码',
  `village_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '村编码',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组编号',
  `person_number` int(11) NULL DEFAULT NULL COMMENT '家庭人口数',
  `agr_number` int(11) NULL DEFAULT NULL COMMENT '农业人口数',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭住址',
  `establish_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建档案时间',
  `registrant` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登记员',
  `holder` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户主',
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户主身份证号',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '家庭档案信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of family
-- ----------------------------
INSERT INTO `family` VALUES (1, '4504210101010048', '450421', '45042101', '4504210101', '430201010109', 5, 3, '广西省梧州市苍梧县龙圩镇', '2021-05-12 13:29:22', '张佳辉', '钟海清', '450421195108247000', '2021-03-21 10:35:47', '2021-05-12 13:32:03');
INSERT INTO `family` VALUES (5, '4504210101012345', '430201', '43020101', '4302010102', '430201010201', 1, 1, '湖北省天门市皂市镇', '2021-05-16 22:28:44', '张佳辉', '测试人员', '429006200007135721', '2021-05-16 22:29:33', '2021-05-22 11:34:53');
INSERT INTO `family` VALUES (6, '4504210101010051', '430201', '43020101', '4302010101', '430201010103', 2, 1, '广西省梧州市苍梧县龙圩镇', '2021-05-23 15:26:46', '张佳辉', '张三', '429006200005212594', '2021-05-23 15:27:53', '2021-05-23 15:27:53');
INSERT INTO `family` VALUES (8, '123123', '430201', '43020101', '4302010101', '430201010105', 2, 1, '广西省梧州市苍梧县龙圩镇', '2021-05-27 11:51:00', '张佳辉', '123123', '123123123123123123', '2021-05-27 11:51:13', '2021-05-27 11:51:32');

-- ----------------------------
-- Table structure for health
-- ----------------------------
DROP TABLE IF EXISTS `health`;
CREATE TABLE `health`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `health_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卫生机构（组织）类别编码',
  `health_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卫生机构（组织）类别名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '(S201_03)  卫生机构（组织）类别' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of health
-- ----------------------------
INSERT INTO `health` VALUES (1, 'A', '医院', '2021-03-20 22:13:10', '2021-03-20 22:13:10');
INSERT INTO `health` VALUES (2, 'A100', '综合医院', '2021-03-20 22:13:17', '2021-03-20 22:13:17');
INSERT INTO `health` VALUES (3, 'A210', '中医医院', '2021-03-20 22:13:24', '2021-03-20 22:13:24');
INSERT INTO `health` VALUES (4, 'A300', '中西医结合医院', '2021-03-20 22:13:31', '2021-03-20 22:13:31');
INSERT INTO `health` VALUES (5, 'A400', '民族医院', '2021-03-20 22:13:36', '2021-03-20 22:13:36');
INSERT INTO `health` VALUES (6, 'A419', '其他民族医院', '2021-03-20 22:13:43', '2021-03-20 22:13:43');
INSERT INTO `health` VALUES (7, 'A5', '专科医院', '2021-03-20 22:13:50', '2021-03-20 22:13:50');
INSERT INTO `health` VALUES (8, 'A511', '口腔医院(包括牙科医院)', '2021-03-20 22:13:57', '2021-03-20 22:13:57');
INSERT INTO `health` VALUES (9, 'A514', '肿瘤医院', '2021-03-20 22:14:02', '2021-03-20 22:14:02');
INSERT INTO `health` VALUES (10, 'A518', '妇产(科)医院包括妇婴(儿)医院', '2021-03-20 22:14:08', '2021-03-20 22:14:08');
INSERT INTO `health` VALUES (11, 'A519', '儿童医院', '2021-03-20 22:14:14', '2021-03-20 22:14:14');
INSERT INTO `health` VALUES (12, 'A520', '精神病医院包括 20 张床位以上的精神卫生中心', '2021-03-20 22:14:20', '2021-03-20 22:14:20');
INSERT INTO `health` VALUES (13, 'A521', '传染病医院', '2021-03-20 22:14:43', '2021-03-20 22:14:43');
INSERT INTO `health` VALUES (14, 'A522', '皮肤病医院包括性病医院', '2021-03-20 22:14:48', '2021-03-20 22:14:48');
INSERT INTO `health` VALUES (15, 'A523', '结核病医院', '2021-03-20 22:15:00', '2021-03-20 22:15:00');
INSERT INTO `health` VALUES (16, 'A527', '康复医院', '2021-03-20 22:15:05', '2021-03-20 22:15:05');
INSERT INTO `health` VALUES (17, 'A600', '疗养院', '2021-03-20 22:15:10', '2021-03-20 22:15:10');
INSERT INTO `health` VALUES (18, 'A7', '护理院', '2021-03-20 22:15:18', '2021-03-20 22:15:18');
INSERT INTO `health` VALUES (19, 'B', '社区卫生服务中心（站）', '2021-03-20 22:15:26', '2021-03-20 22:15:26');
INSERT INTO `health` VALUES (20, 'B100', '社区卫生服务中心', '2021-03-20 22:15:31', '2021-03-20 22:15:31');
INSERT INTO `health` VALUES (21, 'B200', '社区卫生服务站', '2021-03-20 22:15:37', '2021-03-20 22:15:37');
INSERT INTO `health` VALUES (22, 'B200', '社区卫生服务站', '2021-03-20 22:15:37', '2021-03-20 22:15:37');
INSERT INTO `health` VALUES (23, 'C', '卫生院', '2021-03-20 22:15:45', '2021-03-20 22:15:45');
INSERT INTO `health` VALUES (24, 'C100', '街道卫生院', '2021-03-20 22:15:50', '2021-03-20 22:15:50');
INSERT INTO `health` VALUES (25, 'C220', '乡镇卫生院', '2021-03-20 22:15:57', '2021-03-20 22:15:57');
INSERT INTO `health` VALUES (26, 'C210', '乡镇中心卫生院', '2021-03-20 22:16:03', '2021-03-20 22:16:03');
INSERT INTO `health` VALUES (27, 'D', '门诊部、诊所、医务室、村卫生室', '2021-03-20 22:16:05', '2021-03-20 22:16:05');
INSERT INTO `health` VALUES (28, 'D100', '门诊部', '2021-03-20 22:16:17', '2021-03-20 22:16:17');
INSERT INTO `health` VALUES (29, 'D120', '中医门诊部', '2021-03-20 22:16:24', '2021-03-20 22:16:24');
INSERT INTO `health` VALUES (30, 'D121', '中医(综合)门诊部', '2021-03-20 22:16:30', '2021-03-20 22:16:30');
INSERT INTO `health` VALUES (31, 'D122', '中医专科门诊部', '2021-03-20 22:16:39', '2021-03-20 22:16:39');
INSERT INTO `health` VALUES (32, 'D130', '中西医结合门诊部', '2021-03-20 22:16:45', '2021-03-20 22:16:45');
INSERT INTO `health` VALUES (33, 'D140', '民族医门诊部', '2021-03-20 22:16:52', '2021-03-20 22:16:52');
INSERT INTO `health` VALUES (34, 'D211', '普通诊所', '2021-03-20 22:17:01', '2021-03-20 22:17:01');
INSERT INTO `health` VALUES (35, 'D212', '中医诊所', '2021-03-20 22:17:06', '2021-03-20 22:17:06');
INSERT INTO `health` VALUES (36, 'D213', '中西医结合诊所', '2021-03-20 22:19:38', '2021-03-20 22:19:38');
INSERT INTO `health` VALUES (37, 'D214', '民族医诊所', '2021-03-20 22:19:48', '2021-03-20 22:19:48');
INSERT INTO `health` VALUES (38, 'D215', '口腔诊所', '2021-03-20 22:19:53', '2021-03-20 22:19:53');
INSERT INTO `health` VALUES (39, 'D300', '卫生所（室）', '2021-03-20 22:19:55', '2021-03-20 22:19:55');
INSERT INTO `health` VALUES (40, 'D400', '医务室', '2021-03-20 22:19:58', '2021-03-20 22:19:58');
INSERT INTO `health` VALUES (41, 'D500', '中小学卫生保健所', '2021-03-20 22:19:58', '2021-03-20 22:19:58');
INSERT INTO `health` VALUES (42, 'D600', '村卫生室', '2021-03-20 22:19:58', '2021-03-20 22:19:58');
INSERT INTO `health` VALUES (43, 'E', '急救中心（站）', '2021-03-20 22:19:59', '2021-03-20 22:19:59');
INSERT INTO `health` VALUES (44, 'E100', '急救中心', '2021-03-20 22:19:59', '2021-03-20 22:19:59');
INSERT INTO `health` VALUES (45, 'E200', '急救中心站', '2021-03-20 22:19:59', '2021-03-20 22:19:59');
INSERT INTO `health` VALUES (46, 'E300', '急救站', '2021-03-20 22:19:59', '2021-03-20 22:19:59');
INSERT INTO `health` VALUES (47, 'F', '采供血机构', '2021-03-20 22:20:00', '2021-03-20 22:20:00');
INSERT INTO `health` VALUES (48, 'F1', '血站', '2021-03-20 22:20:36', '2021-03-20 22:20:36');
INSERT INTO `health` VALUES (49, 'F2', ' 单采血浆站', '2021-03-20 22:20:36', '2021-03-20 22:20:36');
INSERT INTO `health` VALUES (50, 'G', '妇幼保健院（所、站）', '2021-03-20 22:20:37', '2021-03-20 22:20:37');
INSERT INTO `health` VALUES (51, 'G100', '妇幼保健院', '2021-03-20 22:20:37', '2021-03-20 22:20:37');
INSERT INTO `health` VALUES (52, 'G200', '妇幼保健所', '2021-03-20 22:20:37', '2021-03-20 22:20:37');
INSERT INTO `health` VALUES (53, 'G300', '妇幼保健站', '2021-03-20 22:20:37', '2021-03-20 22:20:37');
INSERT INTO `health` VALUES (54, 'G400', '生殖保健中心', '2021-03-20 22:20:37', '2021-03-20 22:20:37');
INSERT INTO `health` VALUES (55, 'H', '专科疾病防治院（所、站）', '2021-03-20 22:20:38', '2021-03-20 22:20:38');
INSERT INTO `health` VALUES (56, 'H111', ' 传染病防治院', '2021-03-20 22:20:38', '2021-03-20 22:20:38');
INSERT INTO `health` VALUES (57, 'H211', '结核病防治院', '2021-03-20 22:21:59', '2021-03-20 22:21:59');
INSERT INTO `health` VALUES (58, 'H100', '专科疾病防治院', '2021-03-20 22:21:59', '2021-03-20 22:21:59');
INSERT INTO `health` VALUES (59, 'H200', '专科疾病防治所（站、中心）', '2021-03-20 22:21:59', '2021-03-20 22:21:59');
INSERT INTO `health` VALUES (60, 'J', '疾病预防控制中心', '2021-03-20 22:22:00', '2021-03-20 22:22:00');
INSERT INTO `health` VALUES (61, 'J100', ' 疾病预防控制中心', '2021-03-20 22:22:00', '2021-03-20 22:22:00');
INSERT INTO `health` VALUES (62, 'J200', '卫生防疫站', '2021-03-20 22:22:00', '2021-03-20 22:22:00');
INSERT INTO `health` VALUES (63, 'J300', '卫生防病中心', '2021-03-20 22:22:00', '2021-03-20 22:22:00');
INSERT INTO `health` VALUES (64, 'J400', '预防保健中心', '2021-03-20 22:22:00', '2021-03-20 22:22:00');
INSERT INTO `health` VALUES (65, 'K', ' 卫生监督所', '2021-03-20 22:22:37', '2021-03-20 22:22:37');
INSERT INTO `health` VALUES (66, 'K100', ' 卫生监督所(局)', '2021-03-20 22:22:37', '2021-03-20 22:22:37');
INSERT INTO `health` VALUES (67, 'L', ' 卫生监督检验（监测、检测）所', '2021-03-20 22:22:38', '2021-03-20 22:22:38');
INSERT INTO `health` VALUES (68, 'L100', ' 卫生综合监督检验（监测、检测）所', '2021-03-20 22:22:38', '2021-03-20 22:22:38');
INSERT INTO `health` VALUES (69, 'L200', ' 环境卫生监督检验（监测、检测）所', '2021-03-20 22:22:38', '2021-03-20 22:22:38');
INSERT INTO `health` VALUES (70, 'L300', ' 放射卫生监督检验（监测、检测）所', '2021-03-20 22:23:50', '2021-03-20 22:23:50');
INSERT INTO `health` VALUES (71, 'L400', ' 职业卫生监督检验（监测、检测）所', '2021-03-20 22:23:50', '2021-03-20 22:23:50');
INSERT INTO `health` VALUES (72, 'L500', ' 食品卫生监督检验（监测、检测）所', '2021-03-20 22:23:50', '2021-03-20 22:23:50');
INSERT INTO `health` VALUES (73, 'L600', ' 学校卫生监督检验（监测、检测）所', '2021-03-20 22:23:50', '2021-03-20 22:23:50');
INSERT INTO `health` VALUES (74, 'L900', ' 其他卫生监督检验（监测、检测）所', '2021-03-20 22:23:50', '2021-03-20 22:23:50');
INSERT INTO `health` VALUES (75, 'M', ' 医学科学研究机构', '2021-03-20 22:24:36', '2021-03-20 22:24:36');
INSERT INTO `health` VALUES (76, 'M100', ' 医学科学研究院（所）', '2021-03-20 22:24:36', '2021-03-20 22:24:36');
INSERT INTO `health` VALUES (77, 'M121', '卫生学校', '2021-03-20 22:24:36', '2021-03-20 22:24:36');
INSERT INTO `health` VALUES (78, 'M124', '护士学校', '2021-03-20 22:24:36', '2021-03-20 22:24:36');
INSERT INTO `health` VALUES (79, 'M200', ' 预防医学研究院（所）', '2021-03-20 22:24:58', '2021-03-20 22:24:58');
INSERT INTO `health` VALUES (80, 'M300', ' 中医药研究院（所）', '2021-03-20 22:24:58', '2021-03-20 22:24:58');
INSERT INTO `health` VALUES (81, 'M400', ' 中西医结合研究所', '2021-03-20 22:24:58', '2021-03-20 22:24:58');
INSERT INTO `health` VALUES (82, 'M500', ' 民族医药学研究所', '2021-03-20 22:24:59', '2021-03-20 22:24:59');
INSERT INTO `health` VALUES (83, 'M600', ' 医学专科研究所', '2021-03-20 22:24:59', '2021-03-20 22:24:59');
INSERT INTO `health` VALUES (84, 'M700', '药学研究所', '2021-03-20 22:24:59', '2021-03-20 22:24:59');
INSERT INTO `health` VALUES (85, 'N', '医学教育机构', '2021-03-21 10:19:42', '2021-03-21 10:19:42');
INSERT INTO `health` VALUES (86, 'N100', '医学普通高中等学校', '2021-03-21 10:19:43', '2021-03-21 10:19:43');
INSERT INTO `health` VALUES (87, 'N200', '医学成人学校', '2021-03-21 10:19:43', '2021-03-21 10:19:43');
INSERT INTO `health` VALUES (88, 'N300', '医学在职培训机构', '2021-03-21 10:19:44', '2021-03-21 10:19:44');
INSERT INTO `health` VALUES (89, 'O', '健康教育所（站、中心）', '2021-03-21 10:19:45', '2021-03-21 10:19:45');
INSERT INTO `health` VALUES (90, 'O100', '健康教育所', '2021-03-21 10:19:47', '2021-03-21 10:19:47');
INSERT INTO `health` VALUES (91, 'O200', '健康教育站（中心）', '2021-03-21 10:19:47', '2021-03-21 10:19:47');
INSERT INTO `health` VALUES (92, 'P', '其他卫生机构', '2021-03-21 10:19:47', '2021-03-21 10:19:47');
INSERT INTO `health` VALUES (93, 'P100', ' 临床检验中心', '2021-03-21 10:19:47', '2021-03-21 10:19:47');
INSERT INTO `health` VALUES (94, 'P200', ' 卫生新闻出版社', '2021-03-21 10:20:40', '2021-03-21 10:20:40');
INSERT INTO `health` VALUES (95, 'P900', ' 其他卫生事业机构', '2021-03-21 10:20:40', '2021-03-21 10:20:40');
INSERT INTO `health` VALUES (96, 'Q', ' 卫生社会团体', '2021-03-21 10:20:40', '2021-03-21 10:20:40');
INSERT INTO `health` VALUES (97, 'Q100', '红十字会', '2021-03-21 10:20:41', '2021-03-21 10:20:41');
INSERT INTO `health` VALUES (98, 'Q200', '医学会', '2021-03-21 10:20:41', '2021-03-21 10:20:41');
INSERT INTO `health` VALUES (99, 'Q300', '卫生协会', '2021-03-21 10:20:41', '2021-03-21 10:20:41');
INSERT INTO `health` VALUES (100, 'Q900', ' 其他卫生社会团体', '2021-03-21 10:20:41', '2021-03-21 10:20:41');
INSERT INTO `health` VALUES (101, 'A526', '骨科医院', '2021-03-21 10:21:05', '2021-03-21 10:21:05');
INSERT INTO `health` VALUES (102, 'M614', ' 肿瘤(防治)研究所', '2021-03-21 10:21:29', '2021-03-21 10:21:29');

-- ----------------------------
-- Table structure for hospital
-- ----------------------------
DROP TABLE IF EXISTS `hospital`;
CREATE TABLE `hospital`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院ID',
  `economic_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构所属经济类型编号',
  `sub_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属关系编码',
  `health_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卫生机构（组织）类别编码',
  `mechanism_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构级别编号',
  `level_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地域级别编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医院表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of hospital
-- ----------------------------
INSERT INTO `hospital` VALUES (1, '1110A10015', '11', '10', 'A100', '1', '5', '中央医院', '2021-03-21 15:43:11', '2021-03-21 15:43:11');
INSERT INTO `hospital` VALUES (2, '1240B10034', '12', '40', 'B100', '3', '4', '中西结合医院', '2021-03-21 15:43:15', '2021-03-21 15:43:15');
INSERT INTO `hospital` VALUES (3, '2120D13012', '21', '20', 'D130', '1', '2', '北方医院', '2021-03-21 15:43:19', '2021-03-21 15:43:19');
INSERT INTO `hospital` VALUES (4, '3010A10025', '30', '10', 'A100', '2', '5', '西北结合医院', '2021-03-21 15:43:23', '2021-03-21 15:43:23');
INSERT INTO `hospital` VALUES (5, '3462C22013', '34', '62', 'C220', '1', '3', '农合医院', '2021-03-21 15:43:26', '2021-03-21 15:43:26');
INSERT INTO `hospital` VALUES (6, '3420B10034', '34', '20', 'B100', '3', '4', '湖北中心医院', '2021-03-21 15:43:33', '2021-03-21 15:43:33');
INSERT INTO `hospital` VALUES (7, '3010D40095', '30', '10', 'D400', '9', '5', '武汉中医院', '2021-03-21 15:43:37', '2021-03-21 15:43:37');

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NULL DEFAULT NULL COMMENT '机构级别编号',
  `level_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构级别名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '(S201_06)  地域级别' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of level
-- ----------------------------
INSERT INTO `level` VALUES (1, 1, '村卫生室', '2021-03-20 22:02:27', '2021-03-20 22:02:27');
INSERT INTO `level` VALUES (2, 2, '乡镇卫生院', '2021-03-20 22:02:33', '2021-03-20 22:02:33');
INSERT INTO `level` VALUES (3, 3, '县级医疗机构', '2021-03-20 22:02:39', '2021-03-20 22:02:39');
INSERT INTO `level` VALUES (4, 4, '地市级医疗机构', '2021-03-20 22:02:44', '2021-03-20 22:02:44');
INSERT INTO `level` VALUES (5, 5, '省级及以上医疗机构', '2021-03-20 22:02:53', '2021-03-20 22:02:53');
INSERT INTO `level` VALUES (6, 9, '其他医疗机构', '2021-03-20 22:02:59', '2021-03-20 22:02:59');

-- ----------------------------
-- Table structure for limits
-- ----------------------------
DROP TABLE IF EXISTS `limits`;
CREATE TABLE `limits`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limit_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限编号',
  `limit_grade` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限等级',
  `limit_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限描述',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 172 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of limits
-- ----------------------------
INSERT INTO `limits` VALUES (1, 'Data', '-1', '系统管理', '2021-03-18 17:37:03', '2021-03-18 17:37:03');
INSERT INTO `limits` VALUES (2, 'loginEnd', '-1', '退出登录', '2021-03-19 16:58:20', '2021-03-19 16:58:20');
INSERT INTO `limits` VALUES (3, 'User', '1', '用户信息管理', '2021-03-18 17:37:03', '2021-03-18 17:37:03');
INSERT INTO `limits` VALUES (4, 'Role', '1', '角色信息管理', '2021-03-19 20:31:30', '2021-03-19 20:31:30');
INSERT INTO `limits` VALUES (5, 'Limit', '1', '权限信息管理', '2021-03-20 10:45:08', '2021-03-20 10:45:08');
INSERT INTO `limits` VALUES (6, 'findUserById', '3', '查询单个用户', '2021-03-20 14:54:10', '2021-03-20 14:54:10');
INSERT INTO `limits` VALUES (7, 'findUserAll', '3', '查询全部用户', '2021-03-20 14:54:57', '2021-03-20 14:54:57');
INSERT INTO `limits` VALUES (8, 'insertUser', '3', '进入用户新增页面', '2021-03-20 14:55:30', '2021-03-20 14:55:30');
INSERT INTO `limits` VALUES (9, 'addUser', '3', '新增用户', '2021-03-20 14:55:44', '2021-03-20 14:55:44');
INSERT INTO `limits` VALUES (10, 'updateUser', '3', '进入用户修改页面', '2021-03-20 14:56:04', '2021-03-20 14:56:04');
INSERT INTO `limits` VALUES (11, 'editUser', '3', '修改用户', '2021-03-20 14:56:16', '2021-03-20 14:56:16');
INSERT INTO `limits` VALUES (12, 'deleteUserById', '3', '删除用户', '2021-03-20 14:57:49', '2021-03-20 14:57:49');
INSERT INTO `limits` VALUES (13, 'editUserAndUpdate', '-1', '个人信息管理', '2021-03-20 14:56:48', '2021-03-20 14:56:48');
INSERT INTO `limits` VALUES (14, 'editUserAndLook', '13', '个人信息修改', '2021-03-20 14:57:06', '2021-03-20 14:57:06');
INSERT INTO `limits` VALUES (15, 'findRoleById', '4', '查询单个角色', '2021-03-20 14:59:35', '2021-03-20 14:59:35');
INSERT INTO `limits` VALUES (16, 'findRoleAll', '4', '查询全部角色', '2021-03-20 14:59:51', '2021-03-20 14:59:51');
INSERT INTO `limits` VALUES (17, 'insertRole', '4', '进入角色新增页面', '2021-03-20 14:59:58', '2021-03-20 14:59:58');
INSERT INTO `limits` VALUES (18, 'addRole', '4', '新增角色', '2021-03-20 15:00:29', '2021-03-20 15:00:29');
INSERT INTO `limits` VALUES (19, 'updateRole', '4', '进入角色修改页面', '2021-03-20 15:01:00', '2021-03-20 15:01:00');
INSERT INTO `limits` VALUES (20, 'editRole', '4', '修改角色', '2021-03-20 15:01:15', '2021-03-20 15:01:15');
INSERT INTO `limits` VALUES (21, 'deleteRoleById', '4', '删除角色', '2021-03-20 15:01:45', '2021-03-20 15:01:45');
INSERT INTO `limits` VALUES (22, 'findLimitById', '5', '查询单个权限', '2021-03-20 15:02:33', '2021-03-20 15:02:33');
INSERT INTO `limits` VALUES (23, 'findLimitAll', '5', '查询全部权限', '2021-03-20 15:02:34', '2021-03-20 15:02:34');
INSERT INTO `limits` VALUES (24, 'insertLimit', '5', '进入权限新增页面', '2021-03-20 15:03:13', '2021-03-20 15:03:13');
INSERT INTO `limits` VALUES (25, 'addLimit', '5', '新增权限', '2021-03-20 15:03:18', '2021-03-20 15:03:18');
INSERT INTO `limits` VALUES (26, 'updateLimit', '5', '进入权限修改界面', '2021-03-20 15:03:39', '2021-03-20 15:03:39');
INSERT INTO `limits` VALUES (27, 'editLimit', '5', '修改权限', '2021-03-20 15:04:04', '2021-03-20 15:04:04');
INSERT INTO `limits` VALUES (28, 'deleteLimitById', '5', '删除权限', '2021-03-20 15:04:17', '2021-03-20 15:04:17');
INSERT INTO `limits` VALUES (29, 'Agency', '1', '农合经办机构信息管理', '2021-03-25 15:28:36', '2021-03-25 15:28:36');
INSERT INTO `limits` VALUES (30, 'findAgencyById', '29', '农合经办机构信息单个查询', '2021-03-25 15:29:49', '2021-03-25 15:29:49');
INSERT INTO `limits` VALUES (31, 'findAgencyAll', '29', '农合经办机构信息全部查询', '2021-03-25 15:30:21', '2021-03-25 15:30:21');
INSERT INTO `limits` VALUES (32, 'insertAgency', '29', '进入农合经办机构新增界面', '2021-03-25 15:30:45', '2021-03-25 15:30:45');
INSERT INTO `limits` VALUES (33, 'addAgency', '29', '新增农合经办机构信息', '2021-03-25 15:30:57', '2021-03-25 15:30:57');
INSERT INTO `limits` VALUES (34, 'updateAgency', '29', '进入农合经办机构修改节目', '2021-03-25 15:31:39', '2021-03-25 15:31:39');
INSERT INTO `limits` VALUES (35, 'editAgency', '29', '修改农合经办机构信息', '2021-03-25 15:32:07', '2021-03-25 15:32:07');
INSERT INTO `limits` VALUES (36, 'deleteAgencyById', '29', '删除农合经办机构信息', '2021-03-25 15:32:33', '2021-03-25 15:32:33');
INSERT INTO `limits` VALUES (37, 'Chronic', '1', '慢性疾病信息管理', '2021-03-25 15:33:24', '2021-03-25 15:33:24');
INSERT INTO `limits` VALUES (38, 'findChronicById', '37', '慢性疾病信息单个查询', '2021-03-25 15:33:52', '2021-03-25 15:33:52');
INSERT INTO `limits` VALUES (39, 'findChronicAll', '37', '慢性疾病信息全部查询', '2021-03-25 15:34:16', '2021-03-25 15:34:16');
INSERT INTO `limits` VALUES (40, 'insertChronic', '37', '进入慢性疾病新增页面', '2021-03-25 15:34:31', '2021-03-25 15:34:31');
INSERT INTO `limits` VALUES (41, 'addChronic', '37', '新增慢性疾病信息', '2021-03-25 15:35:19', '2021-03-25 15:35:19');
INSERT INTO `limits` VALUES (42, 'updateChronic', '37', '进入慢性疾病修改页面', '2021-03-25 15:35:50', '2021-03-25 15:35:50');
INSERT INTO `limits` VALUES (43, 'editChronic', '37', '修改慢性疾病信息', '2021-03-25 15:36:10', '2021-03-25 15:36:10');
INSERT INTO `limits` VALUES (44, 'deleteChronicById', '37', '删除慢性疾病信息', '2021-03-25 15:36:28', '2021-03-25 15:36:28');
INSERT INTO `limits` VALUES (45, 'Policy', '1', '慢病政策数据管理', '2021-03-25 15:36:58', '2021-03-25 15:36:58');
INSERT INTO `limits` VALUES (46, 'findPolicyById', '45', '慢病政策数据单个查询', '2021-03-25 15:37:19', '2021-03-25 15:37:19');
INSERT INTO `limits` VALUES (47, 'findPolicyAll', '45', '慢病政策数据全部查询', '2021-03-25 15:37:40', '2021-03-25 15:37:40');
INSERT INTO `limits` VALUES (48, 'insertPolicy', '45', '进入慢病政策数据新增界面', '2021-03-25 15:38:20', '2021-03-25 15:38:20');
INSERT INTO `limits` VALUES (49, 'addPolicy', '45', '新增慢病政策数据', '2021-03-25 15:38:39', '2021-03-25 15:38:39');
INSERT INTO `limits` VALUES (50, 'updatePolicy', '45', '进入慢病政策数据修改界面', '2021-03-25 15:38:59', '2021-03-25 15:38:59');
INSERT INTO `limits` VALUES (51, 'editPolicy', '45', '修改慢病政策数据', '2021-03-25 15:39:15', '2021-03-25 15:39:15');
INSERT INTO `limits` VALUES (52, 'deletePolicyById', '45', '删除慢病政策数据', '2021-03-25 15:39:32', '2021-03-25 15:39:32');
INSERT INTO `limits` VALUES (53, 'Region', '1', '行政区域信息管理', '2021-03-25 15:40:11', '2021-03-25 15:40:11');
INSERT INTO `limits` VALUES (54, 'findRegionById', '53', '行政区域信息单个查询', '2021-03-25 15:40:38', '2021-03-25 15:40:38');
INSERT INTO `limits` VALUES (55, 'findRegionAll', '53', '行政区域信息全部查询', '2021-03-25 15:40:53', '2021-03-25 15:40:53');
INSERT INTO `limits` VALUES (56, 'insertRegion', '53', '进入行政区域信息新增界面', '2021-03-25 15:41:24', '2021-03-25 15:41:24');
INSERT INTO `limits` VALUES (57, 'addRegion', '53', '新增行政区域信息', '2021-03-25 15:41:39', '2021-03-25 15:41:39');
INSERT INTO `limits` VALUES (58, 'updateRegion', '53', '进入行政区域信息修改界面', '2021-03-25 15:42:03', '2021-03-25 15:42:03');
INSERT INTO `limits` VALUES (59, 'editRegion', '53', '修改行政区域信息', '2021-03-25 15:42:20', '2021-03-25 15:42:20');
INSERT INTO `limits` VALUES (60, 'deleteRegionById', '53', '删除行政区域信息', '2021-03-25 15:43:06', '2021-03-25 15:43:06');
INSERT INTO `limits` VALUES (61, 'Hospitals', '-1', '医院信息管理', '2021-04-18 22:01:25', '2021-04-18 22:01:25');
INSERT INTO `limits` VALUES (62, 'Economic', '61', '机构所属经济类型管理', '2021-04-18 22:02:25', '2021-04-18 22:02:25');
INSERT INTO `limits` VALUES (63, 'findEconomicById', '62', '机构所属经济类型单个查询', '2021-04-18 22:05:27', '2021-04-18 22:05:27');
INSERT INTO `limits` VALUES (64, 'findEconomicAll', '62', '机构所属经济类型全部查询', '2021-04-18 22:05:37', '2021-04-18 22:05:37');
INSERT INTO `limits` VALUES (65, 'insertEconomic', '62', '进入机构所属经济类型新增界面', '2021-04-18 22:06:00', '2021-04-18 22:06:00');
INSERT INTO `limits` VALUES (66, 'addEconomic', '62', '新增机构所属经济类型', '2021-04-18 22:06:18', '2021-04-18 22:06:18');
INSERT INTO `limits` VALUES (67, 'updateEconomic', '62', '进入机构所属经济类型修改界面', '2021-04-18 22:06:48', '2021-04-18 22:06:48');
INSERT INTO `limits` VALUES (68, 'editEconomic', '62', '修改机构所属经济类型', '2021-04-18 22:07:05', '2021-04-18 22:07:05');
INSERT INTO `limits` VALUES (69, 'deleteEconomicById', '62', '删除机构所属经济类型', '2021-04-18 22:07:26', '2021-04-18 22:07:26');
INSERT INTO `limits` VALUES (70, 'Sub', '61', '隶属关系管理', '2021-04-18 22:08:30', '2021-04-18 22:08:30');
INSERT INTO `limits` VALUES (71, 'findSubById', '70', '隶属关系单个查询', '2021-04-18 22:08:43', '2021-04-18 22:08:43');
INSERT INTO `limits` VALUES (72, 'findSubAll', '70', '隶属关系全部查询', '2021-04-18 22:09:06', '2021-04-18 22:09:06');
INSERT INTO `limits` VALUES (73, 'insertSub', '70', '进入隶属关系新增界面', '2021-04-18 22:09:34', '2021-04-18 22:09:34');
INSERT INTO `limits` VALUES (74, 'addSub', '70', '新增隶属关系', '2021-04-18 22:09:45', '2021-04-18 22:09:45');
INSERT INTO `limits` VALUES (75, 'updateSub', '70', '进入隶属关系修改界面', '2021-04-18 22:10:01', '2021-04-18 22:10:01');
INSERT INTO `limits` VALUES (76, 'editSub', '70', '修改隶属关系', '2021-04-18 22:10:11', '2021-04-18 22:10:11');
INSERT INTO `limits` VALUES (77, 'deleteSubById', '70', '删除隶属关系', '2021-04-18 22:10:21', '2021-04-18 22:10:21');
INSERT INTO `limits` VALUES (78, 'Health', '61', '卫生机构类别管理', '2021-04-18 22:11:05', '2021-04-18 22:11:05');
INSERT INTO `limits` VALUES (79, 'findHealthById', '78', '卫生机构类别单个查询', '2021-04-18 22:11:22', '2021-04-18 22:11:22');
INSERT INTO `limits` VALUES (80, 'findHealthAll', '78', '卫生机构类别全部查询', '2021-04-18 22:11:37', '2021-04-18 22:11:37');
INSERT INTO `limits` VALUES (81, 'insertHealth', '78', '进入卫生机构类别新增界面', '2021-04-18 22:11:54', '2021-04-18 22:11:54');
INSERT INTO `limits` VALUES (82, 'addHealth', '78', '新增卫生机构类别', '2021-04-18 22:12:03', '2021-04-18 22:12:03');
INSERT INTO `limits` VALUES (83, 'updateHealth', '78', '进入卫生机构类别修改界面', '2021-04-18 22:12:15', '2021-04-18 22:12:15');
INSERT INTO `limits` VALUES (84, 'editHealth', '78', '修改卫生机构类别', '2021-04-18 22:12:26', '2021-04-18 22:12:26');
INSERT INTO `limits` VALUES (85, 'deleteHealthById', '78', '删除卫生机构类别', '2021-04-18 22:12:39', '2021-04-18 22:12:39');
INSERT INTO `limits` VALUES (86, 'Mechanism', '61', '机构级别', '2021-04-18 22:15:13', '2021-04-18 22:15:13');
INSERT INTO `limits` VALUES (87, 'findMechanismById', '86', '机构级别单个查询', '2021-04-18 22:15:36', '2021-04-18 22:15:36');
INSERT INTO `limits` VALUES (88, 'findMechanismAll', '86', '机构级别全部查询', '2021-04-18 22:15:51', '2021-04-18 22:15:51');
INSERT INTO `limits` VALUES (89, 'insertMechanism', '86', '进入机构级别新增界面', '2021-04-18 22:20:45', '2021-04-18 22:20:45');
INSERT INTO `limits` VALUES (90, 'addMechanism', '86', '新增机构级别', '2021-04-18 22:20:55', '2021-04-18 22:20:55');
INSERT INTO `limits` VALUES (91, 'updateMechanism', '86', '进入机构级别修改界面', '2021-04-18 22:21:12', '2021-04-18 22:21:12');
INSERT INTO `limits` VALUES (92, 'editMechanism', '86', '修改机构级别', '2021-04-18 22:21:26', '2021-04-18 22:21:26');
INSERT INTO `limits` VALUES (93, 'deleteMechanismById', '86', '删除机构级别', '2021-04-18 22:21:38', '2021-04-18 22:21:38');
INSERT INTO `limits` VALUES (94, 'Level', '61', '地域级别管理', '2021-04-18 22:21:55', '2021-04-18 22:21:55');
INSERT INTO `limits` VALUES (95, 'findLevelById', '94', '地域级别单个查询', '2021-04-18 22:22:11', '2021-04-18 22:22:11');
INSERT INTO `limits` VALUES (96, 'findLevelAll', '94', '地域级别全部查询', '2021-04-18 22:22:26', '2021-04-18 22:22:26');
INSERT INTO `limits` VALUES (97, 'insertLevel', '94', '进入地域级别新增界面', '2021-04-18 22:22:42', '2021-04-18 22:22:42');
INSERT INTO `limits` VALUES (98, 'addLevel', '94', '新增地域级别', '2021-04-18 22:22:50', '2021-04-18 22:22:50');
INSERT INTO `limits` VALUES (99, 'updateLevel', '94', '进入地域级别修改界面', '2021-04-18 22:23:07', '2021-04-18 22:23:07');
INSERT INTO `limits` VALUES (100, 'editLevel', '94', '修改地域级别', '2021-04-18 22:23:19', '2021-04-18 22:23:19');
INSERT INTO `limits` VALUES (101, 'deleteLevelById', '94', '删除地域级别', '2021-04-18 22:23:28', '2021-04-18 22:23:28');
INSERT INTO `limits` VALUES (102, 'Hospital', '61', '医院信息管理', '2021-04-18 22:23:43', '2021-04-18 22:23:43');
INSERT INTO `limits` VALUES (103, 'findHospitalById', '102', '医院信息单个查询', '2021-04-18 22:24:00', '2021-04-18 22:24:00');
INSERT INTO `limits` VALUES (104, 'findHospitalAll', '102', '医院信息全部查询', '2021-04-18 22:24:14', '2021-04-18 22:24:14');
INSERT INTO `limits` VALUES (105, 'insertHospital', '102', '进入医院信息新增界面', '2021-04-18 22:24:32', '2021-04-18 22:24:32');
INSERT INTO `limits` VALUES (106, 'addHospital', '102', '新增医院信息', '2021-04-18 22:24:44', '2021-04-18 22:24:44');
INSERT INTO `limits` VALUES (107, 'updateHospital', '102', '进入医院信息修改界面', '2021-04-18 22:25:03', '2021-04-18 22:25:03');
INSERT INTO `limits` VALUES (108, 'editHospital', '102', '修改医院信息', '2021-04-18 22:25:16', '2021-04-18 22:25:16');
INSERT INTO `limits` VALUES (109, 'deleteHospitalById', '102', '删除医院信息', '2021-04-18 22:25:31', '2021-04-18 22:25:31');
INSERT INTO `limits` VALUES (110, 'Participation', '-1', '参合人员信息管理', '2021-04-18 22:26:12', '2021-04-18 22:26:12');
INSERT INTO `limits` VALUES (111, 'Family', '110', '参合人员档案信息管理', '2021-04-18 22:26:31', '2021-04-18 22:26:31');
INSERT INTO `limits` VALUES (112, 'findFamilyById', '111', '参合人员档案信息单个查询', '2021-04-18 22:26:46', '2021-04-18 22:26:46');
INSERT INTO `limits` VALUES (113, 'findFamilyAll', '111', '参合人员档案信息全部查询', '2021-04-18 22:26:58', '2021-04-18 22:26:58');
INSERT INTO `limits` VALUES (114, 'insertFamily', '111', '进入参合人员档案信息新增界面', '2021-04-18 22:27:11', '2021-04-18 22:27:11');
INSERT INTO `limits` VALUES (115, 'addFamily', '111', '新增参合人员档案信息', '2021-04-18 22:27:20', '2021-04-18 22:27:20');
INSERT INTO `limits` VALUES (116, 'updateFamily', '111', '进入参合人员档案信息修改界面', '2021-04-18 22:27:32', '2021-04-18 22:27:32');
INSERT INTO `limits` VALUES (117, 'editFamily', '111', '修改参合人员档案信息', '2021-04-18 22:27:43', '2021-04-18 22:27:43');
INSERT INTO `limits` VALUES (118, 'deleteFamilyById', '111', '删除参合人员档案信息', '2021-04-18 22:27:53', '2021-04-18 22:27:53');
INSERT INTO `limits` VALUES (119, 'Families', '110', '家庭档案信息管理', '2021-04-18 22:28:13', '2021-04-18 22:28:13');
INSERT INTO `limits` VALUES (120, 'findFamiliesById', '119', '家庭档案信息单个查询', '2021-04-18 22:28:25', '2021-04-18 22:28:25');
INSERT INTO `limits` VALUES (121, 'findFamiliesAll', '119', '家庭档案信息全部查询', '2021-04-18 22:28:41', '2021-04-18 22:28:41');
INSERT INTO `limits` VALUES (122, 'insertFamilies', '119', '进入家庭档案信息新增界面', '2021-04-18 22:28:57', '2021-04-18 22:28:57');
INSERT INTO `limits` VALUES (123, 'addFamilies', '119', '新增家庭档案信息', '2021-04-18 22:29:06', '2021-04-18 22:29:06');
INSERT INTO `limits` VALUES (124, 'updateFamilies', '119', '进入家庭档案信息修改界面', '2021-04-18 22:29:19', '2021-04-18 22:29:19');
INSERT INTO `limits` VALUES (125, 'editFamilies', '119', '修改家庭档案信息', '2021-04-18 22:29:30', '2021-04-18 22:29:30');
INSERT INTO `limits` VALUES (126, 'deleteFamiliesById', '119', '删除家庭档案信息', '2021-04-18 22:29:40', '2021-04-18 22:29:40');
INSERT INTO `limits` VALUES (127, 'Card', '110', '慢性病证信息管理', '2021-05-02 20:07:10', '2021-05-02 20:07:10');
INSERT INTO `limits` VALUES (128, 'findCardById', '127', '慢性病证信息单个查询', '2021-05-02 20:08:05', '2021-05-02 20:08:05');
INSERT INTO `limits` VALUES (129, 'findCardAll', '127', '慢性病证信息全部查询', '2021-05-02 20:08:45', '2021-05-02 20:08:45');
INSERT INTO `limits` VALUES (130, 'insertCard', '127', '进入慢性病证信息新增界面', '2021-05-02 20:09:15', '2021-05-02 20:09:15');
INSERT INTO `limits` VALUES (131, 'addCard', '127', '新增慢性病证信息', '2021-05-02 20:09:31', '2021-05-02 20:09:31');
INSERT INTO `limits` VALUES (132, 'updateCard', '127', '进入慢性病证信息修改界面', '2021-05-02 20:09:49', '2021-05-02 20:09:49');
INSERT INTO `limits` VALUES (133, 'editCard', '127', '修改慢性病证信息', '2021-05-02 20:10:02', '2021-05-02 20:10:02');
INSERT INTO `limits` VALUES (134, 'deleteCardById', '127', '删除慢性病证信息', '2021-05-02 20:10:15', '2021-05-02 20:10:15');
INSERT INTO `limits` VALUES (135, 'Personnel', '110', '人员信息管理', '2021-05-02 20:10:26', '2021-05-02 20:10:26');
INSERT INTO `limits` VALUES (136, 'findPersonnelById', '135', '人员信息单个查询', '2021-05-02 20:10:43', '2021-05-02 20:10:43');
INSERT INTO `limits` VALUES (137, 'findPersonnelAll', '135', '人员信息全部查询', '2021-05-02 20:10:57', '2021-05-02 20:10:57');
INSERT INTO `limits` VALUES (138, 'insertPersonnel', '135', '进入人员信息新增界面', '2021-05-02 20:11:15', '2021-05-02 20:11:15');
INSERT INTO `limits` VALUES (139, 'addPersonnel', '135', '新增人员信息', '2021-05-02 20:11:24', '2021-05-02 20:11:24');
INSERT INTO `limits` VALUES (140, 'updatePersonnel', '135', '进入人员信息修改界面', '2021-05-02 20:11:36', '2021-05-02 20:11:36');
INSERT INTO `limits` VALUES (141, 'editPersonnel', '135', '修改人员信息', '2021-05-02 20:11:45', '2021-05-02 20:11:45');
INSERT INTO `limits` VALUES (142, 'deletePersonnelById', '135', '删除人员信息', '2021-05-02 20:11:59', '2021-05-02 20:11:59');
INSERT INTO `limits` VALUES (143, 'insertLimits', '5', '进入批量权限新增页面', '2021-05-08 23:40:08', '2021-05-19 16:21:02');
INSERT INTO `limits` VALUES (144, 'addLimits', '5', '批量新增权限', '2021-05-08 23:40:08', '2021-05-19 16:21:11');
INSERT INTO `limits` VALUES (145, 'Money', '-1', '缴费管理', '2021-05-08 23:41:29', '2021-05-19 16:21:46');
INSERT INTO `limits` VALUES (146, 'Pay', '145', '参合缴费登记管理', '2021-05-19 16:21:58', '2021-05-19 16:21:58');
INSERT INTO `limits` VALUES (147, 'ChronicReim', '145', '慢性病报销信息管理', '2021-05-19 16:22:16', '2021-05-19 16:22:16');
INSERT INTO `limits` VALUES (148, 'findPayAll', '146', '参合缴费信息全部查询', '2021-05-20 11:02:55', '2021-05-20 11:02:55');
INSERT INTO `limits` VALUES (149, 'insertPay', '146', '进入参合缴费信息新增界面', '2021-05-20 11:03:06', '2021-05-20 11:03:43');
INSERT INTO `limits` VALUES (150, 'addPay', '146', '新增参合缴费信息', '2021-05-20 11:03:11', '2021-05-20 11:04:05');
INSERT INTO `limits` VALUES (151, 'updatePay', '146', '进入参合缴费信息修改界面', '2021-05-20 11:03:16', '2021-05-20 11:03:48');
INSERT INTO `limits` VALUES (152, 'editPay', '146', '修改参合缴费信息', '2021-05-20 11:03:21', '2021-05-20 11:04:02');
INSERT INTO `limits` VALUES (153, 'deletePayById', '146', '删除参合缴费信息', '2021-05-20 11:03:30', '2021-05-20 11:03:59');
INSERT INTO `limits` VALUES (154, 'findReimAll', '147', '慢性病报销信息全部查询', '2021-05-20 11:04:25', '2021-05-20 11:13:09');
INSERT INTO `limits` VALUES (155, 'insertReim', '147', '进入慢性病报销信息新增界面', '2021-05-20 11:04:53', '2021-05-20 11:13:05');
INSERT INTO `limits` VALUES (156, 'addReim', '147', '新增慢性病报销信息', '2021-05-20 11:04:55', '2021-05-20 11:13:02');
INSERT INTO `limits` VALUES (157, 'updateReim', '147', '进入慢性病报销信息修改界面', '2021-05-20 11:05:00', '2021-05-20 11:13:01');
INSERT INTO `limits` VALUES (158, 'editReim', '147', '修改慢性病报销信息', '2021-05-20 11:05:03', '2021-05-20 11:12:55');
INSERT INTO `limits` VALUES (159, 'deleteReimById', '147', '删除慢性病报销信息', '2021-05-20 11:05:10', '2021-05-20 11:12:59');
INSERT INTO `limits` VALUES (160, 'Pays', '145', '参合缴费登记记录', '2021-05-20 16:27:23', '2021-05-20 16:27:23');
INSERT INTO `limits` VALUES (161, 'findPaysAll', '160', '参合缴费登记记录全部查询', '2021-05-20 16:27:32', '2021-05-20 16:28:40');
INSERT INTO `limits` VALUES (162, 'insertPays', '160', '进入参合缴费登记记录新增界面', '2021-05-20 16:27:39', '2021-05-20 16:28:45');
INSERT INTO `limits` VALUES (163, 'addPays', '160', '新增参合缴费登记记录', '2021-05-20 16:27:42', '2021-05-20 16:28:48');
INSERT INTO `limits` VALUES (164, 'updatePays', '160', '进入参合缴费登记记录修改界面', '2021-05-20 16:27:48', '2021-05-20 16:28:54');
INSERT INTO `limits` VALUES (165, 'editPays', '160', '修改参合缴费登记记录', '2021-05-20 16:27:53', '2021-05-20 16:28:58');
INSERT INTO `limits` VALUES (166, 'deletePaysById', '160', '删除参合缴费登记记录', '2021-05-20 16:27:59', '2021-05-20 16:29:03');
INSERT INTO `limits` VALUES (167, 'reimEdit', '147', '进入慢性病报销界面', '2021-05-22 19:05:57', '2021-05-22 19:05:57');
INSERT INTO `limits` VALUES (168, 'reimEditMent', '147', '报销', '2021-05-22 20:06:00', '2021-05-22 20:06:06');
INSERT INTO `limits` VALUES (169, 'cardExcelDownloads', '127', '导出execl表格', '2021-05-22 21:53:12', '2021-05-22 21:53:54');
INSERT INTO `limits` VALUES (170, 'getcountbydisName', '147', '图表-点状图', '2021-05-26 20:44:07', '2021-05-26 23:58:18');
INSERT INTO `limits` VALUES (171, 'getcountbydisName2', '147', '图表-圆形图', '2021-05-26 23:58:35', '2021-05-26 23:58:35');

-- ----------------------------
-- Table structure for mechanism
-- ----------------------------
DROP TABLE IF EXISTS `mechanism`;
CREATE TABLE `mechanism`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mechanism_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构级别编号',
  `mechanism_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构级别名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '(S201_04)  机构级别' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mechanism
-- ----------------------------
INSERT INTO `mechanism` VALUES (1, '1', '综合定点', '2021-03-20 22:05:17', '2021-03-20 22:05:17');
INSERT INTO `mechanism` VALUES (2, '2', '门诊定点', '2021-03-20 22:05:24', '2021-03-20 22:05:24');
INSERT INTO `mechanism` VALUES (3, '3', '住院定点', '2021-03-20 22:05:28', '2021-03-20 22:05:28');
INSERT INTO `mechanism` VALUES (4, '9', '其他定点', '2021-03-20 22:05:31', '2021-03-20 22:05:31');

-- ----------------------------
-- Table structure for pay
-- ----------------------------
DROP TABLE IF EXISTS `pay`;
CREATE TABLE `pay`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缴费编号',
  `pay_text` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '参合缴费标准',
  `year` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '参合缴费年份',
  `money` double(11, 0) NULL DEFAULT NULL COMMENT '缴费金额',
  `start_time` date NULL DEFAULT NULL COMMENT '起始时间',
  `end_time` date NULL DEFAULT NULL COMMENT '终止时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '参合缴费标准' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pay
-- ----------------------------
INSERT INTO `pay` VALUES (1, 'N4325202105101929', '1000*n', '2021', 1000, '2021-05-19', '2021-10-29', '2021-05-19 14:33:46', '2021-05-20 23:07:55');
INSERT INTO `pay` VALUES (2, 'N4325202105101922', '1200*n + 500', '2018', 1200, '2020-10-06', '2022-05-27', '2021-05-20 15:16:00', '2021-05-20 20:48:45');

-- ----------------------------
-- Table structure for pays
-- ----------------------------
DROP TABLE IF EXISTS `pays`;
CREATE TABLE `pays`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pays_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参合缴费记录编号',
  `family_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭编号',
  `now_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当次缴费人姓名',
  `family_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭已缴费人员姓名',
  `family_no_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭未缴费人员姓名',
  `family_num` int(2) NULL DEFAULT NULL COMMENT '家庭已缴费人数',
  `money` double(10, 0) NULL DEFAULT NULL COMMENT '当次缴费金额',
  `moneys` double(10, 0) NULL DEFAULT NULL COMMENT '家庭缴费总金额',
  `year` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭缴费年份',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '缴费时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参合缴费登记记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pays
-- ----------------------------
INSERT INTO `pays` VALUES (1, '45042101010100482018', '4504210101010048', '蔡云妮,', '钟海清,蔡云妮,', '钟艳玲,钟子峰,钟艳婷,', 3, 1000, 2000, '2021', '2021-05-20 22:29:03', '2021-05-20 15:38:09', '2021-05-20 22:55:09');
INSERT INTO `pays` VALUES (6, '450421010101234562021', '4504210101012345', '傅凌湘,', '傅凌湘,', '测试人员,', 1, 1000, 1000, '2021', '2021-05-26 20:22:46', '2021-05-20 23:03:02', '2021-05-26 20:22:48');
INSERT INTO `pays` VALUES (23, '45042101010100512021', '4504210101010051', '张三,', '张三,', NULL, 0, 1000, 1000, '2021', '2021-05-24 16:05:38', '2021-05-24 16:05:37', '2021-05-24 16:05:40');
INSERT INTO `pays` VALUES (25, '1231232021', '123123', '123123,', '123123,', NULL, 0, 1000, 1000, '2021', '2021-05-27 11:52:44', '2021-05-27 11:52:43', '2021-05-27 11:52:46');

-- ----------------------------
-- Table structure for personnel
-- ----------------------------
DROP TABLE IF EXISTS `personnel`;
CREATE TABLE `personnel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `gender` int(11) NULL DEFAULT NULL COMMENT '性别（男：1，女：0）',
  `age` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份证号',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `nation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民族',
  `person_attributes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员属性（党员，共青团员，少先队员，群众）',
  `degree` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文化程度（小学，初中，高中，大学，研究生，博士生）',
  `health` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '健康状况（健康，有疾病，有重大疾病）',
  `countryside` int(11) NULL DEFAULT NULL COMMENT '是否是农村户口（是：1，否：0）',
  `occupation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职业',
  `work_unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作单位',
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `home_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '常住家庭住址',
  `marriage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结婚情况（未婚，初婚，二婚...）',
  `marriage_time` date NULL DEFAULT NULL COMMENT '结婚时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of personnel
-- ----------------------------
INSERT INTO `personnel` VALUES (1, '张佳辉', 1, '21', '429006200007135719', '2000-07-13', '汉', '共青团员', '高中', '健康', 1, '学生', '梧州学院', '17683864164', '17683864164', '广西省梧州市万秀区富民三路梧州学院A10-904', '未婚', '2021-05-02', '2021-05-02 11:46:35', '2021-05-02 11:46:35');
INSERT INTO `personnel` VALUES (6, '测试人员', 1, '20', '429006200007135721', '2021-05-01', '汉族', '中共党员', '硕士', '健康', 0, '公务员', '梧州学院', '17683864164', '17683864164', '湖北省天门市', '未婚', NULL, '2021-05-16 22:29:33', '2021-05-17 09:21:25');
INSERT INTO `personnel` VALUES (7, '傅凌湘', 0, '18', '429006200312131234', '2001-11-21', '汉族', '中共党员', '博士', '健康', 1, '公务员', '梧州学院', '17683864164', '17683864164', '湖北省天门市', '初婚', '2021-11-02', '2021-05-16 23:26:00', '2021-05-17 09:21:19');
INSERT INTO `personnel` VALUES (8, '钟海清', 0, '45', '450421195108247000', '2001-11-21', '汉族', '无党派人士', '初中', '健康', 1, '公务员', '梧州学院', '17683864164', '17683864164', '湖北省天门市', '初婚', '1971-11-02', '2021-05-17 09:33:25', '2021-05-17 09:33:25');
INSERT INTO `personnel` VALUES (9, '张三', 1, '20', '429006200005212594', '2000-05-21', '汉族', '民盟盟员', '大专', '轻微疾病', 1, '工人', '梧州学院', '17683864164', '17683864164', '湖北省天门市', '未婚', NULL, '2021-05-23 15:27:53', '2021-05-23 15:27:53');
INSERT INTO `personnel` VALUES (10, '王五', 1, '20', '429006200007248954', '1998-05-01', '汉族', '中共党员', '硕士', '轻微疾病', 1, '工人', '梧州学院', '17683864164', '17683864164', '湖北省天门市', '未婚', NULL, '2021-05-27 11:34:34', '2021-05-27 11:34:34');
INSERT INTO `personnel` VALUES (11, '123123', 1, '20', '123123123123123123', '2021-05-01', '汉族', NULL, NULL, NULL, 1, '工人', '梧州学院', '17683864164', '17683864164', '湖北省天门市', '未婚', NULL, '2021-05-27 11:51:13', '2021-05-27 11:52:40');

-- ----------------------------
-- Table structure for policy
-- ----------------------------
DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '慢病年度ID',
  `year` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年度',
  `capp_line` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封顶线',
  `reim_ratio` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报销比例',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '慢病政策数据模型' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of policy
-- ----------------------------
INSERT INTO `policy` VALUES (1, '2018001', '2018', '4200', '0.3', '2021-03-21 10:32:20', '2021-05-22 18:42:52');
INSERT INTO `policy` VALUES (2, '2021001', '2021', '5300', '0.4', '2021-05-22 18:42:35', '2021-05-22 18:43:00');

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域编码',
  `region_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `levels` int(11) NULL DEFAULT NULL COMMENT '级别',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '行政区域信息模型' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of region
-- ----------------------------
INSERT INTO `region` VALUES (1, '450421', '苍梧县', 1, '2021-03-20 21:36:23', '2021-03-20 21:36:23');
INSERT INTO `region` VALUES (2, '45042101', '龙圩镇', 2, '2021-03-20 21:36:34', '2021-03-20 21:36:34');
INSERT INTO `region` VALUES (3, '4504210101', '龙圩镇恩义村', 3, '2021-03-20 21:36:43', '2021-03-20 21:36:43');
INSERT INTO `region` VALUES (4, '450421010101', '龙圩镇恩义村多一组', 4, '2021-03-20 21:36:50', '2021-03-20 21:36:50');
INSERT INTO `region` VALUES (5, '430201', '天门市', 1, '2021-03-25 10:31:53', '2021-03-25 10:31:53');
INSERT INTO `region` VALUES (6, '43020101', '皂市镇', 2, '2021-03-25 10:33:08', '2021-03-25 10:33:08');
INSERT INTO `region` VALUES (7, '4302010101', '白土山村', 3, '2021-03-25 10:33:24', '2021-03-25 10:33:24');
INSERT INTO `region` VALUES (8, '430201010109', '白土山村九组', 4, '2021-03-25 10:33:46', '2021-03-25 10:33:46');
INSERT INTO `region` VALUES (9, '430201010101', '白土山村一组', 4, '2021-03-25 15:21:02', '2021-03-25 15:21:02');
INSERT INTO `region` VALUES (10, '450421010109', '龙圩镇恩义村多九组', 4, '2021-03-25 15:23:37', '2021-03-25 15:23:37');
INSERT INTO `region` VALUES (11, '430201010102', '白土山村二组', 4, '2021-03-25 15:56:55', '2021-03-25 15:56:55');
INSERT INTO `region` VALUES (12, '430201010103', '白土山村三组', 4, '2021-03-25 15:57:36', '2021-03-25 15:57:36');
INSERT INTO `region` VALUES (13, '430201010105', '白土山村四组', 4, '2021-03-25 15:57:54', '2021-03-25 15:57:54');
INSERT INTO `region` VALUES (14, '4302010102', '文敦', 3, '2021-03-25 15:58:23', '2021-03-25 15:58:23');
INSERT INTO `region` VALUES (15, '430201010201', '文敦一组', 4, '2021-03-25 15:58:44', '2021-03-25 15:58:44');
INSERT INTO `region` VALUES (16, '430201010202', '文敦二组', 4, '2021-03-25 15:59:00', '2021-03-25 15:59:00');
INSERT INTO `region` VALUES (17, '43020102', '九真', 2, '2021-03-25 15:59:37', '2021-03-25 15:59:37');
INSERT INTO `region` VALUES (18, '430202', '仙桃市', 1, '2021-03-25 16:00:15', '2021-03-25 16:00:15');
INSERT INTO `region` VALUES (19, '430203', '潜江市', 1, '2021-03-25 16:00:33', '2021-03-25 16:00:33');
INSERT INTO `region` VALUES (20, '43020201', '胡市', NULL, '2021-03-25 16:01:01', '2021-03-25 16:01:01');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色编号',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_limit` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '角色权限',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 101, '超级管理员', 'Data,User,findUserById,findUserAll,insertUser,addUser,updateUser,editUser,deleteUserById,Role,findRoleById,findRoleAll,insertRole,addRole,updateRole,editRole,deleteRoleById,Limit,findLimitById,findLimitAll,insertLimit,addLimit,updateLimit,editLimit,deleteLimitById,insertLimits,addLimits,Agency,findAgencyById,findAgencyAll,insertAgency,addAgency,updateAgency,editAgency,deleteAgencyById,Chronic,findChronicById,findChronicAll,insertChronic,addChronic,updateChronic,editChronic,deleteChronicById,Policy,findPolicyById,findPolicyAll,insertPolicy,addPolicy,updatePolicy,editPolicy,deletePolicyById,Region,findRegionById,findRegionAll,insertRegion,addRegion,updateRegion,editRegion,deleteRegionById,loginEnd,editUserAndUpdate,editUserAndLook,Hospitals,Economic,findEconomicById,findEconomicAll,insertEconomic,addEconomic,updateEconomic,editEconomic,deleteEconomicById,Sub,findSubById,findSubAll,insertSub,addSub,updateSub,editSub,deleteSubById,Health,findHealthById,findHealthAll,insertHealth,addHealth,updateHealth,editHealth,deleteHealthById,Mechanism,findMechanismById,findMechanismAll,insertMechanism,addMechanism,updateMechanism,editMechanism,deleteMechanismById,Level,findLevelById,findLevelAll,insertLevel,addLevel,updateLevel,editLevel,deleteLevelById,Hospital,findHospitalById,findHospitalAll,insertHospital,addHospital,updateHospital,editHospital,deleteHospitalById,Participation,Family,findFamilyById,findFamilyAll,insertFamily,addFamily,updateFamily,editFamily,deleteFamilyById,Families,findFamiliesById,findFamiliesAll,insertFamilies,addFamilies,updateFamilies,editFamilies,deleteFamiliesById,Card,findCardById,findCardAll,insertCard,addCard,updateCard,editCard,deleteCardById,cardExcelDownloads,Personnel,findPersonnelById,findPersonnelAll,insertPersonnel,addPersonnel,updatePersonnel,editPersonnel,deletePersonnelById,Money,Pay,findPayAll,insertPay,addPay,updatePay,editPay,deletePayById,ChronicReim,findReimAll,insertReim,addReim,updateReim,editReim,deleteReimById,reimEdit,reimEditMent,getcountbydisName,getcountbydisName2,Pays,findPaysAll,insertPays,addPays,updatePays,editPays,deletePaysById,', '2021-03-19 17:11:46', '2021-05-27 00:00:11');
INSERT INTO `role` VALUES (2, 102, '县合管办领导', 'loginEnd,editUserAndUpdate,editUserAndLook,Participation,Family,findFamilyById,findFamilyAll,insertFamily,addFamily,updateFamily,editFamily,deleteFamilyById,Families,findFamiliesById,findFamiliesAll,insertFamilies,addFamilies,updateFamilies,editFamilies,deleteFamiliesById,Card,findCardById,findCardAll,insertCard,addCard,updateCard,editCard,deleteCardById,cardExcelDownloads,Personnel,findPersonnelById,findPersonnelAll,insertPersonnel,addPersonnel,updatePersonnel,editPersonnel,deletePersonnelById,Money,Pay,findPayAll,insertPay,addPay,updatePay,editPay,deletePayById,ChronicReim,findReimAll,insertReim,addReim,updateReim,editReim,deleteReimById,reimEdit,reimEditMent,getcountbydisName,getcountbydisName2,Pays,findPaysAll,insertPays,addPays,updatePays,editPays,deletePaysById,', '2021-03-19 17:11:46', '2021-05-27 00:00:15');
INSERT INTO `role` VALUES (3, 103, '县合管办经办人', 'loginEnd,editUserAndUpdate,editUserAndLook,Money,ChronicReim,findReimAll,insertReim,addReim,updateReim,editReim,deleteReimById,reimEdit,reimEditMent,', '2021-03-19 17:11:46', '2021-05-23 20:56:18');
INSERT INTO `role` VALUES (4, 104, '乡镇农合经办人', 'loginEnd,editUserAndUpdate,editUserAndLook,Participation,Family,findFamilyById,findFamilyAll,insertFamily,addFamily,updateFamily,editFamily,deleteFamilyById,Families,findFamiliesById,findFamiliesAll,insertFamilies,addFamilies,updateFamilies,editFamilies,deleteFamiliesById,Card,findCardById,findCardAll,insertCard,addCard,updateCard,editCard,deleteCardById,cardExcelDownloads,Personnel,findPersonnelById,findPersonnelAll,insertPersonnel,addPersonnel,updatePersonnel,editPersonnel,deletePersonnelById,Money,Pay,findPayAll,insertPay,addPay,updatePay,editPay,deletePayById,ChronicReim,findReimAll,insertReim,addReim,updateReim,editReim,deleteReimById,reimEdit,reimEditMent,Pays,findPaysAll,insertPays,addPays,updatePays,editPays,deletePaysById,', '2021-03-19 17:11:46', '2021-05-23 20:56:47');

-- ----------------------------
-- Table structure for sub
-- ----------------------------
DROP TABLE IF EXISTS `sub`;
CREATE TABLE `sub`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属关系编码',
  `sub_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属关系名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '(S201_02)  隶属关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sub
-- ----------------------------
INSERT INTO `sub` VALUES (1, '10', '中央', '2021-03-20 21:58:13', '2021-03-20 21:58:13');
INSERT INTO `sub` VALUES (2, '20', '省', '2021-03-20 21:58:42', '2021-03-20 21:58:42');
INSERT INTO `sub` VALUES (3, '40', '市、地区', '2021-03-20 21:58:47', '2021-03-20 21:58:47');
INSERT INTO `sub` VALUES (4, '50', '县', '2021-03-20 21:58:57', '2021-03-20 21:58:57');
INSERT INTO `sub` VALUES (5, '61', '街道', '2021-03-20 21:59:02', '2021-03-20 21:59:02');
INSERT INTO `sub` VALUES (6, '62', '镇', '2021-03-20 21:59:08', '2021-03-20 21:59:08');
INSERT INTO `sub` VALUES (7, '63', '乡', '2021-03-20 21:59:13', '2021-03-20 21:59:13');
INSERT INTO `sub` VALUES (8, '71', '居民委员会', '2021-03-20 21:59:19', '2021-03-20 21:59:19');
INSERT INTO `sub` VALUES (9, '72', '村民委员会', '2021-03-20 21:59:25', '2021-03-20 21:59:25');
INSERT INTO `sub` VALUES (10, '90', '其他', '2021-03-20 21:59:33', '2021-03-20 21:59:33');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(8) NOT NULL COMMENT '用户名',
  `pass_word` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '职位',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (111, '111', 102);
INSERT INTO `user` VALUES (123, '123', 101);
INSERT INTO `user` VALUES (129, '123', 102);
INSERT INTO `user` VALUES (222, '222', 102);
INSERT INTO `user` VALUES (321, '321', 103);
INSERT INTO `user` VALUES (333, '333', 103);
INSERT INTO `user` VALUES (423, '3213', 103);
INSERT INTO `user` VALUES (444, '444', 104);
INSERT INTO `user` VALUES (3242, '34234', 103);
INSERT INTO `user` VALUES (23143, '123456', 103);
INSERT INTO `user` VALUES (23523, '31244', 103);
INSERT INTO `user` VALUES (43532, '342354', 103);
INSERT INTO `user` VALUES (123456, '123456', 103);

SET FOREIGN_KEY_CHECKS = 1;
