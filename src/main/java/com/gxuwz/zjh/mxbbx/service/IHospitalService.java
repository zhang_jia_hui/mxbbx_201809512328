package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Hospital;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 医院表 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
public interface IHospitalService extends IService<Hospital> {

    // 根据 id 查询对应用户信息
    Hospital findHospitalById(Hospital hospital);

    // 查询全部用户信息
    List<Hospital> findHospitalAll();

    //自定义sql 分页
    IPage<Hospital> selectHospitalPage(Page<Hospital> page, @Param(Constants.WRAPPER) Wrapper<Hospital> wrapper);

    // 添加用户信息
    void addHospital(Hospital hospital);

    // 修改用户信息
    void updateHospitalById(Hospital hospital);

    // 删除用户信息
    void deleteHospitalById(Hospital hospital);

}
