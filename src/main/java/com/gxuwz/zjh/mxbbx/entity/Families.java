package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 参合人员档案信息表
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Families") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Families对象", description="参合人员档案信息表")
public class Families implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "家庭成员编号")
    private String familiesId;

    @ApiModelProperty(value = "家庭编号")
    private String familyId;

    @ApiModelProperty(value = "农合证号")
    private String nongheId;

    @ApiModelProperty(value = "医疗证卡号")
    private String medicalId;

    @ApiModelProperty(value = "户内编号")
    private String indoorId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "与户主关系")
    private String holderRela;

    @ApiModelProperty(value = "身份证号")
    private String number;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
