package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Economic;
import com.gxuwz.zjh.mxbbx.mapper.EconomicMapper;
import com.gxuwz.zjh.mxbbx.service.IEconomicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * (S201_01)  机构所属经济类型 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class EconomicServiceImpl extends ServiceImpl<EconomicMapper, Economic> implements IEconomicService {

    @Autowired
    private EconomicMapper economicMapper;

    @Override
    public Economic findEconomicById(Economic economic) {
        return economicMapper.findEconomicById(economic);
    }

    @Override
    public List<Economic> findEconomicAll() {
        return economicMapper.findEconomicAll();
    }

    @Override
    public IPage<Economic> selectEconomicPage(Page<Economic> page, Wrapper<Economic> wrapper) {
        return economicMapper.selectEconomicPage(page, wrapper);
    }

    @Override
    public void addEconomic(Economic economic) {
        economicMapper.addEconomic(economic);
    }

    @Override
    public void updateEconomicById(Economic economic) {
        economicMapper.updateEconomicById(economic);
    }

    @Override
    public void deleteEconomicById(Economic economic) {
        economicMapper.deleteEconomicById(economic);
    }
}
