package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Mechanism;
import com.gxuwz.zjh.mxbbx.mapper.MechanismMapper;
import com.gxuwz.zjh.mxbbx.service.IMechanismService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * (S201_04)  机构级别 服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Service
public class MechanismServiceImpl extends ServiceImpl<MechanismMapper, Mechanism> implements IMechanismService {

    @Autowired
    private MechanismMapper mechanismMapper;

    @Override
    public Mechanism findMechanismById(Mechanism mechanism) {
        return mechanismMapper.findMechanismById(mechanism);
    }

    @Override
    public List<Mechanism> findMechanismAll() {
        return mechanismMapper.findMechanismAll();
    }

    @Override
    public IPage<Mechanism> selectMechanismPage(Page<Mechanism> page, Wrapper<Mechanism> wrapper) {
        return mechanismMapper.selectMechanismPage(page, wrapper);
    }

    @Override
    public void addMechanism(Mechanism mechanism) {
        mechanismMapper.addMechanism(mechanism);
    }

    @Override
    public void updateMechanismById(Mechanism mechanism) {
        mechanismMapper.updateMechanismById(mechanism);
    }

    @Override
    public void deleteMechanismById(Mechanism mechanism) {
        mechanismMapper.deleteMechanismById(mechanism);
    }
}
