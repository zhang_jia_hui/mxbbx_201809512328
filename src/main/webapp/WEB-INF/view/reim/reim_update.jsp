<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/reim/editReim">
            <div class="form-group">
                <div class="label">
                    <label>医院发票号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入医院发票号" value="${chronicReim.invoice}"
                           name="invoice" data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>医院花费：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入预报销金额"  value="${chronicReim.wantPay}"
                           name="wantPay" data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>实际报销金额：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入实际报销金额"  value="${chronicReim.nowPay}"
                           name="nowPay" data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>报销总金额：</label>
                </div>
                <div class="field">
                    <input type="number" class="input w50" placeholder="请输入报销总金额"  value="${chronicReim.payAll}"
                           name="payAll" data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>参合农民身份证号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入参合农民身份证号"  value="${chronicReim.number}"
                           name="number" data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>慢性病：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入慢性病"
                            <c:forEach items="${chronicList}" var="map">
                                <c:if test="${chronicReim.chronicId eq map.chronicId}" >value="${map.chronicName}"</c:if>
                            </c:forEach>
                           name="chronicId" data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>领款时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入领款时间"  value="${chronicReim.time}"
                           id="time" name="time" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                laydate.render({
                  elem: '#time', //指定元素
                  type: 'datetime'
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label>操作员：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入操作员姓名"  value="${chronicReim.person}" name="person"
                           data-validate="required:必填" readonly="readonly"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>状态：</label>
                </div>
                <div class="label">
                    <label><input type="radio" name="start" value="0" data-validate="required:必选"
                                  <c:if test="${chronicReim.start eq 0}">checked</c:if>
                    >未审批</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="start" value="1" checked="checked"
                                  <c:if test="${chronicReim.start eq 1}">checked</c:if>
                    >批准</label>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <label><input type="radio" name="start" value="2"
                                  <c:if test="${chronicReim.start eq 2}">checked</c:if>
                    >拒批</label>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/reim/findReimAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
