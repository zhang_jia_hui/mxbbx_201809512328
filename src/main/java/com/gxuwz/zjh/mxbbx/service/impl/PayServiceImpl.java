package com.gxuwz.zjh.mxbbx.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxuwz.zjh.mxbbx.entity.Pay;
import com.gxuwz.zjh.mxbbx.mapper.PayMapper;
import com.gxuwz.zjh.mxbbx.service.IPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-14
 */
@Service
public class PayServiceImpl extends ServiceImpl<PayMapper, Pay> implements IPayService {

    @Autowired
    private PayMapper payMapper;

    @Override
    public Pay findPayById(Pay pay) {
        return payMapper.findPayById(pay);
    }

    @Override
    public Pay findPayByYear(Pay pay) {
        return payMapper.findPayByYear(pay);
    }

    @Override
    public List<Pay> findPayAll() {
        return payMapper.findPayAll();
    }

    @Override
    public IPage<Pay> selectPayPage(Page<Pay> page, Wrapper<Pay> wrapper) {
        return payMapper.selectPayPage(page, wrapper);
    }

    @Override
    public void addPay(Pay pay) {
        payMapper.addPay(pay);
    }

    @Override
    public void updatePayById(Pay pay) {
        payMapper.updatePayById(pay);
    }

    @Override
    public void deletePayById(Pay pay) {
        payMapper.deletePayById(pay);
    }
}
