<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
    <!-- 引入日期插件 -->
    <script type="text/javascript" src="<%=basePath %>/js/layui/css/layui.css"></script>
    <script src="<%=path%>/js/layui/layui.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 新增信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/card/addCard">
            <div class="form-group">
                <div class="label">
                    <label>慢性病证编号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入慢性病证编号(例: 20181123125412345)" name="cardId" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>姓名：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入姓名(例: 钟海清)" name="name" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>家庭住址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入家庭住址(例: 广西省苍梧县龙圩镇恩义村多一组)" name="address" data-validate="required:必填" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>慢性病疾病名称：</label>
                </div>
                <div class="field">
                    <select id="chronicId" style="width:200px" name="chronicId" class="input w50">
                        <option value="E14.901">请选择慢性病疾病</option>
                        <c:forEach items="${chronicList}" var="map">
                            <option value="${map.getChronicId()}">${map.getChronicName()}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>慢性病起始时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入慢性病起始时间" id="chronicStartTime" name="chronicStartTime" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>慢性病结束时间：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" placeholder="请输入慢性病结束时间" id="chronicEndTime" name="chronicEndTime" />
                    <div class="tips"></div>
                </div>
            </div>
            <script>
              layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                  elem: '#chronicStartTime' //指定元素
                });
                laydate.render({
                  elem: '#chronicEndTime' //指定元素
                });
              });
            </script>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/card/findCardAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>
