package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 人员信息表
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
@Data
@Alias("Personnel") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Personnel对象", description="人员信息表")
public class Personnel implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别（男：1，女：0）")
    private Integer gender;

    @ApiModelProperty(value = "年龄")
    private String age;

    @ApiModelProperty(value = "身份证号")
    private String number;

    @ApiModelProperty(value = "出生日期")
    private String birthday;

    @ApiModelProperty(value = "民族")
    private String nation;

    @ApiModelProperty(value = "人员属性（党员，共青团员，少先队员，群众...）")
    private String personAttributes;

    @ApiModelProperty(value = "文化程度（小学，初中，高中，大学，研究生，博士生...）")
    private String degree;

    @ApiModelProperty(value = "健康状况（健康，有疾病，有重大疾病...）")
    private String health;

    @ApiModelProperty(value = "是否是农村户口（是：1，否：0）")
    private Integer countryside;

    @ApiModelProperty(value = "职业")
    private String occupation;

    @ApiModelProperty(value = "工作单位")
    private String workUnit;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "常住家庭住址")
    private String homeAddress;

    @ApiModelProperty(value = "婚姻状况（未婚，初婚，二婚...）")
    private String marriage;

    @ApiModelProperty(value = "结婚时间")
    private String marriageTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
