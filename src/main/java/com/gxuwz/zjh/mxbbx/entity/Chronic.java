package com.gxuwz.zjh.mxbbx.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>
 * 慢性疾病信息登记表
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Data
@Alias("Chronic") //别名
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Chronic对象", description="慢性疾病信息登记表")
public class Chronic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "疾病编码")
    private String chronicId;

    @ApiModelProperty(value = "拼音码")
    private String nameId;

    @ApiModelProperty(value = "疾病名称")
    private String chronicName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
