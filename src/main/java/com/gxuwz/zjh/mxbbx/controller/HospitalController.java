package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.*;
import com.gxuwz.zjh.mxbbx.service.*;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 医院表 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/hospital")
public class HospitalController {

    @Autowired
    private IHospitalService iHospitalService;
    @Autowired
    private IEconomicService iEconomicService;
    @Autowired
    private ISubService iSubService;
    @Autowired
    private IHealthService iHealthService;
    @Autowired
    private IMechanismService iMechanismService;
    @Autowired
    private ILevelService iLevelService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findHospitalAll")
    public ModelAndView findHospitalAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                        @Param("hospitalId") String hospitalId,
                                   HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Hospital> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("hospital_id");
        // 进行模糊查询!!!
        if(hospitalId != null && hospitalId != ""){
            wrapper.like("hospital_id", hospitalId);
            request.getSession().setAttribute("hospitalId", hospitalId);
        }else {
            request.getSession().setAttribute("hospitalId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Hospital> hospitalIPage = iHospitalService.selectHospitalPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)hospitalIPage.getPages()];
        for(int i=0; i< (int)hospitalIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+hospitalIPage.getTotal());
        System.out.println("总页数"+hospitalIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)hospitalIPage.getPages());
        List<Hospital> hospitalList = hospitalIPage.getRecords();
        System.out.println("hospitalList = "+hospitalList);
        modelAndView.addObject("hospitalList", hospitalList);

        // 获取构成医院表格的5个表的数据
        // economic
        List<Economic> economicList = iEconomicService.findEconomicAll();
        modelAndView.addObject("economicList", economicList);
        // sub
        List<Sub> subList = iSubService.findSubAll();
        modelAndView.addObject("subList", subList);
        // health
        List<Health> healthList = iHealthService.findHealthAll();
        modelAndView.addObject("healthList", healthList);
        // mechanism
        List<Mechanism> mechanismList = iMechanismService.findMechanismAll();
        modelAndView.addObject("mechanismList", mechanismList);
        // level
        List<Level> levelList = iLevelService.findLevelAll();
        modelAndView.addObject("levelList", levelList);

        modelAndView.setViewName("hospital/hospital_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertHospital")
    public ModelAndView insertHospital(ModelAndView modelAndView) {
        // 获取构成医院表格的5个表的数据
        // economic
        List<Economic> economicList = iEconomicService.findEconomicAll();
        modelAndView.addObject("economicList", economicList);
        // sub
        List<Sub> subList = iSubService.findSubAll();
        modelAndView.addObject("subList", subList);
        // health
        List<Health> healthList = iHealthService.findHealthAll();
        modelAndView.addObject("healthList", healthList);
        // mechanism
        List<Mechanism> mechanismList = iMechanismService.findMechanismAll();
        modelAndView.addObject("mechanismList", mechanismList);
        // level
        List<Level> levelList = iLevelService.findLevelAll();
        modelAndView.addObject("levelList", levelList);

        modelAndView.setViewName("hospital/hospital_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addHospital")
    public ModelAndView addHospital(ModelAndView modelAndView, Hospital hospital, HttpServletRequest request) {
        Hospital hospital1 = iHospitalService.findHospitalById(hospital);
        if(hospital1 == null){
            iHospitalService.addHospital(hospital);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("hospital/hospital_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateHospital")
    public ModelAndView updateHospital(ModelAndView modelAndView, String hospitalId) {
        // 获取构成医院表格的5个表的数据
        // economic
        List<Economic> economicList = iEconomicService.findEconomicAll();
        modelAndView.addObject("economicList", economicList);
        // sub
        List<Sub> subList = iSubService.findSubAll();
        modelAndView.addObject("subList", subList);
        // health
        List<Health> healthList = iHealthService.findHealthAll();
        modelAndView.addObject("healthList", healthList);
        // mechanism
        List<Mechanism> mechanismList = iMechanismService.findMechanismAll();
        modelAndView.addObject("mechanismList", mechanismList);
        // level
        List<Level> levelList = iLevelService.findLevelAll();
        modelAndView.addObject("levelList", levelList);

        Hospital hospital = new Hospital();
        hospital.setHospitalId(hospitalId);
        Hospital hospital1 = iHospitalService.findHospitalById(hospital);
        modelAndView.addObject("hospital", hospital1);
        modelAndView.setViewName("hospital/hospital_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editHospital")
    public ModelAndView editHospital(ModelAndView modelAndView, Hospital hospital, HttpServletRequest request) {
        iHospitalService.updateHospitalById(hospital);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("hospital/hospital_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteHospitalById")
    public ModelAndView deleteHospitalById(ModelAndView modelAndView, HttpServletRequest request, String hospitalId) {
        Hospital hospital = new Hospital();
        hospital.setHospitalId(hospitalId);
        iHospitalService.deleteHospitalById(hospital);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("hospital/hospital_list");
        return modelAndView;
    }

}
