package com.gxuwz.zjh.mxbbx.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Families;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxuwz.zjh.mxbbx.entity.Family;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 参合人员档案信息表 Mapper 接口
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@Mapper
@Repository
public interface FamiliesMapper extends BaseMapper<Families> {

    // 根据 id 查询对应用户信息
    Families findFamiliesById(Families families);

    // 根据 family 查询对应用户信息
    List<Families> findFamiliesByFamily(Families families);

    // 根据 nonghe 查询对应用户信息
    Families findFamiliesByNonghe(Families families);

    // 根据 number 查询对应用户信息
    Families findFamiliesByNumber(Families families);

    // 查询全部用户信息
    List<Families> findFamiliesAll();

    //自定义sql 分页
    IPage<Families> selectFamiliesPage(Page<Families> page, @Param(Constants.WRAPPER) Wrapper<Families> wrapper);

    // 添加用户信息
    void addFamilies(Families families);

    // 修改用户信息
    void updateFamiliesById(Families families);

    // 删除用户信息
    void deleteFamiliesById(Families families);

}
