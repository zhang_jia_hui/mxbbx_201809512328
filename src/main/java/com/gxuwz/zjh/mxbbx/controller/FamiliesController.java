package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Families;
import com.gxuwz.zjh.mxbbx.entity.Family;
import com.gxuwz.zjh.mxbbx.entity.Personnel;
import com.gxuwz.zjh.mxbbx.service.IFamiliesService;
import com.gxuwz.zjh.mxbbx.service.IFamilyService;
import com.gxuwz.zjh.mxbbx.service.IPersonnelService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 参合人员档案信息表 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/families")
public class FamiliesController {


    @Autowired
    private IFamiliesService iFamiliesService;
    @Autowired
    private IFamilyService iFamilyService;
    @Autowired
    private IPersonnelService iPersonnelService;

    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     */
    @Pointcut
    @RequestMapping(value = "/findFamiliesAll")
    public ModelAndView findFamiliesAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                        HttpServletRequest request, HttpServletResponse response,
                                        @Param("nongheId") String nongheId,
                                        @Param("name") String name) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Families> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("family_id");
        wrapper.orderByAsc("families_id");
        // 进行模糊查询!!!
        if(nongheId != null && nongheId != ""){
            wrapper.like("nonghe_id", nongheId);
            request.getSession().setAttribute("nongheId", nongheId);
        }else {
            request.getSession().setAttribute("nongheId", "");
        }
        if(name != null && name != ""){
            wrapper.like("name", name);
            request.getSession().setAttribute("name", name);
        }else {
            request.getSession().setAttribute("name", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Families> familiesIPage = iFamiliesService.selectFamiliesPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)familiesIPage.getPages()];
        for(int i=0; i< (int)familiesIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+familiesIPage.getTotal());
        System.out.println("总页数"+familiesIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)familiesIPage.getPages());
        List<Families> familiesList = familiesIPage.getRecords();
        System.out.println("familiesList = "+familiesList);
        modelAndView.addObject("familiesList", familiesList);
        modelAndView.setViewName("families/families_list");

        // 查询全部的nongheId信息
        List<Families> familiesList1 = iFamiliesService.findFamiliesAll();
        modelAndView.addObject("familiesList1", familiesList1);

        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param familyId
     * @return
     */
    @RequestMapping(value = "/insertFamilies")
    public ModelAndView insertFamilies(ModelAndView modelAndView, String familyId) {
        Family family = new Family();
        if(familyId != null && familyId != ""){
            family.setFamilyId(familyId);
            family = iFamilyService.findFamilyById(family);
        }
        Families families = new Families();
        families.setFamilyId(family.getFamilyId());
        modelAndView.addObject("families", families);
        System.out.println("families = " + families);
        modelAndView.setViewName("families/families_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addFamilies")
    public ModelAndView addFamilies(ModelAndView modelAndView, Families families, Personnel personnel,
                                    HttpServletRequest request,
                                    @Param("birthday") String birthday,
                                    @Param("marriageTime") String marriageTime ) throws ParseException {
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        String birthday1 = null;
        String marriageTime1 = null;
        if(birthday != null && birthday != ""){
            Date time1 = formatter2.parse(birthday);
            birthday1 =formatter2.format(time1);
        }
        if(marriageTime != null && marriageTime != ""){
            Date time1 = formatter1.parse(marriageTime);
            marriageTime1 =formatter1.format(time1);
        }
        System.out.println("birthday1 = " + birthday1);
        System.out.println("marriageTime1 = " + marriageTime1);

        personnel.setBirthday(birthday1);
        personnel.setMarriageTime(marriageTime1);

        Personnel personnel1 = iPersonnelService.findPersonnelById(personnel);

        Families families1 = iFamiliesService.findFamiliesById(families);
        if(families1 == null && personnel1 == null){
            iFamiliesService.addFamilies(families);
            iPersonnelService.addPersonnel(personnel);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("families/families_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateFamilies")
    public ModelAndView updateFamilies(ModelAndView modelAndView, String familiesId) {
        // 根据familiesId查找参合农民信息
        Families families = new Families();
        families.setFamiliesId(familiesId);
        Families families1 = iFamiliesService.findFamiliesById(families);
        modelAndView.addObject("families", families1);

        // 根据number查找人员信息
        Personnel personnel = new Personnel();
        String number = families1.getNumber();
        personnel.setNumber(number);
        Personnel personnel1 = iPersonnelService.findPersonnelById(personnel);
        modelAndView.addObject("personnel", personnel1);

        modelAndView.setViewName("families/families_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editFamilies")
    public ModelAndView editFamilies(ModelAndView modelAndView, Families families, Personnel personnel,
                                     @Param("birthday") String birthday,
                                     @Param("marriageTime") String marriageTime,
                                     HttpServletRequest request) throws ParseException {
        // 修改人员信息
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        String birthday1 = null;
        String marriageTime1 = null;
        if(birthday != null && birthday != ""){
            Date time1 = formatter2.parse(birthday);
            birthday1 =formatter2.format(time1);
        }
        if(marriageTime != null && marriageTime != ""){
            Date time1 = null;
            try {
                time1 = formatter1.parse(marriageTime);
            }catch (Exception e){
                time1 = formatter2.parse(marriageTime);
            }
            marriageTime1 =formatter1.format(time1);
        }
        System.out.println("birthday1 = " + birthday1);
        System.out.println("marriageTime1 = " + marriageTime1);
        personnel.setBirthday(birthday1);
        personnel.setMarriageTime(marriageTime1);
        if(iPersonnelService.findPersonnelById(personnel) == null){
            iPersonnelService.addPersonnel(personnel);
        }else {
            iPersonnelService.updatePersonnelById(personnel);
        }

        // 修改参合农民信息
        families.setNumber(personnel.getNumber());
        families.setName(personnel.getName());
        iFamiliesService.updateFamiliesById(families);

        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("families/families_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteFamiliesById")
    public ModelAndView deleteFamiliesById(ModelAndView modelAndView, HttpServletRequest request, String familiesId) {
        Families families = new Families();
        families.setFamiliesId(familiesId);
        iFamiliesService.deleteFamiliesById(families);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("families/families_list");
        return modelAndView;
    }

}
