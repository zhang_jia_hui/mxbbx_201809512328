package com.gxuwz.zjh.mxbbx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Agency;
import com.gxuwz.zjh.mxbbx.entity.Chronic;
import com.gxuwz.zjh.mxbbx.service.IAgencyService;
import com.gxuwz.zjh.mxbbx.service.IChronicService;
import org.apache.ibatis.annotations.Param;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 前端控制器
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-03-21
 */
@RestController
@RequestMapping("/mxbbx/chronic")
public class ChronicController {

    @Autowired
    private IChronicService iChronicService;


    /**
     * 查询全部信息
     * @param modelAndView
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Pointcut
    @RequestMapping(value = "/findChronicAll")
    public ModelAndView findChronicAll(ModelAndView modelAndView, Page page, Integer pageNumber,
                                       @Param("chronicId") String chronicId,
                                       HttpServletRequest request, HttpServletResponse response) {
        // 可以通过 wrapper 进行筛选!!!
        QueryWrapper<Chronic> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("chronic_id");
        // 进行模糊查询!!!
        if(chronicId != null && chronicId != ""){
            wrapper.like("chronic_id", chronicId);
            request.getSession().setAttribute("chronicId", chronicId);
        }else {
            request.getSession().setAttribute("chronicId", "");
        }
        // Current,页码 + Size,每页条数
        if(pageNumber == null){
            page.setCurrent(1);
        }else {
            page.setCurrent((long)pageNumber);
        }
        // 默认每页6行数据！
        page.setSize(6);
        // 调用分页查询方法！!
        IPage<Chronic> chronicIPage = iChronicService.selectChronicPage(page, wrapper);
        HttpSession session = request.getSession();
        // 存放一个数组用来让foreach遍历
        int[] pagesList = new int[(int)chronicIPage.getPages()];
        for(int i=0; i< (int)chronicIPage.getPages(); i++){
            pagesList[i] = i+1;
        }
        session.setAttribute("pagesList", pagesList);
        // 存放page，内有当前页数
        session.setAttribute("page", page);
        System.out.println("总条数"+chronicIPage.getTotal());
        System.out.println("总页数"+chronicIPage.getPages());
        // 存放总页数
        session.setAttribute("pages", (int)chronicIPage.getPages());
        List<Chronic> chronicList = chronicIPage.getRecords();
        System.out.println("chronicList = "+chronicList);
        modelAndView.addObject("chronicList", chronicList);
        modelAndView.setViewName("chronic/chronic_list");
        return modelAndView;
    }


    /**
     * 跳转进入新增界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/insertChronic")
    public ModelAndView insertChronic(ModelAndView modelAndView) {
        modelAndView.setViewName("chronic/chronic_insert");
        return modelAndView;
    }

    /**
     * 添加信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/addChronic")
    public ModelAndView addChronic(ModelAndView modelAndView, Chronic chronic, HttpServletRequest request) {
        Chronic chronic1 = iChronicService.findChronicById(chronic);
        if(chronic1 == null){
            iChronicService.addChronic(chronic);
            request.setAttribute("result1", "addTrue");
        }else {
            request.setAttribute("result1", "addFalse");
        }
        modelAndView.setViewName("chronic/chronic_list");
        return modelAndView;
    }

    /**
     * 跳转进入修改界面
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/updateChronic")
    public ModelAndView updateChronic(ModelAndView modelAndView, String chronicId) {
        Chronic chronic = new Chronic();
        chronic.setChronicId(chronicId);
        Chronic chronic1 = iChronicService.findChronicById(chronic);
        modelAndView.addObject("chronic", chronic1);
        modelAndView.setViewName("chronic/chronic_update");
        return modelAndView;
    }

    /**
     * 修改信息
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/editChronic")
    public ModelAndView editChronic(ModelAndView modelAndView, Chronic chronic, HttpServletRequest request) {
        iChronicService.updateChronicById(chronic);
        request.setAttribute("result2", "editTrue");
        modelAndView.setViewName("chronic/chronic_list");
        return modelAndView;
    }

    /**
     * 删除信息
     * @return
     */
    @RequestMapping(value = "/deleteChronicById")
    public ModelAndView deleteChronicById(ModelAndView modelAndView, HttpServletRequest request, String chronicId) {
        Chronic chronic = new Chronic();
        chronic.setChronicId(chronicId);
        iChronicService.deleteChronicById(chronic);
        request.setAttribute("result3", "deleteTrue");
        modelAndView.setViewName("chronic/chronic_list");
        return modelAndView;
    }

}
