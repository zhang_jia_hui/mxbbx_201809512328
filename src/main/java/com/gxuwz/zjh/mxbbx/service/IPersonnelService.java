package com.gxuwz.zjh.mxbbx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.zjh.mxbbx.entity.Personnel;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 人员信息表 服务类
 * </p>
 *
 * @author zhangjiahui
 * @since 2021-05-02
 */
public interface IPersonnelService extends IService<Personnel> {

    // 根据 id 查询对应用户信息
    Personnel findPersonnelById(Personnel personnel);

    // 查询全部用户信息
    List<Personnel> findPersonnelAll();

    //自定义sql 分页
    IPage<Personnel> selectPersonnelPage(Page<Personnel> page, @Param(Constants.WRAPPER) Wrapper<Personnel> wrapper);

    // 添加用户信息
    void addPersonnel(Personnel personnel);

    // 修改用户信息
    void updatePersonnelById(Personnel personnel);

    // 删除用户信息
    void deletePersonnelById(Personnel personnel);

}
