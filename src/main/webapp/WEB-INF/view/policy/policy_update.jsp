<%@ page pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>慢性病报销系统</title>
    <link rel="stylesheet" href="<%=basePath %>/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>/css/admin.css">
    <link rel="shortcut icon" href="<%=basePath %>/images/logo.jpg">
    <script src="<%=basePath %>/js/jquery.js"></script>
    <script src="<%=basePath %>/js/pintuer.js"></script>
</head>
<body>
<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span> 修改信息</strong></div>
    <div class="body-content">
        <form method="post" class="form-x" action="<%=basePath %>/mxbbx/policy/editPolicy">
            <div class="form-group">
                <div class="label">
                    <label>慢病政策ID：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${policy.policyId}" name="policyId" data-validate="required:建议不修改"  />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>年度：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${policy.year}" name="year" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>封顶线：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${policy.cappLine}" name="cappLine" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>报销比例：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="${policy.reimRatio}" name="reimRatio" data-validate="required:" />
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <button class="button bg-main icon-check-square-o" type="submit"> 提交</button>
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <a href="<%=basePath %>/mxbbx/policy/findPolicyAll"><input type="button" class="button bg-main icon-check-square-o" value="取消" /></a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
